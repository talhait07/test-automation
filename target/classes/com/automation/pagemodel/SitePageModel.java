package com.automation.pagemodel;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
//import org.sikuli.api.DesktopScreenRegion;
//import org.sikuli.api.ImageTarget;
//import org.sikuli.api.ScreenRegion;
//import org.sikuli.api.Target;
//import org.sikuli.api.robot.Keyboard;
//import org.sikuli.api.robot.Mouse;
//import org.sikuli.api.robot.desktop.DesktopKeyboard;
//import org.sikuli.api.robot.desktop.DesktopMouse;
import org.testng.Assert;

import com.google.common.base.Function;

public class SitePageModel {

	static protected String uid = "email";
	static protected String pwd = "pass";
	static protected String btnSubmit = "u_0_n";
	static protected String user = "";
	static protected String pass = "";

	public static void clickMenuLinkText(WebDriver driver, String strMenuText) {

		implicitlyWait(driver, 30);
		driver.findElement(By.linkText(strMenuText)).click();
	}

	public static void checkMenuText(WebDriver driver, String strMenuText, By by) {

		String strResultText = driver.findElement(by).getText();
		Assert.assertEquals(strResultText, strMenuText);
	}

	public static void clickListLink(WebDriver driver, String by) {

		List<WebElement> linkElement = driver.findElements(By.linkText(by));
		linkElement.get(0).click();
	}

	public static void checkLogo(WebDriver driver, By by) {

		boolean bStatus = elementIsVisible(driver, by);
		Assert.assertTrue(bStatus);
	}
	
	public static String checkText(WebDriver driver, String strMenuText, By by) {

		String strResultText = driver.findElement(by).getText();
		Assert.assertEquals(strResultText, strMenuText);
		return strResultText;
	}

	/*
	 * this method will clear text element
	 */
	public static void clearTxt(WebDriver driver, By by) {
		driver.findElement(by).clear();
	}

	public static void implicitlyWait(WebDriver driver, int waitForSecond) {
		driver.manage().timeouts()
				.implicitlyWait(waitForSecond, TimeUnit.SECONDS);
	}

	protected static void pageLoadTimeout(WebDriver driver, int waitForSecond) {
		driver.manage().timeouts()
				.pageLoadTimeout(waitForSecond, TimeUnit.SECONDS);
	}

	protected static WebElement fluentWait(WebDriver driver, int waitForSecond,
			int checkingInterval, final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(waitForSecond, TimeUnit.SECONDS)
				.pollingEvery(checkingInterval, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});

		return element;
	};

	/**
	 * use this method to enter text into text fields. first clears and then
	 * sends keys.
	 * 
	 * @param by
	 *            element location
	 * @param text
	 *            text message information
	 */
	public static void enterText(WebDriver driver, By by, String text) {
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(text);
	}

	public static void enterAutoCompleteText(WebDriver driver, By by,
			String text) {

		driver.findElement(by).click();
		waitFor(7);
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(text);
		waitFor(5);
		driver.findElement(by).sendKeys(Keys.TAB);
		waitFor(5);
	}

	/**
	 * returns element text.
	 */
	public static String getElementText(WebDriver driver, By by) {
		return driver.findElement(by).getText();
	}

	public static void tryClick(WebDriver driver, By by) {
		driver.findElement(by).click();
	}

	/**
	 * this method returns true/false for displaying
	 * 
	 * @param by
	 *            element location
	 * @return returns whether the element is visible or not.
	 */
	public static boolean elementIsVisible(WebDriver driver, By by) {
		return driver.findElement(by).isDisplayed();
	}

	/**
	 * waits up to few seconds to perform action.
	 */
	public static void waitFor(double second) {
		try {
			Thread.sleep((int) (1000 * second));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void scrollDownOfPage(WebDriver driver) {
		   JavascriptExecutor jse = (JavascriptExecutor)driver;
		   jse.executeScript("scroll(0, 150)"); //y value '250' can be altered
		}

	/**
	 * Switch to new window opened
	 */
	protected static void switchToNewestWindow(WebDriver driver, String main,
			Set<String> winHandles) {
		for (String winHandle : winHandles) {
			if (!winHandle.equals(main)) {
				driver.switchTo().window(winHandle);
			}
		}
	}

	/**
	 * this method will login as required and return success or fail as boolean
	 */
	public static boolean loginAs(WebDriver driver, String userId,
			String passwd, int nWaitTime) {
		boolean bSuccess = false;
		// if not logged in, login to the system
		if (driver
				.findElements(
						By.cssSelector("div#main-menu > ul#mainmenu > li.dropdown > a.menu-profile.dropdown-toggle"))
				.size() == 0) {
			enterText(driver, By.id(uid), userId);
			enterText(driver, By.id(pwd), passwd);
			tryClick(driver, By.id(btnSubmit));

			bSuccess = true;
		} else
			bSuccess = false;

		// return false at login failure
		return bSuccess;
	}

	/**
	 * this method will logout if system is in login and return success or fail
	 * as boolean
	 */
	protected static boolean logoutFromAccount(WebDriver driver, int nWaitFor) {
		// if logged in, log out
		if (driver
				.findElements(
						By.cssSelector("div#main-menu > ul#mainmenu > li.dropdown > a.menu-profile.dropdown-toggle"))
				.size() > 0) {
			tryClick(
					driver,
					By.cssSelector("div#main-menu > ul#mainmenu > li.dropdown > a.menu-profile.dropdown-toggle"));
			tryClick(driver, By.linkText("Log out"));
		}
		// return true on logout success
		if (driver
				.findElements(
						By.cssSelector("div#main-menu > ul#mainmenu > li.dropdown > a.menu-profile.dropdown-toggle"))
				.size() == 0)
			return true;

		// return false on logout failure
		return false;
	}

	protected static String getPageSource(WebDriver driver, String url,
			int nWaitTime) {
		driver.get(url);
		waitFor(nWaitTime);
		return driver.getPageSource();
	}

	protected static long getRandomValueFixedLength(int length) {
		Random random = new Random();
		long number = random.nextInt((int) (Math.pow(10, length)));

		return number;
	}

	protected static void refreshBrowser(WebDriver driver) {
		driver.navigate().refresh();
		waitFor(3);
	}

	protected static int getItemnumber(WebDriver driver, By by) {
		WebElement element = driver.findElement(by);
		int itemCount = Integer.parseInt(element.getText().replaceFirst(
				".*?(\\d+).*", "$1"));
		return itemCount;
	}

	public static void selectOptionVal(WebDriver driver, By by,
			String strOptText) {

		WebElement select = driver.findElement(by);
		List<WebElement> options = select.findElements(By.tagName("option"));

		for (WebElement option : options) {
			if (option.getText().equals(strOptText)) {
				option.click();
				break;
			}
		}
	}

	protected static void clickLinkByHref(WebDriver driver, String href) {
		List<WebElement> anchors = driver.findElements(By.tagName("a"));
		Iterator<WebElement> i = anchors.iterator();

		while (i.hasNext()) {
			WebElement anchor = i.next();
			if (anchor.getAttribute("href").contains(href)) {
				anchor.click();
				break;
			}
		}
	}

	public static void linkClick(WebDriver driver, By by) {

		implicitlyWait(driver, 20);

		List<WebElement> linkElement = driver.findElements(by);
		((WebElement) linkElement.get(0)).click();
	}

	public static void handleAlertWindow(WebDriver driver, boolean bAccept) {

		if (driver.switchTo().alert() != null) {
			Alert alert = driver.switchTo().alert();
		//	String alertText = alert.getText();
			if (bAccept) {
				alert.accept();
			} else {
				alert.dismiss();
			}

		}
	}

	public static void fileUploadAutoIt(WebDriver driver,
			String strUploadExePath) throws IOException, InterruptedException {
		
		implicitlyWait(driver, 5);
		Runtime.getRuntime().exec(strUploadExePath);
	}

	public static void checkLinkURL(WebDriver driver, By by) throws IOException {
		String response = "";
		URL urlLink = null;
		WebElement elementLink = driver.findElement(by);
		System.out.println("Link Text: " + elementLink.getText());

		String strBaseURL = "https://staging-secure.blinqmedia.com/";

		String strHref = elementLink.getAttribute("href");
		strHref = strBaseURL + strHref;
		System.out.println("HREF Text: " + strHref);
		try {
			urlLink = new URL(strHref);
		} catch (MalformedURLException e) {
			response = e.getMessage();
		}

		HttpURLConnection connection = (HttpURLConnection) urlLink
				.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
		} catch (Exception exp) {
			response = exp.getMessage();
		}

		System.out.println("Response Text: " + response);
		Assert.assertEquals(response, "OK");
	}

	public static void checkUrl(WebDriver driver, String url) {

		String urlLibDirectory = driver.getCurrentUrl();
		Assert.assertEquals(urlLibDirectory, url);
		//System.out.println(driver.getCurrentUrl());
	}
//
//	public static void checkTitle(WebDriver driver, String textTitle) {
//		String urlLibDirectory = driver.getCurrentUrl();
//		Assert.assertEquals(driver.getTitle(), textTitle);
//		System.out.println(driver.getTitle());
//	}

	public static void checkbox(WebDriver driver, By by) {
		List<WebElement> checkbox = driver.findElements(by);
		((WebElement) checkbox.get(0)).click();
	}

	public static void selectRadioText(WebDriver driver, By by) {

		WebElement radioElement = driver.findElement(by);
		System.out.println("Option Text:" + radioElement.getText());
		radioElement.click();
	}

	public static WebElement getElementObject(WebDriver driver, By by) {

		WebElement element = null;
		try {
			element = driver.findElement(by);
			return element;
		} catch (Exception e) {
			return null;
		}
	}

	public static void maximizeWindow(WebDriver driver) {

		driver.manage().window().maximize();
	}

//	public static void switchtoPOPupWindow(WebDriver driver) {
//
//		Set<String> handle = driver.getWindowHandles();
//	}
	
	public static String randomStringGenerator(WebDriver driver) {
		
		final String AB = "0123";
		Random rnd = new Random();

		   StringBuilder sb = new StringBuilder();
		   for( int i = 0; i < 6; i++ ) 
		      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		   return sb.toString();
		}
	
	public static void scrollBottomOfPage(WebDriver driver) {
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
	}

	public static void scrollTopOfPage(WebDriver driver){
	   
		((JavascriptExecutor) driver).executeScript("scroll(250,0);");
	      }
	
	
	
	public static String getTopWindowHandle(WebDriver driver) {

		Set<String> windows = driver.getWindowHandles();

		String handleName = "";
		for (String window : windows) {
			handleName = window;
		}

		return handleName;
	}
	

//	public static void clickLocationBySikuli(String strLocationImage) {
//
//		ScreenRegion screen = new DesktopScreenRegion();
//		Mouse mouse = new DesktopMouse();
//		//tell Sikuli which part of the desktop it should target
//		Target target = new ImageTarget(new File(strLocationImage));
//		BufferedImage bImage = screen.capture();
//		//target the region
//		ScreenRegion region = screen.find(target);
//		//click on the center of it
//		mouse.click(region.getCenter());
//	}
//	
//	
//	public static void typeImagePathBySikuli(String strInputTextImage,
//			String strTargetImage) {
//
//		ScreenRegion screen = new DesktopScreenRegion();
//		Mouse mouse = new DesktopMouse();
//		Keyboard kb = new DesktopKeyboard();
//
//		Target target = new ImageTarget(new File(strInputTextImage));
//
//		ScreenRegion region = screen.find(target);
//		mouse.click(region.getCenter());
//
//		kb.type(strTargetImage);
//	}
	
	 public static void clearTextByRobotClass() throws AWTException {
			
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_A);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_A);
			robot.keyPress(KeyEvent.VK_BACK_SPACE);
			robot.keyRelease(KeyEvent.VK_BACK_SPACE);
			
		}
	 
	 
	 
	 public static void clickButtonByRobotClass() throws AWTException {

 		Robot robot = new Robot();
 		robot.keyPress(KeyEvent.VK_ENTER);
 		robot.keyRelease(KeyEvent.VK_ENTER);
 	

//		Robot robot = new Robot();
//		//press Enter key
//		robot.keyPress(KeyEvent.VK_2);
//		//release Enter key
//		robot.keyRelease(KeyEvent.VK_2);
//		
//		
//		robot.keyPress(KeyEvent.VK_0);
//		//release Enter key
//		robot.keyRelease(KeyEvent.VK_0);
//		
//		
//		robot.keyPress(KeyEvent.VK_1);
//		//release Enter key
//		robot.keyRelease(KeyEvent.VK_1);
//		
//		
//		robot.keyPress(KeyEvent.VK_5);
//		//release Enter key
//		robot.keyRelease(KeyEvent.VK_5);
		
	}
	
	 public static String AddDateWithToday(int addDateNum) {
		  
		 DateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy h:mm a");		  
		  Calendar calendar = Calendar.getInstance();
		      
		     // get a date to represent "today"
		    Date today = calendar.getTime();
		     // add day to the date/calendar
		     calendar.add(Calendar.DAY_OF_YEAR, addDateNum);
		      
		     // now get future
		     Date future = calendar.getTime();
		   String futureDate=dateFormat.format(future);
		    
		       return futureDate;
		  
		 }
	 
	 public static void enterAutoCompleteDate(WebDriver driver, By by,
		      String date) {

		     driver.findElement(by).clear();
		     driver.findElement(by).click();
		     driver.findElement(by).sendKeys(date);	     

		     waitFor(10);
		     driver.findElement(by).sendKeys(Keys.TAB);
		  }
	 
	 public static boolean doesElementExist(WebDriver driver, By by) {
		    return driver.findElements(by).size() != 0;
		}	 
	 
	 public static void scrollElement(WebDriver driver, By by) {

			Actions actions = new Actions(driver);
	        actions.moveToElement(driver.findElement(by));
	        actions.click();
	        actions.perform();
	        actions.sendKeys(Keys.PAGE_DOWN).perform();
		}
	 
	 
	 public static void enterCompleteText(WebDriver driver, By by,
				String text) {


			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(text);
			waitFor(5);
			driver.findElement(by).sendKeys(Keys.TAB);
			waitFor(5);
		}
	 
	 
		public static String getCurrnetSystemTime(WebDriver driver) {
			
			Date now = new Date();
//			String timeStamp = new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
//			String format2 = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").format(now);	//EEE for day of the weeks
			String timeStamp = new SimpleDateFormat("MMMM d, yyyy h:mm a").format(now);
//			String timeStamp = new SimpleDateFormat("HH:mm").format(now);
//			String timeStamp = new SimpleDateFormat("hh:mm").format(now);

//			System.out.println(timeStamp);
			return timeStamp;	
			
		}
		
		
		public static String getRoundSystemTime(WebDriver driver) {		
			

			Date whateverDateYouWant = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(whateverDateYouWant);

			int unroundedMinutes = calendar.get(Calendar.MINUTE);
			int mod = unroundedMinutes % 30;
			calendar.add(Calendar.MINUTE, mod < 29 ? -mod : (30-mod));
			//calendar.set(Calendar.MINUTE, unroundedMinutes + mod);
			return btnSubmit;
			
//			calendar = DateUtils.truncate(calendar, Calendar.MINUTE);
			
		}
		public static SimpleDateFormat getTimeDifference(WebDriver driver, long elementval) {

		long start = System.currentTimeMillis();
//		long end = start + (1*3600 + 23*60 + 45) * 1000 + 678; // 1 h 23 min 45.678 s
		Date timeDiff = new Date(elementval - start - 3600000); // compensate for 1h in millis
		SimpleDateFormat timeFormat = new SimpleDateFormat("H:mm:ss.SSS");
		System.out.println("Duration: " + timeFormat.format(timeDiff));
		return timeFormat;
		}

		public static void getTimeDifference(WebDriver driver, String elementval) {
			// TODO Auto-generated method stub
			
		}
}