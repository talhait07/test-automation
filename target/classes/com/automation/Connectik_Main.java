 package com.automation;

import java.util.Map;

import com.automation.suite.TestStarter;
import com.automation.util.*;
import com.model.TestStatusReportModel;

public class Connectik_Main {

	public static void main(String[] args) {
		
		System.out.println("Prepare Setting");
		PropertySettings setting = new PropertySettings();
		
		// run test for all browser one by one
		Map<Integer, String> browsers = setting.getBrowserList();
		for (int i = 0; i < browsers.size(); i++) {
			if (!browsers.get(i).equals("")) {
				setting.setBrowser(browsers.get(i));
				setting.setDriver();
				TestStarter testStarter = new TestStarter(setting);
				testStarter.start();
			}
		}
		
//		int nFailCount = TestStatusReportModel.getTotalFailed();
//		if (nFailCount > 0) {
			 SendMail.sendmail(setting);
//			if(setting.doFailedStatusUpdate()){
//				setting.updateTestCaseFailedStatus();
//			}
			
//		}
//		else {
//			System.out.println("No fail");
//		}
	}
}

