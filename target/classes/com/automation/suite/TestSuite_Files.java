//package com.automation.suite;
//
//import java.awt.AWTException;
//import java.util.List;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.StaleElementReferenceException;
//import org.openqa.selenium.WebElement;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//import com.automation.pagemodel.SitePageModel;
//import com.automation.util.AppConstant;
//
//public class TestSuite_Files extends TestHelper {
//	
//	String folederName = SitePageModel.randomStringGenerator(driver);
//    String privateFlodername = SitePageModel.randomStringGenerator(driver);
//    String channelName = "Files Automation"; 
//    String channelDescrip = "This channel is created for files portion";
//    
//    
//	@Test
//	@TestInfo(tcName="Create folder",feature="File", expectedResult="Folder is created")
//	public void tc_0201_CreateFolder() {
//		SitePageModel.waitFor(5);
//		
//		/*SitePageModel.tryClick(driver,By.id("lsb-feeds")); // Click on create channel icon Add Team
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.partialLinkText("Archives")); // Click on create channel icon Add Team
//		SitePageModel.waitFor(5);
//		SitePageModel.enterText(driver,By.xpath("//input[@class='form-control teamsCustom']"), channelName);
//		SitePageModel.waitFor(2);
//		driver.findElement(By.xpath("//input[@class='form-control teamsCustom']")).sendKeys(Keys.ENTER);
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.className("caret"));
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.partialLinkText("Reactivate Team"));
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='Reactivate Team' and @class='btn btn-success']"));
//		SitePageModel.waitFor(2); */
//		
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File
//		SitePageModel.waitFor(5);
//		
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect	All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a")).getText();
//			SitePageModel.waitFor(3);
//			if (eventLists1.equals("Files Automation")) {
//				SitePageModel.scrollElement(driver, By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a"));// click file Automation				
//				break;				
//			}
//		} 
//		SitePageModel.waitFor(7);
//		SitePageModel.scrollElement(driver,By.xpath("//button[text()='New Folder' and @class='btn btn-default btn-mini']"));
//		// String folderName= SitePageModel.randomStringGenerator(driver);
//		SitePageModel.waitFor(3);
//		SitePageModel.enterText(driver,By.xpath("//input[@class='bootbox-input bootbox-input-text form-control']"), folederName);
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='OK' and @class='btn btn-primary']")); // ok
//		SitePageModel.waitFor(5);
//		List<WebElement> folder = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All lists
//		SitePageModel.waitFor(2);
//		System.out.println(folder.size());
//		for (int i = 1; i <= folder.size(); i++) {
//			String cretedFolderName = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]/a")).getText();
//			SitePageModel.waitFor(3);
//			if (cretedFolderName.equals(folederName)) {
//				String actual = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, cretedFolderName);
//				SitePageModel.waitFor(5);
//				SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]/a"));
//				break;
//			}
//		}
//	}
//
//	@Test
//	@TestInfo(tcName="Upload file into a folder",feature="Calendar", expectedResult="File is uploaded into a folder")
//	public void tc_0202_FileUploadInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "testimg1.png";
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		// check file is uploaded
//
//		List<WebElement> fileName = driver.findElements(By
//				.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All
//																// lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileName.size());
//
//		for (int i = 1; i <= fileName.size(); i++) {
//
//			String uploadedFileName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]/a")).getText();
//
//			if (uploadedFileName.equals(UploadedFile)) {
//
//				String actual = driver.findElement(
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, UploadedFile);
//				break;
//			}
//
//		}
//		SitePageModel.waitFor(5);
//	}
//
//	
//	@Test
//	@TestInfo(tcName="Upload same file multiple times",feature="File", expectedResult="Uploaded File version is updated")
//	public void tc_0203_DuplicateFileUploadInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "testimg1.png";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='OK' and @class='btn btn-primary']")); // ok
//		// check file is uploaded
//		SitePageModel.waitFor(5);
//		String uploadedFileNameExpected = "testimg1.png ver 2";
//		String uploadedFileName = driver.findElement(
//				By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"))
//				.getText();
//
//		Assert.assertEquals(uploadedFileName, uploadedFileNameExpected);
//
//	}
//
//	@Test
//	@TestInfo(tcName="Upload a unsupported file",feature="File", expectedResult="Error message is displayed")
//	public void tc_0204_UploadUnsupportedFileInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "testtext.txt";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(5);		
//	}
//	
//	@Test
//	@TestInfo(tcName="Delete a uploaded file",feature="File", expectedResult="Uploaded file is deleted")
//	public void tc_0205_DeleteFileUploadInFolder() {
//		
//        SitePageModel.waitFor(5);
//		SitePageModel.checkbox(driver,By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[1]/div")); //click on check box
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='Delete' and @class='btn btn-danger btn-mini']")); // Delete
//		SitePageModel.waitFor(2);
//		driver.switchTo().alert().dismiss(); //Cancel
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='Delete' and @class='btn btn-danger btn-mini']")); // Delete
//		SitePageModel.waitFor(2);
//		SitePageModel.handleAlertWindow(driver, true); //Confirm delete
//	}
//	
//	@Test
//	@TestInfo(tcName="Upload file using link",feature="File", expectedResult="File is uplaoded")
//	public void tc_0206_UploadFileFromDropsfiletoupload() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-uploader']/div/div/div[1]/div/span"));// click// drops file to  upload
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "test4.xlsx";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		// check file is uploaded
//
//		List<WebElement> fileName = driver.findElements(By
//				.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All
//																// lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileName.size());
//
//		for (int i = 1; i <= fileName.size(); i++) {
//
//			String uploadedFileName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]/a")).getText();
//
//			if (uploadedFileName.equals(UploadedFile)) {
//
//				String actual = driver.findElement(
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, UploadedFile);
//
//				break;
//			}
//
//		}
//
//	}
//
//	@Test
//	@TestInfo(tcName="Delete folder",feature="File", expectedResult=" Folder is deleted")
//	public void tc_0207_DeleteFolder() {
//		
//		SitePageModel.waitFor(3);
//		SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tr/td[2]/i"));// back
//		SitePageModel.waitFor(3);		
//		List<WebElement> fileName = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All
//																// lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileName.size());
//
//		for (int i = 1; i <= fileName.size(); i++) {
//
//			String uploadedFileName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]")).getText();
//
//			System.out.println(uploadedFileName);
//			if (uploadedFileName.equals(folederName)) {
//
//				SitePageModel.waitFor(2);
//				SitePageModel.checkbox(driver,By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[1]/div"));
//				SitePageModel.waitFor(2);																								
//				SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click on Delete
//				SitePageModel.waitFor(3);
//				driver.switchTo().alert().dismiss();
//				SitePageModel.waitFor(2);
//				SitePageModel.tryClick(driver,By.xpath("//button[text()='Delete' and @class='btn btn-danger btn-mini']")); // Delete button			
//				SitePageModel.waitFor(3);
//				driver.switchTo().alert().accept();				
//				break;
//			}
//
//		}
//
//	}
//
//	@Test
//	@TestInfo(tcName="Restore a Folder",feature="File", expectedResult="Previously deleted folder is retrieved")
//	public void tc_0208_RestoreFolder() {
//
//		SitePageModel.waitFor(2);
//
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr"));// Collect All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]")).getText();
//			System.out.println(eventLists1);
//			if (eventLists1.equals("Trash")) {
//				SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]"));// click Trash
//				break;
//			}
//		}
//		// SitePageModel.tryClick(driver,
//		// By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Find a member...']"));//("Click member field"));
//		SitePageModel.waitFor(2);
//		List<WebElement> trashfolderList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect// All// lists
//		SitePageModel.waitFor(3);
//		System.out.println("trash size" + trashfolderList.size());
//
//		for (int i = 1; i <= trashfolderList.size(); i++) {
//
//			String trashFolder = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]")).getText();
//			System.out.println(trashFolder);
//			if (trashFolder.equals(folederName)) {
//				SitePageModel.waitFor(2);
//				SitePageModel.checkbox(driver, By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[1]/div"));//
//				SitePageModel.waitFor(2);
//				SitePageModel.tryClick(driver,By.xpath("//button[text()='Restore' and @class='btn btn-success btn-mini']")); // Click on Restore
//				SitePageModel.waitFor(3);
//				break;
//			}
//
//		}
//		
//	}
//	
//	@Test
//	@TestInfo(tcName="Delete a floder from Trash",feature="File", expectedResult="Folder is deleted from trash")
//	public void tc_0209_DeleteFolderFromTrash() {
//		driver.navigate().to("https://secure.connectik.com");
//		SitePageModel.waitFor(20);
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File
//		SitePageModel.waitFor(3);
//		List<WebElement> fileListAu = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect	All lists
//		SitePageModel.waitFor(3);
//		System.out.println(fileListAu.size());
//		for (int i = 1; i <= fileListAu.size(); i++) {
//			String eventListsAu = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a")).getText();
//			if (eventListsAu.equals("Files Automation")) {
//				SitePageModel.scrollElement(driver, By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a"));// click file Automation
//				break;
//			}
//		}	
//		SitePageModel.waitFor(3);
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr"));// Collect All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]")).getText();
//			System.out.println(eventLists1);
//			if (eventLists1.equals("Trash")) {
//				SitePageModel.waitFor(2);
//				SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]"));// click Trash
//				SitePageModel.waitFor(3);
//				break;
//			}
//		}
//		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/thead/tr/th[1]/div/label"));// click on All check box
//		SitePageModel.checkbox(driver,By.xpath("//div[@class='checkbox check-default']")); //Click check box
//		List<WebElement> checkList = driver.findElements(By.xpath("//div[@class='checkbox check-default']"));// Collect All check box lists
//		SitePageModel.waitFor(3);
//		System.out.println(checkList.size());
//		if(checkList.size()>1){
//			SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click Delete button
//			SitePageModel.waitFor(3);
//			driver.switchTo().alert().dismiss();
//			SitePageModel.waitFor(3);
//			SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click Delete button
//			SitePageModel.waitFor(3);
//			driver.switchTo().alert().accept();
//			SitePageModel.waitFor(3);
//		}else{
//			System.out.println("There is no selected item");
//		}
//	}
//	
//	@Test
//	@TestInfo(tcName="Restore a Folder",feature="File", expectedResult="Previously deleted folder is retrieved")
//	public void tc_0210_SearchFolder() {
//		SitePageModel.waitFor(5);
//		driver.navigate().to("https://secure.connectik.com/group");
//		SitePageModel.waitFor(20);
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File
//		SitePageModel.waitFor(3);
//		SitePageModel.enterText(driver, By.className("form-control"),"File Automation"); //search by file name
//		SitePageModel.waitFor(2);			
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect	All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a")).getText();
//			if (eventLists1.equals("Files Automation")) {
//				SitePageModel.scrollElement(driver, By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a"));// click file Automation
//				SitePageModel.waitFor(3);
//				break;
//			}
//		}
//	}
//
//	
//	@Test
//	@TestInfo(tcName="Upload a file",feature="File", expectedResult="Previously deleted folder is retrieved")
//	public void tc_0211_UploadFileInFile() throws AWTException {
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));// click upload button
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "test4.xlsx";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		// check file is uploaded
//
//		List<WebElement> fileName = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileName.size());
//		for (int i = 1; i <= fileName.size(); i++) {
//
//			String uploadedFileName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]/a")).getText();
//			if (uploadedFileName.equals(UploadedFile)) {
//
//				String actual = driver.findElement(
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, UploadedFile);
//				break;
//			}
//		}
//	}
//
//	@Test
//	@TestInfo(tcName="Delete uploaded file",feature="File", expectedResult="Uploaded file is deleted")
//	public void tc_0212_DeleteUploadFilefromFile() throws AWTException {
//		SitePageModel.waitFor(2);
//		SitePageModel.checkbox(driver,By.xpath("//div[@class='checkbox check-default']")); //Click check box
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click Delete button
//		SitePageModel.waitFor(2);
//		SitePageModel.handleAlertWindow(driver, true);
//	}
//	
//	@Test
//	@TestInfo(tcName="Create Folder in Private Folder",feature="File", expectedResult="Folder is created on Private")
//	public void tc_0213_CreateFolderInPrivate() throws AWTException {
//		SitePageModel.waitFor(5);
//		driver.navigate().to("https://secure.connectik.com/group");
//		SitePageModel.waitFor(20);
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File
//		SitePageModel.waitFor(5);
//        SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/ul/li[2]/a"));     
//        SitePageModel.waitFor(5);
//		/*List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect All lists	
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]/a")).getText();
//			if (eventLists1.equals("Files Automation")) {
//				SitePageModel.scrollElement(driver,By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]/a"));// click file Automation
//				break;
//			}
//		}*/
//
//		SitePageModel.waitFor(2);
//		SitePageModel.scrollElement(driver,By.xpath("//button[text()='New Folder' and @class='btn btn-default btn-mini']"));
//		SitePageModel.waitFor(3);		
//		SitePageModel.enterText(driver,By.xpath("//input[@class='bootbox-input bootbox-input-text form-control']"), privateFlodername);
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='OK' and @class='btn btn-primary']")); // ok
//		SitePageModel.waitFor(5);
//		List<WebElement> folder = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All List
//		SitePageModel.waitFor(3);
//		System.out.println(folder.size());
//
//		for (int i = 1; i <= folder.size(); i++) {
//
//			String cretedFolderName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]/a")).getText();
//
//			if (cretedFolderName.equals(privateFlodername)) {
//
//				String actual = driver.findElement(
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, cretedFolderName);
//				SitePageModel.waitFor(3);
//				SitePageModel.tryClick(
//						driver,
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a"));
//				break;
//			}
//		}
//	}
//               
//	@Test
//	@TestInfo(tcName="Upload File into Private Folder",feature="File", expectedResult="File is uploaded into private Folder")
//	public void tc_0214_PrivateFileUploadInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "Ee2.png";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(20);
//		// check file is uploaded
//
//		List<WebElement> fileName = driver.findElements(By
//				.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All
//																// lists
//		SitePageModel.waitFor(5);
//		System.out.println(fileName.size());
//
//		for (int i = 1; i <= fileName.size(); i++) {
//
//			String uploadedFileName = driver.findElement(
//					By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//							+ "]/td[2]/a")).getText();
//
//			if (uploadedFileName.equals(UploadedFile)) {
//
//				String actual = driver.findElement(
//						By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i
//								+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, UploadedFile);
//
//				break;
//			}
//
//		}
//
//	}    
//  
//	@Test
//	@TestInfo(tcName="Upload unsupported File into Private Folder",feature="File", expectedResult="File unsupported error is displayed")
//	public void tc_0215_PrivateUploadUnsupportedFileInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "testtext.txt";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/ul/li[1]/div"));
//
//		
//	}
//
//	@Test
//	@TestInfo(tcName="Upload same file multiple times on Private",feature="File", expectedResult="Uploaded File version is updated on private folder")
//	public void tc_0216_PrivateDuplicateFileUploadInFolder() throws AWTException {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='uploadBtn']"));
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//
//		SitePageModel.clearTextByRobotClass();
//
//		String UploadedFile = "Ee2.png";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='OK' and @class='btn btn-primary']")); // ok
//		SitePageModel.waitFor(3);
//		String uploadedFileNameExpected = "test4.xlsx ver 2";
//		SitePageModel.waitFor(2);
//		String uploadedFileName = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a")).getText();
//		SitePageModel.waitFor(10);
//		Assert.assertEquals(uploadedFileName, uploadedFileNameExpected);
//	}
//	
//	
//	
//	@Test
//	@TestInfo(tcName="Delete File from private",feature="File", expectedResult="File is deleted")
//	public void tc_0217_PrivateDeleteFileUploadInFolder() {
//
//		SitePageModel.waitFor(3);
//		SitePageModel.checkbox(driver,By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[1]/div"));
//		SitePageModel.waitFor(3);
//		SitePageModel.tryClick(driver,By.xpath("//button[text()='Delete' and @class='btn btn-danger btn-mini']"));// click Delete button
//		SitePageModel.waitFor(5);
//		SitePageModel.handleAlertWindow(driver, true);
//	}
//
//	@Test
//	@TestInfo(tcName="Upload file into Private by Drop icon",feature="File", expectedResult="File is uploaded")
//	public void tc_0218_PrivateUploadFileFromDropsfiletoupload() throws AWTException {
//
//		SitePageModel.waitFor(3);
//		SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-uploader']/div/div/div[1]/div/span"));// click drops file to upload
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//		String UploadedFile = "Ee2.png";
//		SitePageModel.waitFor(1);
//		String strFileImgPath = AppConstant.IMAGE_PATH + UploadedFile;
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strFileImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		// check file is uploaded
//		List<WebElement> fileName = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr/td"));// Collect All															// lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileName.size());
//		for (int i = 1; i <= fileName.size(); i++) {
//			String uploadedFileName = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a")).getText();
//			if (uploadedFileName.equals(UploadedFile)) {
//				String actual = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i+ "]/td[2]/a")).getText();
//				Assert.assertEquals(actual, UploadedFile);
//				break;
//			}
//		}
//	}	
//	
//	
//	@Test
//	@TestInfo(tcName="Restore the previously deleted folder from private",feature="File", expectedResult="Folder is restored")
//	public void tc_0219_RestoreFolderFromPrivate() {
//
//		SitePageModel.waitFor(5);
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File
//		SitePageModel.waitFor(5);
//        SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/ul/li[2]/a"));     
//        SitePageModel.waitFor(5);
//
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr"));// Collect All lists
//		SitePageModel.waitFor(2);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]")).getText();
//			System.out.println(eventLists1);
//			if (eventLists1.equals("Trash")) {
//				SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]"));// click Trash
//				break;
//			}
//		}
//		
//		SitePageModel.waitFor(3);
//		List<WebElement> trashfolderList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr/td[2]/a"));// Collect// All// lists
//		SitePageModel.waitFor(2); //
//		System.out.println("trash size" + trashfolderList.size());
//
//		for (int i = 1; i <= trashfolderList.size(); i++) {
//
//			String trashFolder = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[2]")).getText();
//			System.out.println(trashFolder);
//			if (trashFolder.equals(privateFlodername)) {
//				SitePageModel.waitFor(2);
//				SitePageModel.checkbox(driver, By.xpath(".//*[@id='DataTables_Table_0']/tbody/tr[" + i + "]/td[1]/div"));//
//				SitePageModel.waitFor(2);
//				SitePageModel.tryClick(driver,By.xpath("//button[text()='Restore' and @class='btn btn-success btn-mini']")); // Click on Restore
//				SitePageModel.waitFor(3);
//				break;
//			}
//
//		}
//		
//}
//	
//	@Test
//	@TestInfo(tcName="Delete Folder from Trash of Private",feature="File", expectedResult="File is deleted from Trash of Private")
//	public void tc_0220_DeleteFolderFromTrashPrivate() {
//		SitePageModel.scrollElement(driver,By.xpath(".//*[@id='lsb-file']/span"));// Click File		
//		SitePageModel.waitFor(3);	
//		SitePageModel.tryClick(driver,By.linkText("Private"));// Click private tab		
//		SitePageModel.waitFor(3);
//		List<WebElement> fileList = driver.findElements(By.xpath(".//*[@id='DataTables_Table_0']/tr"));// Collect All lists
//		SitePageModel.waitFor(3);
//		System.out.println(fileList.size());
//		for (int i = 1; i <= fileList.size(); i++) {
//			String eventLists1 = driver.findElement(By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]")).getText();
//			System.out.println(eventLists1);
//			if (eventLists1.equals("Trash")) {
//				SitePageModel.waitFor(2);
//				SitePageModel.tryClick(driver,By.xpath(".//*[@id='DataTables_Table_0']/tr[" + i + "]/td[2]"));// click Trash
//				SitePageModel.waitFor(2);
//				break;
//			}
//		}
//		SitePageModel.tryClick(driver,By.xpath("//div[@class='checkbox check-default']"));// click on All check box
//		List<WebElement> checkList = driver.findElements(By.xpath("//div[@class='checkbox check-default']"));// Collect All check box lists
//		SitePageModel.waitFor(20);
//		System.out.println(checkList.size());
//		if(checkList.size()>0){
//			SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click Delete button
//			SitePageModel.waitFor(2);
//			driver.switchTo().alert().dismiss();
//			SitePageModel.waitFor(2);
//			SitePageModel.tryClick(driver,By.xpath("//button[@class='btn btn-danger btn-mini']"));// click Delete button
//			SitePageModel.waitFor(2);
//			driver.switchTo().alert().accept();
//			SitePageModel.waitFor(2);
//		}else{
//			System.out.println("There is no selected item");
//		}
//	}
//	}
//	