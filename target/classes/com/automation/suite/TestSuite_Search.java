package com.automation.suite;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;

public class TestSuite_Search extends TestHelper{
	
	@Test
	@TestInfo(tcName="Regular search",feature="Search", expectedResult="User is able to search Channel, people and content")
	public void tc_1201_RegularSearch() {
		
		driver.navigate().to("https://esn.connectik.org");
//		driver.navigate().to("https://secure.connectik.com");
		SitePageModel.waitFor(10);
		
		SitePageModel.enterText(driver, By.xpath("/html/body/div[1]/div/div[2]/div[1]/ul[2]/li/form/div/input"),"test1");// write on new new comment

//		SitePageModel.enterText(driver, By.xpath(".//*[@id='main']/ul/li[1]/div/div"),"sasote sarker");  //Enter keyword on search field
		SitePageModel.waitFor(2);	

		SitePageModel.tryClick(driver, By.id("global-search"));//click on 'Search' button
		SitePageModel.waitFor(7);
		
		String strExpect1 = "Search Results";
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/h3/span")); //Get "Search Result" Title
		boolean bFound1 = aboutTextElm.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		
		SitePageModel.waitFor(2);	

		String strExpect = "Test1 COC";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/h5/a")); //Get Title of Search result
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
	}	
	
	@Test
	@TestInfo(tcName="Sorted Search Result by relevancy",feature="Search", expectedResult="Search result is displayed as sort by relevancy")
	public void tc_1204_SortedSearchResultByRelevancy() {
	
		driver.navigate().to("https://secure.connectik.com");	
		SitePageModel.waitFor(20);
		
		SitePageModel.enterText(driver, By.className("global-search-form-input"), "Test1 COC");// Enter text on search field
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("global-search")); //Click on search button
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("dropdownMenu123")); //Click on sort by drop down
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[2]/ul/li[1]/a"));
		SitePageModel.waitFor(5);
		String strExpect = "Test1 COC";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/h5/a")); //Get Title of Search result
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		
		
	}	

	@Test
	@TestInfo(tcName="Sorted Search Result by relevancy",feature="Search", expectedResult="Search result is displayed as sort by relevancy")
	
	public void tc_1205_SortedSearchResultByDate() {
	
		driver.navigate().to("https://secure.connectik.com");	
		SitePageModel.waitFor(20);
		
		SitePageModel.enterText(driver, By.className("global-search-form-input"), "Test1 COC");// Enter text on search field
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("global-search")); //Click on search button
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("dropdownMenu123")); //Click on sort by drop down
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[2]/ul/li[2]/a"));// select Date
		SitePageModel.waitFor(5);
		String strExpect = "Test1 COC";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/h5/a")); //Get Title of Search result
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		
	}	

	
	
	
	
	@Test
	@TestInfo(tcName="Blank search",feature="Search", expectedResult="Proper validation message is displayed")
	public void tc_1206_BlankSearch() {
		
		driver.navigate().to("https://secure.connectik.com");	
		SitePageModel.waitFor(20);
		
		SitePageModel.enterText(driver, By.className("global-search-form-input"), "");// Enter text on search field
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("global-search")); //Click on search button
		//SitePageModel.waitFor(5);
		//SitePageModel.getTopWindowHandle(driver);
		SitePageModel.waitFor(2);
		String expectedText = "Type at least 1 characters in order to search.";
	    List<WebElement> popMessage = driver.findElements(By.xpath("//div[@class='messenger-message-inner']"));
	    SitePageModel.waitFor(5);
	    boolean bFoundTitle=popMessage.get(0).getText().equals(expectedText);
	    Assert.assertTrue(bFoundTitle);
	    SitePageModel.waitFor(5);
	}	
	
	

	
	@Test
	@TestInfo(tcName="Filter search result",feature="Search", expectedResult="Search result is diaplayed as filter type")
	public void tc_1207_FilterSearchResult() {
		
		driver.navigate().to("https://secure.connectik.com");	
		SitePageModel.waitFor(20);
		
		SitePageModel.enterText(driver, By.className("global-search-form-input"), "Test1 COC");// Enter text on search field
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("global-search")); //Click on search button
		SitePageModel.waitFor(5);
		//SitePageModel.getTopWindowHandle(driver);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/button")); // Click on filter
		SitePageModel.waitFor(2);
		
		SitePageModel.checkbox(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[1]/div/label"));// check member
		SitePageModel.waitFor(10);
		SitePageModel.checkbox(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[2]/div/label"));// check team
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/div[3]/button"));// Click apply
		SitePageModel.waitFor(10);
		String strExpect = "Test1 COC";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/h5/a")); //Get Title of Search result
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
	}	
	
	
	@Test
	@TestInfo(tcName="Invalid Search",feature="Search", expectedResult="proper validation message is diaplayed")
	public void tc_1208_InvalidSearch() {
		
		driver.navigate().to("https://secure.connectik.com");	
		SitePageModel.waitFor(20);
		
		SitePageModel.enterText(driver, By.className("global-search-form-input"), "t");// Enter text on search field
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("global-search")); //Click on search button
		SitePageModel.waitFor(5);
		//SitePageModel.getTopWindowHandle(driver);
		
		
		SitePageModel.waitFor(10);
		String strExpect = "No results found!";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div")); //Get validation message
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
	}	
	
	
	
//	@Test
//	@TestInfo(tcName="Advanced search",feature="Search", expectedResult="User is able to search from advance search page")
//	public void tc_1202_AdvancedSearch() {
//		
//		driver.navigate().to("https://esn.connectik.org");
//		SitePageModel.waitFor(20);
//
//		SitePageModel.tryClick(driver, By.id("global-search-advanced"));//click on ' Advance Search' button
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[1]/div/div[2]/a"));//click on ' toggle all' button
//		SitePageModel.waitFor(3);
//					
//		SitePageModel.enterText(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[2]/div/div/input"),"User1");  //Enter keyword on search field below toggle all
//		SitePageModel.waitFor(2);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[2]/div/div/span/button"));//click on ' search icon' button
//		SitePageModel.waitFor(5);
//
//		String strExpect = "User User 1";
//		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/h5/a")); //Get Title of Search result
//		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
//		Assert.assertTrue(bFound);
//		
//		SitePageModel.waitFor(2);	
//
//		String strExpect2 = "View Profile";
//		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/div[2]/ul/li/a")); //Get Title of Search result
//		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
//		Assert.assertTrue(bFound2);
//	}	
//	
	
	@Test
	@TestInfo(tcName="Validate search result",feature="Search", expectedResult="Proper message is displayed when search result return nothing")
	public void tc_1203_AnalyzeSearchResults() {
		
		driver.navigate().to("https://secure.connectik.com");
		SitePageModel.waitFor(20);

		SitePageModel.tryClick(driver, By.id("global-search-advanced"));//click on ' Advance Search' button
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[1]/div/div[2]/a"));//click on ' toggle all' button
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[1]/div/div[1]/div[1]/div/div/label"));//click on 'Emails checkbox' button
		SitePageModel.waitFor(2);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[2]/div/div/input"),"Test1 COC");  //Enter keyword on search field below toggle all
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/form/div[2]/div/div/span/button"));//click on ' search icon' button
		SitePageModel.waitFor(5);

		String strExpect = "View Profile";
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/ul/li[1]/div[2]/div/div[2]/ul/li/a")); //Get Title of Search result
		boolean bFound = aboutTextElm1.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);	
		SitePageModel.waitFor(2);	

	}	
}
	
	
