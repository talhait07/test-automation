package com.automation.suite;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;

public class TestSuite_Login extends TestHelper{
	
	
	@Test
	@TestInfo(tcName="Login with invalid password",feature="Login", expectedResult="Validatation message displayed successfully")
	public void tc_0101_loginInvalidPasswordTest() {

		SitePageModel.maximizeWindow(driver);		
		SitePageModel.waitFor(50);
		
		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com");
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.id("password"),"qwerty"); //Enter wrong password on login panel
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(3);  
		
		String strExpect = "Invalid username or password.";

		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/ul/li[1]/div/div"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
	}
	
	@Test
	@TestInfo(tcName="Login with invalid username",feature="Login", expectedResult="Validatation message displayed successfully")
	public void tc_0102_loginInvalidUserNameTest() {

		SitePageModel.waitFor(10);
		
		SitePageModel.enterText(driver, By.id("username"),"lorem ipsum");		
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.id("password"),"qwerty");
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(5);  
		
		String strExpect = "Invalid username or password.";
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/ul/li[1]/div/div"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
	}
	
	@Test
	public void tc_103_ForgotPassword() {				

		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Forgot Password"));//click on 'Forgot Password' link
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver, By.name("email"),"user7@rootnext.com");		
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'Recover' button
		SitePageModel.waitFor(5);
				
		String strExpect = "Please check your email for instructions.";
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[1]/div/div/div/div/div[1]/p[2]")); //Get "Search Result" Title
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(2);		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div/div/div/div[2]/div/div/a"));// accept confirmation
		SitePageModel.waitFor(5);	
	}
	
	@Test
	public void tc_104_loginTest() {
		
		SitePageModel.enterText(driver, By.id("username"),"user4@rootnext.com");
		SitePageModel.waitFor(2);
		
		SitePageModel.enterText(driver, By.id("password"),"T@skor13");
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(20);	

		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(50);	
		
		SitePageModel.elementIsVisible(driver, By.xpath("/html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[2]/div/h2"));//Click 'Sign Out'
		SitePageModel.waitFor(5);
	}	
	
	@Test
	public void loginWithTenantTest() {
		
		SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[3]/ul/li[7]/a"));//click on 'log out link
		SitePageModel.waitFor(35);
		
		SitePageModel.enterText(driver, By.id("username"),"user4@rootnext.com");		
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.id("password"),"T@skor13");
		SitePageModel.waitFor(2);  

		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(5);  
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='button']"));//click on 'log in button'
		SitePageModel.waitFor(25);  
	}	
	
	@Test
	public void loginWithTenantKeepLoggedinTest() {
			
		SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[3]/ul/li[7]/a"));//click on 'log out link
		SitePageModel.waitFor(25);
		
		SitePageModel.enterText(driver, By.id("username"),"user4@rootnext.com");		
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.id("password"),"T@skor13");
		SitePageModel.waitFor(2);  

		SitePageModel.tryClick(driver, By.xpath("//label[text()='Keep me signed in']"));// Click "keep me sign me" check box
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(5);  
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='button']"));//click on 'log in button'
		SitePageModel.waitFor(25);  
		
		SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link

	}	
	
	@Test
	@TestInfo(tcName="Login with keep me sign in",feature="Login", expectedResult="User is logged in successfully")
	public void tc_0103_loginWithKeepMeLoggedInTest() {	
		
		SitePageModel.waitFor(15);
		
		SitePageModel.maximizeWindow(driver);		

		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com");		
//		SitePageModel.enterText(driver, By.id("username"),"user6@rootnext.com");		
		
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("password"),"T@skor13");
		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.xpath("//label[text()='Keep me signed in']"));// Click "keep me sign me" check box
//		SitePageModel.waitFor(5); 


		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
		SitePageModel.waitFor(15);  
		
//		List <WebElement> menuIconList = driver.findElements(By.xpath("//div[@class='iconset top-menu-toggle-dark']")); //Find All (+) icon and click on 1st
//		SitePageModel.waitFor(2);
//		menuIconList.get(1).click();		
//		SitePageModel.waitFor(2);
				
//		SitePageModel.elementIsVisible(driver, By.xpath("/html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[2]/div/h2"));//Click 'Sign Out'
//		SitePageModel.waitFor(1);
//		SitePageModel.enterText(driver,By.xpath(".//input[contains(@class, 'form-control') and (@placeholder='Invite Member...')]"),"user5");//Enter value 'Invite member' field

		if(SitePageModel.elementIsVisible(driver, By.xpath("//i[@class='fa fa fa-users']")))
//			fa fa-angle-down Quick Links
//            SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa fa-users']"));//("Click on finish"));
		{			
			
			System.out.println("if loop in login");
		}
		
		
		else{
			
			System.out.println("else loop in login");
			
			List <WebElement> menuIconList2 = driver.findElements(By.xpath("//div[@class='iconset top-menu-toggle-dark']")); //Find All (+) icon and click on 1st
			SitePageModel.waitFor(2);
			menuIconList2.get(1).click();		
			SitePageModel.waitFor(2);
			
		}
			
	}	
}