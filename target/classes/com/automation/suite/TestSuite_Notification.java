package com.automation.suite;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;

public class TestSuite_Notification extends TestHelper{
	
	@Test
	@TestInfo(tcName="Notification mark as read",feature="Notifications", expectedResult="Notification unread number is disappeared")
	public void tc_1101_NotificationsMarkAsReadTest() {
		
		driver.navigate().to("https://secure.connectik.com");		
		SitePageModel.waitFor(20);
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("/html/body/div[1]/div/div[2]/div[2]/ul/li[4]/div/h3/div/button[1]"));
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
	}	
	
	@Test
	@TestInfo(tcName="View all notification",feature="Notifications", expectedResult="All notification is displayed")
	public void tc_1102_ReadNotificationsViewAllTest() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("/html/body/div[1]/div/div[2]/div[2]/ul/li[4]/div/h3/div/button[2]"));
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
	}
	
	@Test
	@TestInfo(tcName="Notification for new user", feature="My profile", expectedResult="Only new user is got no current notification message")
	public void tc_1103_ValidateNoNewNotifications() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
		//String strResultText = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/ul/li[4]/div/div/div")).getText();
		//System.out.println(strResultText);	
		String strExpect = "You have no courrent notification";
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("/html/body/div[1]/div/div[2]/div[2]/ul/li[4]/div/div/div"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);	
		if(bFound){
			Assert.assertTrue(bFound);
			System.out.println(bFound);
		}else{
			System.out.println("This test case should be skiped");
		}
	}
	
	@Test
	public void tc_1104_readNotificationsFrameTest() {
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
		SitePageModel.waitFor(2);
		//SitePageModel.tryClick(driver, By.className("description"));
		SitePageModel.waitFor(2);
		//SitePageModel.waitFor(10);
		 List<WebElement> linkElements = driver.findElements(By.className("message-wrapper"));	              
	        for(int i = 0; i< linkElements.size(); i++){
	        	SitePageModel.tryClick(driver, By.xpath("description"));
	        	SitePageModel.waitFor(2);
	        	SitePageModel.tryClick(driver, By.id("toggle-notification-center"));
	        	SitePageModel.waitFor(7);
	            }
		
	}	
}
	
	
