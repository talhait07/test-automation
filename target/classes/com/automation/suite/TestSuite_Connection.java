package com.automation.suite;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;

public class TestSuite_Connection extends TestHelper {
	
	@Test
	@TestInfo(tcName="Check connection exists", feature="My Connections", expectedResult="No connection found message is displayed if there is no connection")
	public void tc_1301_CheckNoConnection() {
		
		driver.navigate().to("https://secure.connectik.com");
		SitePageModel.waitFor(20);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		//SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[3]/ul/li[3]/a")); // Click on Connection
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);
		Boolean bElementExists=SitePageModel.doesElementExist(driver, By.xpath(".//*[@id='connections']/p"));
		if(bElementExists){
		String strExpect = "No connections found!";		
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='connections']/p")); // Find Text - No connections found!
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);	
		Assert.assertTrue(bFound);
		System.out.println(bFound);
		}else{
			System.out.println("This test cases should be skiped");
		}
	}	
	
	
	@Test
	@TestInfo(tcName="View member list on Member tab",feature="My Connections", expectedResult="Member list is displayed")
	public void tc_1302_MemberList() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		int linkElements = driver.findElements(By.className("user-big-tile")).size(); // Count member 
		System.out.println(linkElements);
		SitePageModel.waitFor(2);
	}	
	
	@Test
	@TestInfo(tcName="Search member", feature="My Connections", expectedResult="User is able to search member")
	public void tc_1303_MemberListSearch() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); // Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
		SitePageModel.waitFor(10); 
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button[1]")); //Click on cross button
		SitePageModel.waitFor(10);
	}	
	
	
	@Test
	@TestInfo(tcName="Add and Remove member", feature="My Connections", expectedResult="Member is added and removed")
	public void tc_1304_AddRemoveMember() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); // Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
		SitePageModel.waitFor(3);
		//SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/div[1]/div/table/tbody/tr[2]/td[1]/button")); //Click on add member button		
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Add Connection' and @class='btn btn-block btn-success btn-mini m-b-10']"));//("Click on Add button");
		SitePageModel.waitFor(5);
		//SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/div[1]/div/table/tbody/tr[2]/td[1]/button")); //Click on Cancel button
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Cancel' and @class='btn btn-block btn-danger btn-mini m-b-10']"));//("Click on Chancel button");
		SitePageModel.waitFor(10);
	}	
	
	@Test
	@TestInfo(tcName="View member profile", feature="My Connections", expectedResult="Member profile is opened")
	public void tc_1305_ViewProfileMember() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); // Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.partialLinkText("View Profile")); //Click on View profile		
		SitePageModel.waitFor(10);
	}	
	
	
	@Test
	@TestInfo(tcName="Send message using chat", feature="My Connections", expectedResult="User is able to chat with a member")
	public void tc_1306_SendMessageMember() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); // Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.partialLinkText("Send Message"));	//Click on Send Message 	
		SitePageModel.waitFor(30);
		SitePageModel.enterText(driver, By.id("chat-textarea"),"Hi, What's Up");		
		driver.findElement(By.id("chat-textarea")).sendKeys(Keys.ENTER); // Press Enter
		SitePageModel.waitFor(10);				     
        SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[1]/div[1]/div[1]/a[3]/i"));//("Click on setting link");
        SitePageModel.waitFor(10);
        SitePageModel.tryClick(driver, By.partialLinkText("Finish"));//("Click on finish"));
        SitePageModel.waitFor(10);
        //SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
        SitePageModel.tryClick(driver, By.xpath("//button[text()='OK' and @class='btn btn-primary']"));//("Click on ok button of poll");
		SitePageModel.waitFor(10);
	}
	
	@Test
	@TestInfo(tcName="Check Connection invitation", feature="My Connections", expectedResult="Member is got invitations ")
	public void tc_1307_AddMemberCheck() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); // Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Add Connection' and @class='btn btn-block btn-success btn-mini m-b-10']"));//("Click on Add button");
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);	
		SitePageModel.enterText(driver, By.id("username"),"test1.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);
		
	}	
	@Test
	@TestInfo(tcName="Decline Connection invitation", feature="My Connections", expectedResult="Member is able to decline invitation")
	public void tc_1308_AddMemberDecline() {
		
		SitePageModel.waitFor(5);	
		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		

		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(5);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); /// Search by member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); // Click on Search button
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Cancel' and @class='btn btn-block btn-danger btn-mini m-b-10']"));//("Click on Chancel button");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Add Connection' and @class='btn btn-block btn-success btn-mini m-b-10']"));//("Click on Add button");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options"));
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);	
		SitePageModel.enterText(driver, By.id("username"),"test1.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		


		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);

		
		SitePageModel.tryClick(driver, By.id("user-options"));//Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Decline' and @class='btn btn-danger btn-small']")); // Click on Decline button
		SitePageModel.waitFor(10);

	}	
	@Test
	@TestInfo(tcName="Accept Connection invitation", feature="My Connections", expectedResult="Member is able to accept invitation")
	public void tc_1309_AddMemberAccept() {
		
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);		
		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		

		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(10);	
		SitePageModel.tryClick(driver, By.id("tab-members"));
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test1"); //Search by Member
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on Search button
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Add Connection' and @class='btn btn-block btn-success btn-mini m-b-10']"));//("Click on Add button");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);	
		SitePageModel.enterText(driver, By.id("username"),"test1.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		
		
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);
		
		
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Accept' and @class='btn btn-success btn-small']")); //Click on Accept button
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("Logout")); //Click on Logout
		SitePageModel.waitFor(10);	
		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com"); //Enter correct username on login panel				
		SitePageModel.enterText(driver, By.id("password"),"T@skor13"); //Enter  password on login panel		


		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
		SitePageModel.waitFor(5); 
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
 		SitePageModel.waitFor(25);

		
		SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
		SitePageModel.tryClick(driver, By.partialLinkText("My Connections")); // Click on Connection
		SitePageModel.waitFor(15);
		SitePageModel.tryClick(driver, By.xpath("//button[text()='Remove Connection' and @class='btn btn-danger btn-small m-t-5']")); //Click on Removed button
		SitePageModel.waitFor(10);
	}	
}
