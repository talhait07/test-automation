package com.automation.util;

import org.openqa.selenium.WebDriver;

import com.automation.pagemodel.SitePageModel;

public class AppConstant {
	
	private AppConstant () { } // prevents instantiation
	
	private static String USER_DIR = System.getProperty("user.name");
	private static WebDriver driver;
	//private static WebDriver driver;
	public static final String CURRENT_DIR = System.getProperty("user.dir");
	public static final String userName= SitePageModel.randomStringGenerator(driver);
	public static final String ERROR_LOG_PATH = CURRENT_DIR+"/assets/log/error.log";
	public static final String TEST_LOG_PATH = CURRENT_DIR+"/assets/log/";
	public static final String SETTING_PATH = CURRENT_DIR+"/config/setting.conf";
	public static final String TEST_SUITE_XML = CURRENT_DIR+"/config/";
	public static final String SCREEN_SHOT_DIR = CURRENT_DIR+"/assets/screenshots/";
	public static final String TESTNG_REPORT_DIR = CURRENT_DIR+"/assets/";
	public static final String RESOURCE_DIR = CURRENT_DIR+"/resource/";
	public static final String CHROME_DRIVER = CURRENT_DIR+"//driver//chromedriver";
//	public static final String CHROME_DRIVER = CURRENT_DIR+"\\driver\\chromedriver.exe";
	public static final String IE_DRIVER = CURRENT_DIR+"\\driver\\IEDriverServer.exe";
	public static final String DOWNLOAD_DIR = "/Users/"+ USER_DIR +"/Downloads/";
	public static final String TEST_CACHE_PATH = CURRENT_DIR+"/assets/log/testCache.txt";
	public static final String TEST_DATA_PATH = CURRENT_DIR + "/testData/";
	public static final String TEST_FILE_NAME = "TestData.xlsx";
	
	public static final String AUTOIT_EXE_PATH = CURRENT_DIR+"\\autoit\\";
	
	public static final String TEST_SUMMARY_PATH = CURRENT_DIR+"\\assets\\";
	public static final String IMAGE_PATH=CURRENT_DIR+"/test-image/";
}
