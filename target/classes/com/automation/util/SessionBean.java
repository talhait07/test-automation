package com.automation.util;

import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class SessionBean {

	private Map<String, String> temporaryValue = new HashMap<String, String>();
	
	private Connection conn = null;
	private Statement stmt = null;

	/**
	 * @return the temporaryValue
	 */
	public Map<String, String> getTemporaryValue() {
		return temporaryValue;
	}

	/**
	 * @param temporaryValue the temporaryValue to set
	 */
	public void setTemporaryValue(Map<String, String> temporaryValue) {
		this.temporaryValue = temporaryValue;
	}

	/**
	 * @return the conn
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param conn the conn to set
	 */
	public void setConn(Connection conn) {
		this.conn = conn;
	}

	/**
	 * @return the stmt
	 */
	public Statement getStmt() {
		return stmt;
	}

	/**
	 * @param stmt the stmt to set
	 */
	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}
	
	
	
}
