package com.automation.suite;
import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.AppConstant;
public class TestSuite_Filter extends TestHelper {
	
	
	String teamName = "1st M tested team";
	String teamDis = "This is my first tested team";
	
	
	@Test
	@TestInfo(tcName="Filter Activity feed by Post",feature="Activity Filter", expectedResult="Activity Feed is Filtered by post")
	public void tc_0902_ActivityFilterByPost(){
		
//		driver.navigate().to("https://secure.connectik.com");
//	    SitePageModel.waitFor(20);
	
		SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
		SitePageModel.waitFor(2);
	
		SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox
		SitePageModel.waitFor(2);
	
		SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
		SitePageModel.waitFor(7);
	
		String strExpectedToolTipText="Post";
		List<WebElement> listPostElement = driver.findElements(By.xpath("//div[@class='cbp_tmicon animated bounceIn']"));
		
		SitePageModel.waitFor(3);
		for (int i=0;i<listPostElement.size();i++){
	
		boolean bFoundToolTipText = listPostElement.get(i).getAttribute("title").equals(strExpectedToolTipText);
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundToolTipText);
		}
	}
	
	@Test
	@TestInfo(tcName="Filter Activity feed by Event",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Event")
	public void tc_0903_ActivityFilterByEvent(){

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
		SitePageModel.waitFor(2);
	
		SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Events']"));//Select Event checkbox
		SitePageModel.waitFor(2);
	
		SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
		SitePageModel.waitFor(7);
	
		String strExpectedToolTipText="No one has made a post yet.";
		List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
		
		SitePageModel.waitFor(2);
	
		boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
		if(bFoundToolTipText){
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundToolTipText);
			}else{
				
				String strExpectedToolTipText2="Event";
				List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon success animated bounceIn']"));
				SitePageModel.waitFor(2);
				for (int i=0;i<listPostElement2.size();i++){
		
				boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
				SitePageModel.waitFor(2);
				Assert.assertTrue(bFoundToolTipText2);
				}
			}
	}	
	
	
	
	
	
	
	@Test
	@TestInfo(tcName="Filter Activity feed by Task",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Task")
	public void tc_0904_ActivityFilterByTasks(){

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Tasks']"));//Select Tasks checkbox
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
		SitePageModel.waitFor(7);
		
		String strExpectedToolTipText="No one has made a post yet.";
		List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
		SitePageModel.waitFor(1);
		boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
		if(bFoundToolTipText){
		SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText);
				}else{
				
				String strExpectedToolTipText2="Task";
				List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon warning animated bounceIn']"));
				SitePageModel.waitFor(2);
				for (int i=0;i<listPostElement2.size();i++){
		
				boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
				SitePageModel.waitFor(1);
				Assert.assertTrue(bFoundToolTipText2);
				}
			}
	}	
	
	
	
		@Test
		@TestInfo(tcName="Filter Activity feed by Polls",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Polls")
		public void tc_0905_ActivityFilterByPolls(){
		
			SitePageModel.waitFor(5);
		
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
		
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Polls']"));//Select Polls checkbox
			SitePageModel.waitFor(2);
		
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
		
			String strExpectedToolTipText="No one has made a post yet.";
			List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
			SitePageModel.waitFor(2);
			boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
			if(bFoundToolTipText){
			SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText);
	}else{
		
		String strExpectedToolTipText2="Poll";
		List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon info animated bounceIn']"));
		SitePageModel.waitFor(2);
		for (int i=0;i<listPostElement2.size();i++){

		boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundToolTipText2);
			}
		}
	}	
	
		
		@Test
		@TestInfo(tcName="Filter Activity feed by Emails",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Emails")
		public void tc_0906_ActivityFilterByAnnouncements(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
		
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Announcements']"));//Select Announcements checkbox
			SitePageModel.waitFor(2);
		
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
		
			String strExpectedToolTipText="No one has made a post yet.";
			List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
			SitePageModel.waitFor(2);
			boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
			if(bFoundToolTipText){
			SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText);
		}else{
			
			String strExpectedToolTipText2="Email";
			List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon info animated bounceIn']"));
			SitePageModel.waitFor(2);
			for (int i=0;i<listPostElement2.size();i++){
	
			boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
			SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText2);
			}
		}
	}
		
		@Test
		@TestInfo(tcName="Filter Activity feed by Chat",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Chat")
		public void tc_0907_ActivityFilterByChat(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Chats']"));//Click Chats Checkbox
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
		
			String strExpectedToolTipText="No one has made a post yet.";
			List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
			SitePageModel.waitFor(2);
			boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
			if(bFoundToolTipText){
			SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText);
				}else{
					
					String strExpectedToolTipText2="Chat";
					List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon success animated bounceIn']"));
			
					SitePageModel.waitFor(2);
					for (int i=0;i<listPostElement2.size();i++){
			
					boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
					SitePageModel.waitFor(2);
					Assert.assertTrue(bFoundToolTipText2);
					}
				}
	}	
		

		@Test
		@TestInfo(tcName="Filter Activity feed by Files",feature="Activity Filter", expectedResult="Activity Feed is Filtered by Files")
		public void tc_0908_ActivityFilterByFiles(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);

			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Files']"));//Select Files checkbox
			SitePageModel.waitFor(2);
		
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
		
			String strExpectedToolTipText="No one has made a post yet.";
			List<WebElement> listPostElement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[2]/div/div"));
			SitePageModel.waitFor(2);
			boolean bFoundToolTipText = listPostElement.get(0).getText().equals(strExpectedToolTipText);
			if(bFoundToolTipText){
			SitePageModel.waitFor(2);
			Assert.assertTrue(bFoundToolTipText);

			}else{
				String strExpectedToolTipText2="File";
				List<WebElement> listPostElement2 = driver.findElements(By.xpath("//div[@class='cbp_tmicon success animated bounceIn']"));
				
				SitePageModel.waitFor(2);
				for (int i=0;i<listPostElement2.size();i++){
		
				boolean bFoundToolTipText2 = listPostElement2.get(i).getAttribute("title").equals(strExpectedToolTipText2);
				SitePageModel.waitFor(2);
				Assert.assertTrue(bFoundToolTipText2);
				}
			}
		}
		
		
		@Test
		@TestInfo(tcName="Reset All Filter",feature="Activity Filter", expectedResult="Default Filter is displayed")
		public void tc_0909_ActivityFilterByResetAll(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Announcements']"));//Select Announcements checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Events']"));//Select Event checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Tasks']"));//Select Tasks checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Polls']"));//Select Polls checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Chats']"));//Click Chats Checkbox
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Files']"));//Click Files Checkbox
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);
	}	
	
		
		@Test
		@TestInfo(tcName="ResetAll from Post Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0910_ActivityFilterByResetPost(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);

	}	
		
		@Test
		@TestInfo(tcName="ResetAll from Announcements Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0911_ActivityFilterByResetAnnouncements(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Announcements']"));//Select Announcements checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);

	}	
		
		@Test
		@TestInfo(tcName="ResetAll from Events Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0912_ActivityFilterByResetEvents(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Events']"));//Select Event checkbox
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);
	}	
		
		@Test
		@TestInfo(tcName="ResetAll from Polls Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0913_ActivityFilterByResetPolls(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[2]/div[4]/div/label"));//Select Polls checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);

	}	
		
		@Test
		@TestInfo(tcName="ResetAll from Tasks Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0914_ActivityFilterByResetTasks(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[2]/div[5]/div/label"));//Select Tasks checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(4);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);

	}
		
		@Test
		@TestInfo(tcName="ResetAll from Chat",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0916_ActivityFilterByResetChat(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Chats']"));//Click Chats Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);

	}

		@Test
		@TestInfo(tcName="ResetAll from File Filter",feature="Activity Filter", expectedResult="All filter checkbox is unchecked ")
		public void tc_0917_ActivityFilterByResetFile(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Files']"));//Click Files Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/div/form/div[3]/a[2]"));// Click Reset button
			SitePageModel.waitFor(5);
	}
		
		@Test
		@TestInfo(tcName="Edit post from Activity feed",feature="Activity Filter", expectedResult="Post is updated")
		public void tc_0918_ActivityFilterByPostEdit() throws AWTException{
			//Code for team creation
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-external"));//Open Activity
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.partialLinkText("Joined")); // Click on create channel icon Add Team
			SitePageModel.waitFor(5);
			SitePageModel.enterText(driver,By.xpath("//input[@class='form-control teamsCustom']"), teamName);
			SitePageModel.waitFor(2);
			driver.findElement(By.xpath("//input[@class='form-control teamsCustom']")).sendKeys(Keys.ENTER);
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver,By.xpath("//i[@class='fa fa-lg fa-star-o']"));
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("postForm"));//Click on post icon
			SitePageModel.waitFor(3);		
			SitePageModel.enterText(driver, By.tagName("textarea"),"Hi every one, Today we will start new testing");//Enter  text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createPostButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver, By.linkText("Edit"));// Click Edit
			SitePageModel.waitFor(3);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.enterText(driver, By.tagName("textarea"),"New Automation testing is started from Today");// Enter text into text area		
			SitePageModel.waitFor(1);		
			SitePageModel.tryClick(driver, By.id("updatePostButton"));//Click on Update button
			SitePageModel.waitFor(8);

	}	
		
		@Test
		@TestInfo(tcName="Cancel the editing activity of post",feature="Activity Filter", expectedResult="Editing is canceled")
		public void tc_0919_ActivityFilterByPostEditCancel(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.partialLinkText("Edit"));// Click Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.enterText(driver, By.tagName("textarea"),"New Automation testing is started from Today");// Enter text into text area
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-default btn-small']"));//Click on Cancel button
			SitePageModel.waitFor(5);
	}	
		
		@Test
		@TestInfo(tcName="Edit post with image",feature="Activity Filter", expectedResult="Post is updated with image")
		public void tc_0920_ActivityFilterByPostEditWithImage() throws AWTException{
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Edit"));// Click Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.enterText(driver, By.tagName("textarea"),"New Automation testing is started from Today");// Enter text into text area
			
			SitePageModel.waitFor(2);
			String strUploadImage = AppConstant.IMAGE_PATH + "testimg2.jpg";
			WebElement upload = driver.findElement(By.id("attachImageButton"));
		    SitePageModel.waitFor(2);
		    upload.sendKeys(strUploadImage);		    
		    SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("updatePostButton"));//Click on Update button
			SitePageModel.waitFor(7);
	}
		
		@Test
		@TestInfo(tcName="Cancel the deleting operation of Post",feature="Activity Filter", expectedResult="Deleting operation of is canceled")
		public void tc_0921_ActivityFilterByPostDeleteCancel(){
			
			SitePageModel.waitFor(5);	
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click Delete
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Cancel')]"));// Click OK button of confirmation
			SitePageModel.waitFor(5);	
	}
		
		@Test
		@TestInfo(tcName="Delete a Post from Activity Feed",feature="Activity Filter", expectedResult="Post is deleted")
		public void tc_0922_ActivityFilterByPostDelete(){
			
			SitePageModel.waitFor(5);	
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click Delete
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
			SitePageModel.waitFor(5);	
	}
		@Test
		@TestInfo(tcName="Edit a Post from Activity Feed",feature="Activity Filter", expectedResult="Post is updated")
		public void tc_0923_ActivityEditPost() throws AWTException{
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='groupFilterPanel']/button"));//Click Filter
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='checkbox']/label[text()='Posts']"));//Click Posts Checkbox	
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//div[@class='filter-buttons']/a[text()='Apply']"));//Click Apply
			SitePageModel.waitFor(7);	
		    SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.partialLinkText("Edit"));// Click Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.enterText(driver, By.tagName("textarea"),"New Automation testing is started from Today");// Enter text into text area		
			SitePageModel.tryClick(driver, By.id("updatePostButton"));//Click on Update button
			SitePageModel.waitFor(5);	
	}
		
		
		@Test
		@TestInfo(tcName="Cancel the Editing operation of Post",feature="Activity Filter", expectedResult="Editing operation is canceled")
		public void tc_0924_ActivityEditPostCancel(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("postForm"));//Click on post icon
			SitePageModel.waitFor(3);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='postCreateForm']/div[1]/textarea"),"Hi every one, Today we will start new testing5673");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createPostButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Edit"));// Click Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.enterText(driver, By.tagName("textarea"),"New Automation testing is started from Today");// Enter text into text area		
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-default btn-small']"));//Click on Cancel button
			SitePageModel.waitFor(5);
	}
		
		
		@Test
		@TestInfo(tcName="Cancel the Delete operation of Post",feature="Activity Filter", expectedResult="Deleting operation is canceled")
		public void tc_0925_ActivityEditPostDeleteCancel(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.id("postForm"));//Click on post icon
			SitePageModel.waitFor(3);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='postCreateForm']/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createPostButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click Delete
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Cancel')]"));// Click OK button of confirmation
			SitePageModel.waitFor(5);
	}
		
		
		@Test
		@TestInfo(tcName="Delete Post from Activity Feed",feature="Activity Filter", expectedResult="Post is deleted")
		public void tc_0926_ActivityEditPostDelete(){
			
			SitePageModel.waitFor(5);	
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(8);
			SitePageModel.tryClick(driver, By.id("postForm"));//Click on post icon
			SitePageModel.waitFor(3);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='postCreateForm']/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createPostButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click Delete
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
			SitePageModel.waitFor(5);
	}
		
		
		@Test
		@TestInfo(tcName="Edit Announcement from Activity Feed",feature="Activity Feed", expectedResult="Announcement is updated")
		public void tc_0927_ActivityAnnouncementEdit(){
			
			SitePageModel.waitFor(5);	
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.id("announcementForm"));//Click on post icon
			SitePageModel.waitFor(2);
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by M");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Edit"));// Click on Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.name("title"));// Clear text from text area
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by M2");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.tagName("textarea"),"Hi every one, Today we will start old testing");
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("updateAnnouncementButton"));// Click on Update button
			SitePageModel.waitFor(5);
	}
		
		
		@Test
		@TestInfo(tcName="Cancel the Edit operation of Announcement",feature="Activity Feed", expectedResult="Editing operation is canceled")
		public void tc_0928_ActivityAnnouncementEditCancel(){
			
			SitePageModel.waitFor(5);	
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.id("announcementForm"));//Click on post icon
			SitePageModel.waitFor(2);
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by Min");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Edit"));// Click on Edit
			SitePageModel.waitFor(2);
			SitePageModel.clearTxt(driver, By.name("title"));// Clear text from text area
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by M2");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.clearTxt(driver, By.tagName("textarea"));// Clear text from text area
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.tagName("textarea"),"Hi every one, Today we will start old testing");
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-default btn-small']"));// Click on cancel button
			SitePageModel.waitFor(5);
	}

		@Test
		@TestInfo(tcName="Cancel the Delete operation of Announcement",feature="Activity Filter", expectedResult="Deleting operation is canceled")
		public void tc_0929_ActivityAnnouncementDeleteCancel(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(8);
			SitePageModel.tryClick(driver, By.id("announcementForm"));//Click on post icon
			SitePageModel.waitFor(3);
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by Min");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click on Delete
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[@class='bootbox-close-button close']"));// Click on Cross
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click on Edit
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Cancel')]"));// Click OK button of confirmation
			SitePageModel.waitFor(2);
			
	}
		
		
		@Test
		@TestInfo(tcName="Delete Announcement from Activity Feed",feature="Activity Filter", expectedResult="Announcement is deleted")
		public void tc_0930_ActivityAnnouncementDelete(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-external-list']/ul[1]/li[1]/a/span"));//Click on team
			SitePageModel.waitFor(8);
			SitePageModel.tryClick(driver, By.id("announcementForm"));//Click on post icon
			SitePageModel.waitFor(4);
			SitePageModel.enterText(driver, By.name("title"),"New QA automation by Min");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div[1]/textarea"),"Hi every one, Today we will start new testing");//Enter text into text filed	
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));// Click on Create button	
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.id("lsb-home"));//Open Activity
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.linkText("Delete"));// Click on Delete
			SitePageModel.waitFor(2);				
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
			SitePageModel.waitFor(2);
			
			
			/*
			 code for Deleting TEAM
			 */
			SitePageModel.tryClick(driver, By.id("lsb-external"));//Open Activity
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.partialLinkText("Joined")); // Click on create channel icon Add Team
			SitePageModel.waitFor(5);
			SitePageModel.enterText(driver,By.xpath("//input[@class='form-control teamsCustom']"), teamName);
			SitePageModel.waitFor(2);
			driver.findElement(By.xpath("//input[@class='form-control teamsCustom']")).sendKeys(Keys.ENTER);
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver,By.xpath("//i[@class='fa fa-lg fa-star']"));
			SitePageModel.waitFor(5);
		}		
}