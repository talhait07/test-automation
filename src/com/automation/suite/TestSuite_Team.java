package com.automation.suite;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.AppConstant;

public class TestSuite_Team extends TestHelper{
	
	
	String name=SitePageModel.randomStringGenerator(driver);
	String team_name="Test team "+name;
	String nameEdit=SitePageModel.randomStringGenerator(driver);
	String team_name_edit="Test team edited "+nameEdit;
	String teamPost=SitePageModel.randomStringGenerator(driver);
	String team_post_name="Test team post "+teamPost;
	String teamAnnouncement=SitePageModel.randomStringGenerator(driver);
	String team_announcement_name="Test team announcement "+teamAnnouncement;
	String teamEvent=SitePageModel.randomStringGenerator(driver);
	String team_event_name="Test team event "+teamEvent;
	String teampoll=SitePageModel.randomStringGenerator(driver);
	String team_poll_name="Test team poll "+name;
	String teamTask=SitePageModel.randomStringGenerator(driver);
	String team_task_name="1"+"Test team task "+teamTask;
	String teamChat=SitePageModel.randomStringGenerator(driver);
	String team_Chat_name="Test team chat "+teamTask;
	String teamFile=SitePageModel.randomStringGenerator(driver);
	String team_File_name="Test team file "+teamFile;
	
	
		
//	@Test
//	@TestInfo(tcName="Create team with required field",feature="Team", expectedResult="Team is created using required field")
	public void tc_0401_CreatePrivateTeamWithReqField() {

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("lsb-external")); //Click on public Teams
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.name("title"),team_name);//Enter text in 'Team name' field
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("createGroupButton"));//Click on 'create team' button
		SitePageModel.waitFor(35);
		
		boolean bFoundTeamName = true;
		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));//collect all team names

		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_name)) {
				 bFoundTeamName = true;
				break;
			}
		}

		Assert.assertTrue(bFoundTeamName);
		
		SitePageModel.waitFor(5);

		
	}

	@Test
	@TestInfo(tcName="Invite member to a team",feature="Team", expectedResult="member is included into team")
	public void tc_0402_InviteMember() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

		
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(5);
//		
//		try {
//		  for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
		
		
//		SitePageModel.waitFor(10);
		
		//SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[1]/a"));

		SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-details']/div/div/div/a"));// ("Click on Invite members"));

		SitePageModel.waitFor(3);

		SitePageModel.enterText(driver,By.id("externalMailInput"), "sayedul@gmail.com");//Enter email
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
		SitePageModel.waitFor(2);


		SitePageModel.enterText(driver,By.name("title"),"test4");//enter value 'invite member'
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.id("user-test4.coc@rootnext.com"));// Click on image
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member' 
		SitePageModel.waitFor(30);
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='sidebar-tile-members']/div[2]/div/div/a[2]/img"));//check image of right side panel
 		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Update team member",feature="Team", expectedResult="Team member is updated ")
	public void tc_0403_EditPrivateTeamMember() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(5);
//		
//		try {
//			  for (WebElement teamName : teamNames) {
//				if (teamName.getText().equals(team_name)) {
//					teamName.click();// Click Previously created team
//					break;
//				}
//			}
//			}
//			catch (StaleElementReferenceException e){
//			       e.toString();
//			}
//			
//		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(1);
    	SitePageModel.tryClick(driver,By.xpath(".//a[contains(@data-bind, 'click:updateGroup') and 'Info & Members']"));// (" Select Info and member"));
		SitePageModel.waitFor(5);
	
//		SitePageModel.enterText(driver,By.name("title"),team_name_edit);//Team name
//		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.tagName("textarea"), "Test Automation Description ");
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver,By.id("externalMailInput"),"sayedul123@gmail.com");//value to 'Ad guest'
	    SitePageModel.waitFor(1);
	    SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver,By.xpath(".//input[contains(@class, 'form-control') and (@placeholder='Invite Member...')]"),"test7");//Enter value 'Invite member' field
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.id("user-test7.coc@rootnext.com"));//Click on user8 img
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                                                             
		SitePageModel.waitFor(30);
		                                       
//		boolean bFoundTeamName = true;
//		List<WebElement> searchTeamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));//collect all team names
//		
//		for (WebElement searchTeamName : searchTeamNames) {
//			if (searchTeamName.getText().equals(team_name_edit)) {
//				bFoundTeamName = true;
//				break;
//			}
//		}
//		Assert.assertTrue(bFoundTeamName);

	}


//	@Test
	@TestInfo(tcName="Close a team",feature="Team", expectedResult="Team is closed")
	public void tc_0404_ClosePrivateTeam() {
		
		
		
		SitePageModel.waitFor(5);
		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(2);
		
		try {
			  for (WebElement teamName : teamNames) {
				if (teamName.getText().equals(team_name)) {
					teamName.click();// Click Previously created team
					break;
				}
			}
			}
			catch (StaleElementReferenceException e){
			       e.toString();
			}
			
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow

		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
																									
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);

	}

	//@Test
	@TestInfo(tcName="Create a team by fill-up all field",feature="Team", expectedResult="Team is created sucessfully")
	public void tc_0405_CreatePrivateTeamWithAllInfo() {

		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on public Teams 
		SitePageModel.waitFor(5);
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		SitePageModel.waitFor(3);
		
		String teamname=SitePageModel.randomStringGenerator(driver);
		String team_name_allInfo="Test team "+teamname;
		
		SitePageModel.enterText(driver,By.name("title"),team_name_allInfo);//'Team name' value
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Description");//Team name description
		SitePageModel.waitFor(1);
//		SitePageModel.enterText(driver,By.xpath(".//*[@id='externalMailInput']"), "sayedul@gmail.com");//'Ad guest' value
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
//		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver, By.xpath(".//*[@id='member-invite']"),"user7");//'Invite member' value
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.id("user-user7@rootnext.com"));//Click on 'user7'
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("createGroupButton"));//Click 'create team' button
		SitePageModel.waitFor(20);

//		boolean bFoundTeamName = false;
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));//collect all team names
//        
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_name_allInfo)) {
//				bFoundTeamName = true;
//				break;
//			}
//		}
//		}
//          catch (StaleElementReferenceException e){
//           e.toString();
//}
//
//		Assert.assertTrue(bFoundTeamName);
		
		SitePageModel.waitFor(5);
		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(2);
		
		try {
			  for (WebElement teamName : teamNames) {
				if (teamName.getText().equals(team_name_allInfo)) {
					teamName.click();// Click Previously created team
					break;
				}
			}
			}
			catch (StaleElementReferenceException e){
			       e.toString();
			}
			
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
																									
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);
	}

	@Test
	@TestInfo(tcName="Create team using invalid info",feature="Team", expectedResult="Error message is displayed")
	public void tc_0406_CreatePrivateTeamWithBlankInfo() {
		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		SitePageModel.waitFor(3);

		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
		SitePageModel.waitFor(3);

		String strExpectedPostError = "Required field.";
		List<WebElement> strActualPostError = driver
				.findElements(By
						.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/span"));//warning message
		SitePageModel.waitFor(2);
		boolean bFound = strActualPostError.get(0).getText()
				.contains(strExpectedPostError);
		Assert.assertTrue(bFound);
		

	}

	@Test
	@TestInfo(tcName="Create post on a team",feature="Team Post", expectedResult="Post is created on a team")
	public void tc_0407_CreatePrivateTeamPost() throws AWTException {
		
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//
//		SitePageModel.enterText(driver,By.name("title"),team_post_name);//'Team name' value
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(30);
		
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(2);
//		
//		try {
//			  for (WebElement teamName : teamNames) {
//				if (teamName.getText().equals(team_post_name)) {
//					teamName.click();// Click Previously created team
//					break;
//				}
//			}
//			}
//			catch (StaleElementReferenceException e){
//			       e.toString();
//			}
//			
//		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("postForm"));// ("Click on Post");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post");//enter value in 'text' field
		SitePageModel.waitFor(2);
		
		String strUploadImage = AppConstant.IMAGE_PATH + "index.png";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(5);
	    upload.sendKeys(strUploadImage);
	    
	    SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));// Click on 'create' button
		SitePageModel.waitFor(12);
		String strExpectedPost = "Test Automation Post";
		List<WebElement> strActualPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		                                                               
		boolean bFound = strActualPost.get(0).getText().contains(strExpectedPost);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Create post with required information on team",feature="Team Post", expectedResult="Post is created using required information")
	public void tc_0408_CreatePrivateTeamPostWithRequiredInfo() {
		
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
//		SitePageModel.waitFor(7);
		
		
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//        
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_post_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		
//        catch (StaleElementReferenceException e){
//              e.toString();
//       }
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='postForm']"));// Click on Post
		SitePageModel.waitFor(4);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post with Required Field");//Text field value
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));// Click on 'create' button
		SitePageModel.waitFor(7);
		
		String strExpectedPost = "Test Automation Post with Required Field";
		List<WebElement> strActualPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));

		boolean bFound = strActualPost.get(0).getText()
				.contains(strExpectedPost);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(1);

	}

	@Test
	@TestInfo(tcName="Create post with invalid information",feature="Team Post", expectedResult="Error message is displayed")
	public void tc_0409_CreatePrivateTeamPostWithBlankInfo() {
		
		SitePageModel.waitFor(5);

//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_post_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//	catch (StaleElementReferenceException e){
//	       e.toString();
//	}
//		SitePageModel.waitFor(5);
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='postForm']"));//Click on Post
		SitePageModel.waitFor(3);

		SitePageModel
				.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));// Click on 'Create'
		SitePageModel.waitFor(3);

		String strExpectedPost = "Required field.";
		List<WebElement> strActualPost = driver.findElements(By.xpath(".//*[@id='postCreateForm']/span"));

		boolean bFound = strActualPost.get(0).getText()
				.contains(strExpectedPost);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(2);

	}

	
	@Test
 	@TestInfo(tcName="Edit a post",feature="Team Post", expectedResult="Post is updated")
	public void tc_0410_EditPrivateTeamPost() {
		
		SitePageModel.waitFor(5);

//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//    try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_post_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//    catch (StaleElementReferenceException e){
//    e.toString();
//    }
//		SitePageModel.waitFor(5);
	
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post Editing");
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='updatePostButton']"));// Click Update button
		SitePageModel.waitFor(7);

		String strExpectedEditedPost = "Test Automation Post Editing";
		List<WebElement> strActualEditedPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		SitePageModel.waitFor(2);
		boolean bFound = strActualEditedPost.get(0).getText()
				.contains(strExpectedEditedPost);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(2);
	}

	@Test
	@TestInfo(tcName="Delete Team for post",feature="Team Deletion", expectedResult="Team is deleted")
	public void tc_0411_DeletePrivateTeamPost() {
		
		SitePageModel.waitFor(3);
//		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
//		SitePageModel.waitFor(7);		
		
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_post_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//catch (StaleElementReferenceException e){
//    e.toString();
//}
//		SitePageModel.waitFor(5);
		
		
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
		SitePageModel.waitFor(3);
		
//		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
//		
//		SitePageModel.waitFor(2);
//
//		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
//		SitePageModel.waitFor(3);
	
	}

	
	@Test
	@TestInfo(tcName="Create a team announcement",feature="Team Announcement", expectedResult="Announcement is created")
	public void tc_0412_CreatePrivateTeamAnnouncement() {
		
		
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//
//		SitePageModel.enterText(driver,By.name("title"),team_announcement_name);//'Team name' value
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(30);
//		
//		
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(2);
//		
//		try {
//			  for (WebElement teamName : teamNames) {
//				if (teamName.getText().equals(team_announcement_name)) {
//					teamName.click();// Click Previously created team
//					break;
//				}
//			}
//			}
//			catch (StaleElementReferenceException e){
//			       e.toString();
//			}
//			
//		SitePageModel.waitFor(5);
		
		
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);			
		
		SitePageModel.tryClick(driver, By.id("announcementForm"));// ("Click on Announcement");
		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.name("title"),"Test team Announcement");//enter value in 'title' field
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Announcement description");//enter value in 'Text' field
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(10);

		String strExpectedAnnouncement = "Test team Announcement";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//first announcement

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Create a team announcement with required field",feature="Team Announcement", expectedResult="Announcement is created only with required fields")
	public void tc_0413_CreatePrivateTeamAnnouncementWithRequiredField() {
		
//		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(2);
//		
//		try {
//			  for (WebElement teamName : teamNames) {
//				if (teamName.getText().equals(team_announcement_name)) {
//					teamName.click();// Click Previously created team
//					break;
//				}
//			}
//			}
//			catch (StaleElementReferenceException e){
//			       e.toString();
//			}
//			
//		SitePageModel.waitFor(5);

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("announcementForm"));// Open Announcement form
		SitePageModel.waitFor(3);

		SitePageModel.enterText(driver,By.name("title"),"Announcement with required field");
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver,By.tagName("textarea"),"Announcement with required field description");//enter value in 'Text' field
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(10);

		String strExpectedAnnouncement = "Announcement with required field";
		List<WebElement> strActualAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//last announcement

		boolean bFound = strActualAnnouncement.get(0).getText().contains(strExpectedAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(2);

	}

	@Test
	@TestInfo(tcName="Create announcement with invalid information",feature="Team Announcement", expectedResult="Error message is displayed")
	public void tc_0414_CreatePrivateTeamAnnouncementWithBlankInfo() {

		
//		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(2);
//		
//		try {
//			  for (WebElement teamName : teamNames) {
//				if (teamName.getText().equals(team_announcement_name)) {
//					teamName.click();// Click Previously created team
//					break;
//				}
//			}
//			}
//			catch (StaleElementReferenceException e){
//			       e.toString();
//			}
//			
				
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("announcementForm"));// ("Click on announcement");
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(2);

		String strExpectedErrorAnnouncement = "Required field.";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/span"));

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedErrorAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(2);

	}

	@Test
	@TestInfo(tcName="Edit a team announcement",feature="Team Announcement", expectedResult="Announcement is updated")
	public void tc_0415_EditPrivateTeamAnnouncement() {
		
		SitePageModel.waitFor(5);

//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		
//		
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_announcement_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}		
//		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.linkText("Edit"));//(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/ul/li/ul/li[1]/a"));// Click on Edit
		SitePageModel.waitFor(7);

		SitePageModel.enterText(driver, By.name("title"),"Test Announcement Edit");//last announcement
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Announcement description edit");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("updateAnnouncementButton"));// ("Click on Update");
		SitePageModel.waitFor(12);

		String strExpectedEditedAnnouncement = "Test Announcement Edit";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);

	}

	@Test
	@TestInfo(tcName="Delete a team announcement",feature="Team Announcement", expectedResult="Announcement is deleted")
	public void tc_0416_DeletePrivateTeamAnnouncement() {
		
//		SitePageModel.waitFor(5);
//
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_announcement_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//catch (StaleElementReferenceException e){
//    e.toString();
//}

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation
		SitePageModel.waitFor(4);
//		 
//		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
//			
//		SitePageModel.waitFor(2);
//
//		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
//		SitePageModel.waitFor(10);

	}

	@Test
	@TestInfo(tcName="Create a team Event with required field",feature="Team Event", expectedResult="Event is created")
	public void tc_0417_CreatePrivateTeamEventWithRequiredField() throws AWTException {
		

//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//
//		SitePageModel.enterText(driver,By.name("title"),team_event_name);//'Team name' value
//        SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(30);
//
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//			e.toString();
//}

		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
 		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.name("title"),"QA learning Event");//title
		SitePageModel.waitFor(2);
		
		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.waitFor(2);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));//Click 'location' field
		SitePageModel.waitFor(5);
		
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(6);
		allElement.get(0).sendKeys("Bangladesh");
		SitePageModel.waitFor(3);	
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver,By.id("createEventButton"));// ("Click on create"));
		SitePageModel.waitFor(7);
		
		
		String strEvent = "QA learning Event";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strEvent);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(5);
		


	}
	
	
	@Test
	@TestInfo(tcName="Create a team Event with using Allday",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0418_CreatePrivateTeamEvent_WithAllDay() throws AWTException {

		SitePageModel.waitFor(5);
		
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//		try{															// names
//
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//
//		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.name("title"),"QA learning Event two");//title
		SitePageModel.waitFor(1); 
		
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[2]/div/div/label"));//Click on 'all day' event;
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));//Click 'location' field
		SitePageModel.waitFor(5);
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		
		SitePageModel.waitFor(1);
		allElement.get(0).sendKeys("Bangladesh");

		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));

		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.tagName("textarea"),"QA meeting");//Description
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='createEventButton']"));// ("Click on create"));
		SitePageModel.waitFor(7);
		
		String strExpectedEditedAnnouncement = "QA learning Event two";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(2);

	}
	
	
	@Test
	@TestInfo(tcName="Create a team Event with inviting member",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0419_CreatePrivateTeamEvent_WithInviteMember() throws AWTException {
		
		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(5);
//		
//		try {
//		  for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}		
//		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver,By.xpath(".//a[contains(@class, 'btn btn-mini btn-default') and 'Invite Members']"));// ("Click on Invite members"));

		SitePageModel.waitFor(7);

		SitePageModel.enterText(driver,By.id("externalMailInput"), "sayedul@gmail.com");//Enter email
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
		SitePageModel.waitFor(2);


		SitePageModel.enterText(driver,By.name("title"),"test7");//enter value 'invite member'
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.id("user-test7.coc@rootnext.com"));// Click on image
																									
		SitePageModel.waitFor(3);

		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                                                             
		SitePageModel.waitFor(50);
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='sidebar-tile-members']/div[2]/div/div/a[2]/img"));//check image of right side panel
		SitePageModel.waitFor(5);

	}
	
	@Test
	@TestInfo(tcName="Create a team Event with invalid data",feature="Team Event", expectedResult="Error message is displayed")
	public void tc_0420_CreatePrivateTeamEventBlankField() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		
		//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//		SitePageModel.waitFor(4);
 
		
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver,By.id("createEventButton"));// ("Click on create"));
		SitePageModel.waitFor(3);
		SitePageModel.scrollTopOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpectedErrorAnnouncement = "Required field.";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedErrorAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(4);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

	}
	
	
	@Test
	@TestInfo(tcName="Create a team event by fill-up all field",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0421_CreatePrivateTeamEvent_AllInfo() throws AWTException {
	
//		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
		
		
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver,By.name("title"),"Important event for QA meeting two");//title
		SitePageModel.waitFor(1);

		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and 'Select location']"));//Click 'location' field
		SitePageModel.waitFor(4);
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(2);
		allElement.get(0).sendKeys("Bangladesh");

		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver,By.tagName("textarea"),"QA meeting");//description
		SitePageModel.waitFor(1);

//		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Find a member...']"));//("Click member field"));
//
//	    SitePageModel.waitFor(5);

//	    List<WebElement>allElements=driver.findElements(By.tagName("input"));
//		SitePageModel.waitFor(2);		
//		allElements.get(0).sendKeys("user8");
//
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
//		SitePageModel.waitFor(3);
		String strUploadImage = AppConstant.IMAGE_PATH + "index.png";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(2);
	    upload.sendKeys(strUploadImage);

		SitePageModel.waitFor(10);
		//SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-info btn-small']"));//("Click on update button"));
//		SitePageModel.tryClick(driver, By.id("createEventButton"));
//		SitePageModel.waitFor(10);
		SitePageModel.scrollBottomOfPage(driver);
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("createEventButton"));
		SitePageModel.waitFor(15);
		
		String strExpectedEditedAnnouncement = "Important event for QA meeting two";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));//last created event
		SitePageModel.waitFor(2);
		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(2);
			
	}
	
  @Test
  @TestInfo(tcName="Cancel during editing a event",feature="Team Event", expectedResult="Team event editing mode is closed")
  public void tc_0422_EditPrivateTeamEventCancel() throws AWTException {
		
		driver.navigate().refresh();		
		SitePageModel.waitFor(25);
		
		
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(5);										
//		
//     try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//	}catch (StaleElementReferenceException e){
//	       e.toString();
//	       
//	}
//		
//		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));// ("Click on Dropdown"));
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class, 'btn btn-small btn-gray') and 'Cancel']"));//Click 'cancel' 
		
        SitePageModel.waitFor(10);
		
//        String strExpectedEditedEvent = "Important event for QA meeting two";
//		List<WebElement> strActualEditedEvent = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//last created event
//		SitePageModel.waitFor(2);
//
//		boolean bFoundEditedText = strActualEditedEvent.get(0).getText()
//				.contains(strExpectedEditedEvent);
//		Assert.assertTrue(bFoundEditedText);
//		SitePageModel.waitFor(3);
        
	}
	
  	@Test
	@TestInfo(tcName="Edit a team event",feature="Team Event", expectedResult="Event is updated successfully")
	public void tc_0423_EditPrivateTeamEvent() throws AWTException {
		
		SitePageModel.waitFor(10);
		SitePageModel.scrollTopOfPage(driver);
		
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver,By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));// ("Click on Dropdown"));
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver, By.name("title"), "Team event Edit");//title
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.id("updateEventButton"));
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@id, 'updateEventButton') and 'Update']"));//Click 'update' 
		SitePageModel.waitFor(20);
		
//      String strExpectedEditedEvent = "Team event Edit";
//		List<WebElement> strActualEditedEvent= driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
//		                                                                     
//		boolean bFoundEditedText = strActualEditedEvent.get(0).getText()
//				.contains(strExpectedEditedEvent);
//		Assert.assertTrue(bFoundEditedText);
//		SitePageModel.waitFor(5);
      
	}
	
	

	
	@Test
	@TestInfo(tcName="Delete a team event",feature="Team Event", expectedResult="Team Event is deleted")
	public void tc_0424_DeletePrivateTeamEvent() throws AWTException {
		
		
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		
//		SitePageModel.waitFor(5);
//		
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//		SitePageModel.waitFor(3);															// names
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_event_name)) {
//				
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}catch (StaleElementReferenceException e){
//	       e.toString();
//	       
//	}
     
		
		
        SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver,By.linkText("Delete"));// Select Delete
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation
		
//        SitePageModel.waitFor(5);
//        String strExpectedEditedAnnouncement = "QA learning Event two";
//		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
//
//		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
//				.contains(strExpectedEditedAnnouncement);
//		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath(".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
//		
//		SitePageModel.waitFor(2);
//
//		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
//		SitePageModel.waitFor(10);
                
	}
	
	
	@Test
	@TestInfo(tcName="Create a team poll for multiple day",feature="Team Poll", expectedResult="Team Poll is created with multiple day")
	public void tc_0425_CreatePrivateTeamMultipleChoicePollForMultipleDay() {

		
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//		SitePageModel.enterText(driver,By.name("title"),team_poll_name);//'Team name' value
//        SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(25);
//		
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//		SitePageModel.waitFor(7);															// names
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}catch (StaleElementReferenceException e){
//	       e.toString();
//	       
//	}
//		SitePageModel.waitFor(7);
		
		
		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));
		
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.name("title"),"What is our national flower");
		SitePageModel.waitFor(2);

		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");//Description
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on Add Answers");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Water Lily");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Rose");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"lotus");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPollButton']"));// ("Click on create button");
		SitePageModel.waitFor(10);

		String strExpect = "What is our national flower";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h5[2]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(2);
	}

	@Test
	@TestInfo(tcName="Create team poll for one day",feature="Team Poll", expectedResult="Team Poll is created for one day")
	public void tc_0426_CreatePrivateTeamMultipleChoicePollForOneDay() {
		
		
		SitePageModel.waitFor(5);

//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//		
//
//		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//multiple choice
		
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.name("title"),"What is our national bird");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		SitePageModel.waitFor(2);
		String startDate= SitePageModel.AddDateWithToday(1);
		SitePageModel.waitFor(1);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventAllDayField']/div/input"), startDate);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");//Description
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on add form");
		SitePageModel.waitFor(3);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Peacock");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Magpie Robin");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Crow");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.id("createPollButton"));// ("Click on create button");
		SitePageModel.waitFor(10);

		List<WebElement> pollNames = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li"));

		try{
			
		for (WebElement pollName : pollNames) {
			if (pollName.getText().equals("What is our national bird")) {
				System.out.println(pollName.getText());
				break;
			}
		}}
		catch (StaleElementReferenceException e){
	       e.toString();
	}
		
		SitePageModel.waitFor(5);
	
	}
	
	
	
	@Test
	@TestInfo(tcName="Like to a Poll",feature="Private Team Poll", expectedResult="Like number is increased")
    public void tc_0457_LikePoll(){
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    SitePageModel.waitFor(2);

	}
	@Test
	@TestInfo(tcName="Unlike to a Poll",feature="Private Team Poll", expectedResult="Like number is decreased")
     public void tc_0458_UnLikePoll(){
		
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    SitePageModel.waitFor(2);

	}
	
	
	@Test
	@TestInfo(tcName="Create team poll with invalid data",feature="Team Poll", expectedResult="Error message is displayed")
	public void tc_0427_CreatePrivateTeamPollBlankField() {
		
//		SitePageModel.waitFor(3);
//
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//      try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//      catch (StaleElementReferenceException e){
//	       e.toString();
//	}
//	
//
//		SitePageModel.waitFor(7);
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPollButton']"));// ("Click on create button");
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));
		SitePageModel.waitFor(5);
	}

	@Test
	@TestInfo(tcName="Edit team poll",feature="Team Poll", expectedResult="Team poll is updated")
	public void tc_0428_EditPrivateTeamPollOneDay() throws InterruptedException {
		

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
//		SitePageModel.waitFor(5);
//
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//		SitePageModel.waitFor(5);
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//		
//
//		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7); 
		
        SitePageModel.scrollDownOfPage(driver);
		SitePageModel.enterText(driver, By.tagName("textarea"), "This is edited poll of Bird");
		SitePageModel.waitFor(2);
		String startDate= SitePageModel.AddDateWithToday(0);
		SitePageModel.waitFor(1);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventAllDayField']/div/input"), startDate);
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on add form");
		SitePageModel.waitFor(5);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[5]/input"),"Parrot");
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));// ("Click on update button");
		

     	SitePageModel.waitFor(30);

		String strExpect = "This is edited poll of Bird";
		String parrotXpath=".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[2]/p";
		//String print_parrotXpath = driver.findElement(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[2]/p")).getText();
		//SitePageModel.waitFor(3);                     
		
        //System.out.println(print_parrotXpath);
		SitePageModel.waitFor(3);
		
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(parrotXpath));//parrot
		                
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);
	}
	
	@Test
	@TestInfo(tcName="Cast vote on All day Poll",feature="Private Team Poll", expectedResult="Vote is cast in Poll")
    public void tc_0456_CastVoteInPoll(){
		
//		SitePageModel.waitFor(5);
//
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//		
//
//		SitePageModel.waitFor(5);
		
		SitePageModel.selectRadioText(driver, By.xpath("//label[text()='Magpie Robin']"));// click radio button
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary btn-mini btn-cons m-t-5' and text() = 'Vote']"));//("Click on vote");	
		SitePageModel.waitFor(7);
		
		SitePageModel.checkMenuText(driver, "1 vote", By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[4]/a"));

		
	}
	
	@Test
	@TestInfo(tcName="Add comment on a poll",feature="Private team Poll", expectedResult="Comment is added")
    public void tc_0471_AddCommentToPoll(){
		
		
		
		SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);

	}
	
	
	@Test
	@TestInfo(tcName="Edit comment on a poll",feature="Private team Poll", expectedResult="Comment is updated")
    public void tc_0472_EditCommentToPoll(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(7);
		

	}
	
	
	@Test
	@TestInfo(tcName="Delete comment from a Poll",feature="Private team Poll", expectedResult="Comment is deleted")
     public void tc_0473_DeleteCommentToPoll(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(7);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);

	}
	
	
	@Test
	@TestInfo(tcName="Delete team for poll",feature="Team Poll", expectedResult="Error message is displayed")
	public void tc_0429_DeletePrivateTeamPoll() {
		
 
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Private Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
//		SitePageModel.waitFor(5);
//
//		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
//
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_poll_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch (StaleElementReferenceException e){
//		       e.toString();
//		}
//		
//
//		SitePageModel.waitFor(6);

		
 		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.linkText("Delete"));
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation
		SitePageModel.waitFor(3);

//		String strExpect = "What is our national flower";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//		Assert.assertTrue(bFound);
//		SitePageModel.waitFor(3);
//		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
//		
//		SitePageModel.waitFor(2);
//
//		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
//		SitePageModel.waitFor(10);
	
	}
	@Test
	@TestInfo(tcName="Create team for poll",feature="Team Health Poll", expectedResult="Team Health Poll Created Successfully")
	public void tc_0450_PrivateTeamPollHealthTeam() throws AWTException {
	
		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//		SitePageModel.enterText(driver,By.name("title"),team_poll_name);//'Team name' value
//        SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(25);
//		
//		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);		
		
		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));
		SitePageModel.waitFor(4);
//		SitePageModel.tryClick(driver, By.id("questionsDropdown"));
//		SitePageModel.waitFor(4);
//		SitePageModel.tryClick(driver, By.linkText("More Details"));
		//SitePageModel.tryClick(driver, By.className("semi-bold poll-more-details"));
		SitePageModel.waitFor(4);
		//SitePageModel.enterText(driver, By.tagName("textarea"), "test Multiple choice poll");
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("repeatCycleDropdown"));
		
		SitePageModel.tryClick(driver, By.linkText("Daily"));
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("createPollButton"));
		SitePageModel.waitFor(4);
		
		String strExpect = "Currently, I feel our team is strong and working well together.";
		String nameXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h5[2]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(nameXpath));	
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpect1 = "Like";
		String nameXpath1 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(2);
		
		String strExpect2 = "0 comments";
		String nameXpath2 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[2]/a";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));	
		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
		Assert.assertTrue(bFound2);
		SitePageModel.waitFor(2);
		
//		String strExpect3 = "0 votes";
//		String nameXpath3 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[3]/a";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm3 = driver.findElements(By.xpath(nameXpath3));	
//		boolean bFound3 = aboutTextElm3.get(0).getText().contains(strExpect3);
//		Assert.assertTrue(bFound3);
//		SitePageModel.waitFor(2);
		
		
	}
	@Test
	@TestInfo(tcName="Edit team health poll By Voters Are Anonymous",feature="Team Health Poll", expectedResult="Team Health Poll updated Successfully")
	public void tc_0451_EditPrivateTeamPollHealthTeamByVotersAreAnonymous() throws AWTException {
	
		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7);             
        SitePageModel.scrollDownOfPage(driver);
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[2]/label"));
        //SitePageModel.tryClick(driver,By.xpath(".//div[contains(@class, 'checkbox') and text='Voters are anonymous']"));//Click 'Anonymous'
		
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));
		SitePageModel.waitFor(7);
		String strExpect = "Your vote will be anonymous.";
		String nameXpath = ".//p[contains(@class, 'anonymous-message')]";
		//String nameXpath = ".//p[contains(@class, 'anonymous-message') and text='Your vote will be anonymous.']";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(nameXpath));	
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(4);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpect1 = "Like";
		String nameXpath1 = "html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(4);
		
		String strExpect2 = "0 comments";
		String nameXpath2="html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[2]/a";
		
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));	
		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
		Assert.assertTrue(bFound2);
		SitePageModel.waitFor(4);
		
		String strExpect3 = "0 votes";
		String nameXpath3 = "html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[4]/a";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm3 = driver.findElements(By.xpath(nameXpath3));	
		boolean bFound3 = aboutTextElm3.get(0).getText().contains(strExpect3);
		Assert.assertTrue(bFound3);
		SitePageModel.waitFor(4);
		
	}
	
	@Test
	@TestInfo(tcName="Edit Team Health poll By Disable Comments",feature="Team Health Poll", expectedResult="Team Health Poll updated Successfully")
	public void tc_0452_EditPrivateTeamPollHealthTeamByDisableComments() throws AWTException {
	
		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7);             
        SitePageModel.scrollDownOfPage(driver);
        	SitePageModel.waitFor(2);
        	SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[5]/label"));
        	
        //SitePageModel.tryClick(driver, By.id("disable-comments"));
    	SitePageModel.waitFor(3);
        //SitePageModel.tryClick(driver,By.xpath(".//div[contains(@class, 'checkbox') and text='Voters are anonymous']"));//Click 'Anonymous'
		
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));
		SitePageModel.waitFor(4);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpect1 = "Like";
		String nameXpath1 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(4);
		
//		String strExpect2 = "Share";
//		String nameXpath2 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[4]/div[2]/div/div/div[6]/ul/li[2]/div/a";
//		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));	
//		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
//		Assert.assertTrue(bFound2);
//		SitePageModel.waitFor(4);
		
		String strExpect3 = "0 votes";
		String nameXpath3 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[3]/a";
		SitePageModel.waitFor(2);
		String voteText= driver.findElement(By.xpath(nameXpath3)).getText();
		SitePageModel.waitFor(2);
		System.out.println(voteText);
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm3 = driver.findElements(By.xpath(nameXpath3));	
		boolean bFound3 = aboutTextElm3.get(0).getText().contains(strExpect3);
		Assert.assertTrue(bFound3);
		SitePageModel.waitFor(4);
		
		SitePageModel.waitFor(5);

			
	}
	
	
	@Test
	@TestInfo(tcName="Create team for poll",feature="Rating Scale Poll", expectedResult="Rating Scale Poll Created Successfully")
	public void tc_0453_PrivateTeamPollRatingScale() throws AWTException {
	
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
//		SitePageModel.waitFor(3);
//		SitePageModel.enterText(driver,By.name("title"),team_poll_name);//'Team name' value
//        SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
//		SitePageModel.waitFor(25);
		
		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);		
		
		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[3]/a"));//Rating scale
		SitePageModel.waitFor(4);          

		
		SitePageModel.enterText(driver, By.name("title"), "test Rating Scale poll");
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[2]/a"));//more details
		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver, By.tagName("textarea"), "testing details for Rating Scale poll");//details text
		SitePageModel.waitFor(4);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[2]/input"), "rate 5");
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[3]/input"), "rate 4");
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[4]/input"), "rate 3");
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[5]/input"), "rate 2");
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[6]/input"), "rate 1");
		
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("repeatCycleDropdown"));
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Daily"));
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("createPollButton"));
		SitePageModel.waitFor(4);
		
		
		String strExpect = "test Rating Scale poll";
		String nameXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h5[2]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(nameXpath));	
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		String strExpect1 = "testing details for Rating Scale poll";
		String nameXpath1 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[2]/p";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		String strExpectrate5 = "rate 5";
		String nameXpathrate5 = ".//*[@id='user-management']/div[1]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmrate5 = driver.findElements(By.xpath(nameXpathrate5));	
		boolean bFoundrate5 = aboutTextElmrate5.get(0).getText().contains(strExpectrate5);
		Assert.assertTrue(bFoundrate5);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		String strExpectrate4 = "rate 4";
		String nameXpathrate4 = ".//*[@id='user-management']/div[2]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmrate4 = driver.findElements(By.xpath(nameXpathrate4));	
		boolean bFoundrate4 = aboutTextElmrate4.get(0).getText().contains(strExpectrate4);
		Assert.assertTrue(bFoundrate4);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		String strExpectrate3 = "rate 3";
		String nameXpathrate3 = ".//*[@id='user-management']/div[3]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmrate3 = driver.findElements(By.xpath(nameXpathrate3));	
		boolean bFoundrate3 = aboutTextElmrate3.get(0).getText().contains(strExpectrate3);
		Assert.assertTrue(bFoundrate3);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		String strExpectrate2 = "rate 2";
		String nameXpathrate2 = ".//*[@id='user-management']/div[4]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmrate2 = driver.findElements(By.xpath(nameXpathrate2));	
		boolean bFoundrate2 = aboutTextElmrate2.get(0).getText().contains(strExpectrate2);
		Assert.assertTrue(bFoundrate2);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		String strExpectrate1 = "rate 1";
		String nameXpathrate1 = ".//*[@id='user-management']/div[5]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmrate1 = driver.findElements(By.xpath(nameXpathrate1));	
		boolean bFoundrate1 = aboutTextElmrate1.get(0).getText().contains(strExpectrate1);
		Assert.assertTrue(bFoundrate1);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		
		
		String strExpectlike = "Like";
		String nameXpathlike = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmlike = driver.findElements(By.xpath(nameXpathlike));	
		boolean bFoundlike = aboutTextElmlike.get(0).getText().contains(strExpectlike);
		Assert.assertTrue(bFoundlike);
		SitePageModel.waitFor(2);
		
		String strExpectcomments = "0 comments";
		String nameXpathcomments = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[2]/a";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmcomments = driver.findElements(By.xpath(nameXpathcomments));	
		boolean bFoundcomments = aboutTextElmcomments.get(0).getText().contains(strExpectcomments);
		Assert.assertTrue(bFoundcomments);
		SitePageModel.waitFor(2);
		
		String strExpectvotes = "0 votes";
		String nameXpathvotes = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[4]/a";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmvotes = driver.findElements(By.xpath(nameXpathvotes));	
		boolean bFoundvotes = aboutTextElmvotes.get(0).getText().contains(strExpectvotes);
		Assert.assertTrue(bFoundvotes);
		SitePageModel.waitFor(2);
		
		
	}
	@Test
	@TestInfo(tcName="Edit Rating Scale poll",feature="Rating Scale poll", expectedResult="Rating Scale poll updated Successfully")
	public void tc_0454_EditPrivateTeamPollRatingScaleByVotersAreAnonymous() throws AWTException {
	
		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7);             
        SitePageModel.scrollDownOfPage(driver);
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[2]/label"));//Click 'Anonymous'
        //SitePageModel.tryClick(driver,By.xpath(".//div[contains(@class, 'checkbox') and text='Voters are anonymous']"));//Click 'Anonymous'
		
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));
		SitePageModel.waitFor(7);
		String strExpect = "Your vote will be anonymous.";
		String nameXpath =  ".//p[contains(@class, 'anonymous-message')]";
		//String nameXpath = ".//p[contains(@class, 'anonymous-message') and text='Your vote will be anonymous.']";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(nameXpath));	
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(4);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpect1 = "Like";
		String nameXpath1 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(4);
		
		String strExpect2 = "0 comments";
		String nameXpath2="html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[2]/a";	
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));	
		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
		Assert.assertTrue(bFound2);
		SitePageModel.waitFor(4);
		
		String strExpect3 = "0 votes";
		String nameXpath3 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[4]/a";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm3 = driver.findElements(By.xpath(nameXpath3));	
		boolean bFound3 = aboutTextElm3.get(0).getText().contains(strExpect3);
		Assert.assertTrue(bFound3);
		SitePageModel.waitFor(4);
		
	}
	@Test
	@TestInfo(tcName="Edit Team Rating Scale poll By Disable Comments",feature="Rating Scale poll", expectedResult="Rating Scale poll updated Successfully")
	public void tc_0455_EditPrivateTeamPollRatingScaleByDisableComments() throws AWTException {
	
		SitePageModel.waitFor(5);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_poll_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		

		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7);             
        SitePageModel.scrollDownOfPage(driver);
        	SitePageModel.waitFor(2);
        	SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[5]/label"));//disable comments
        	
        //SitePageModel.tryClick(driver, By.id("disable-comments"));
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));
		SitePageModel.waitFor(4);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpect1 = "Like";
		String nameXpath1 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));	
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(4);
		
		String strExpect2 = "Share";
		String nameXpath2 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[2]/div/a";
		//String nameXpath1 = ".//a[contains(@data-bind, 'click:like')]";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));	
		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
		Assert.assertTrue(bFound2);
		SitePageModel.waitFor(4);
		
		String strExpect3 = "0 votes";
		String nameXpath3 = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[3]/a";
		SitePageModel.waitFor(2);
		String voteText= driver.findElement(By.xpath(nameXpath3)).getText();
		SitePageModel.waitFor(2);
		//System.out.println(voteText);
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm3 = driver.findElements(By.xpath(nameXpath3));	
		boolean bFound3 = aboutTextElm3.get(0).getText().contains(strExpect3);
		Assert.assertTrue(bFound3);
		SitePageModel.waitFor(4);
		
		SitePageModel.waitFor(5);

			
	}
	
	
	@Test
	@TestInfo(tcName="Create team Task",feature="Team Task", expectedResult="Team task is created successfully")

	public void tc_0430_CreatePrivateTeamTask() throws AWTException {
				
		
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver,By.name("title"),team_task_name);//'Team name' value
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Description");//Team name description
			SitePageModel.waitFor(1);
//			SitePageModel.enterText(driver,By.xpath(".//*[@id='externalMailInput']"), "sayedul@gmail.com");//'Ad guest' value
//			SitePageModel.waitFor(1);
//			SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
//			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.id("member-invite"),"user8");//'Invite member' value
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver,By.id("user-user8@rootnext.com"));//Click on 'user 8'
			SitePageModel.waitFor(1);
			SitePageModel.enterText(driver, By.id("member-invite"),"user7");//'Invite member' value
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver,By.id("user-user7@rootnext.com"));//Click on 'user7'
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.id("createGroupButton"));//Click 'create team' button
			SitePageModel.waitFor(50);
			
			List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names

			try{
			for (WebElement teamName : teamNames) {
				if (teamName.getText().equals(team_task_name)) {
					teamName.click();// Click Previously created team
					break;
				}
			}
			}
			catch (StaleElementReferenceException e){
			       e.toString();
			}
			

			SitePageModel.waitFor(6);
			
			SitePageModel.tryClick(driver, By.id("taskForm"));
			SitePageModel.waitFor(5);
			SitePageModel.enterText(driver, By.name("task"), "task");
					
			SitePageModel.tryClick(driver, By.id("add-task"));//task button
			SitePageModel.waitFor(5);
			
			String strExpect = "task";
			String nameXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(nameXpath));	
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			SitePageModel.waitFor(5);
			
			
			
		}
	@Test
	@TestInfo(tcName="Change status 'In Progress' of task",feature="Team Task", expectedResult="Team task is in status 'In Progress' successfully")

	public void tc_0431_InProgressPrivateTeamTask() throws AWTException {
		 
		
		//SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-check')]"));
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[1]/button[2]"));
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.linkText("IN PROGRESS"));
		SitePageModel.waitFor(3);
		SitePageModel.checkMenuText(driver, "IN PROGRESS", By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[1]"));
		
		SitePageModel.waitFor(3);
	}
		
	@Test
	@TestInfo(tcName="Change status 'Done' of task",feature="Team Task", expectedResult="Team task is in status 'Done' successfully")

	public void tc_0432_DonePrivateTeamTask() throws AWTException {
		 
		
		//SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-check')]"));
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[1]/button[2]"));
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.linkText("DONE"));
		SitePageModel.waitFor(3);
		SitePageModel.checkMenuText(driver, "DONE", By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[1]"));
		
		SitePageModel.waitFor(3);
	}
	
	@Test
	@TestInfo(tcName="View all team task",feature="Team Task", expectedResult="All Team task will show")

	public void tc_0433_ViewAllPrivateTeamTask() throws AWTException {
		 
		
		//SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-check')]"));
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/a"));//("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[1]/button[2]"));
		SitePageModel.waitFor(7);
		String text=driver.findElement(By.id("show-completed")).getText();
		SitePageModel.waitFor(4);
		System.out.println(text);
		SitePageModel.checkMenuText(driver, "1 completed task", By.id("show-completed"));
		
		SitePageModel.waitFor(3);
	}
	
	@Test
	@TestInfo(tcName="Create team task by fill-up all field",feature="Team Task", expectedResult="Team task is created successfully")

	public void tc_0431_CreatePrivateTeamTaskAllInfo() throws AWTException {
		 
		SitePageModel.waitFor(5);
		driver.navigate().to("https://esn.connectik.org");;
		SitePageModel.waitFor(50);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(8);
		
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_task_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("taskForm"));
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver, By.name("task"), "New task");
				
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("group"));
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(2);
		
		allElement.get(0).sendKeys("user8");
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
	
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.id("add-task"));
		SitePageModel.waitFor(15);
		
		String strExpectAssignee = "user Eight";
		String nameXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/a/strong";
		                    
		
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElmAssignee = driver.findElements(By.xpath(nameXpath));	      
		boolean bFound = aboutTextElmAssignee.get(0).getText().contains(strExpectAssignee);
		Assert.assertTrue(bFound);
		
		String strExpect = "New task";
		String taskXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]";//task title
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(taskXpath));//task title	            
		boolean bFound1 = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound1);
		SitePageModel.waitFor(5);
		
		
		
		
	}
	@Test
	@TestInfo(tcName="Create team task and assigned task in two assignee",feature="Team Task", expectedResult="Team task is created successfully")

	public void tc_0432_CreatePrivateTeamTaskTwoAssignee() throws AWTException {
		
		driver.navigate().to("https://esn.connectik.org");;
		SitePageModel.waitFor(25);

		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(15);
		
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_task_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.linkText(team_task_name));
		SitePageModel.waitFor(40);
		SitePageModel.tryClick(driver, By.id("taskForm"));
		SitePageModel.waitFor(10);
		SitePageModel.enterText(driver, By.name("task"), "New task two");
				
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.id("group"));
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(10);
		
		allElement.get(0).sendKeys("user7");
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		
//		SitePageModel.tryClick(driver, By.id("group"));
//		List<WebElement>allElement1=driver.findElements(By.tagName("input"));
//		SitePageModel.waitFor(10);
//		allElement1.get(0).sendKeys("user8");
//		SitePageModel.waitFor(10);
//		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
	
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("add-task"));
		SitePageModel.waitFor(10);
		
		String strExpectAssignee= "user Eight";
		String nameXpath1=".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/a[2]/strong";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(nameXpath1));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpectAssignee);
		Assert.assertTrue(bFound1);
		
//		String strExpectAssignee2= "user Seven";
//		String nameXpath2=".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/a[1]/strong";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(nameXpath2));
//		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpectAssignee2);
//		Assert.assertTrue(bFound2);
		
		String strExpect = "New task two";
		String taskXpath = ".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]";//task title
		SitePageModel.waitFor(5);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(taskXpath));//task title	            
		boolean bFound3 = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound3);
		SitePageModel.waitFor(5);
		
		
	}	
	
	
	@Test
	@TestInfo(tcName="Delete team task",feature="Team Task", expectedResult="Team task is deleted")

	public void tc_0434_DeletePrivateTeamTask() throws AWTException {
		
		SitePageModel.waitFor(5);
		
		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(8);
		
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_task_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
		
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);
	}
	@Test
    @TestInfo(tcName="Open File tab view on a Team",feature="Team File", expectedResult="File window is opened")

	public void tc_0435_OpenPrivateTeamFileWindow() {
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.name("title"),team_File_name);//'Team name' value
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("createGroupButton"));//Click 'create team' button
		
		SitePageModel.waitFor(10);
		
		List<WebElement> teamNames = driver.findElements(By.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All team names
		SitePageModel.waitFor(15);
		
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_File_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch (StaleElementReferenceException e){
		       e.toString();
		}
		
		SitePageModel.waitFor(5);
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));// ("Click on file");
		SitePageModel.waitFor(7);
		
		String strExpect = "Description";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/label"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);

	}

//	@Test
//    @TestInfo(tcName="Upload File by cloud icon",feature="Team File", expectedResult="File is uploaded")
//
//	public void tc_0436_UploadPrivateTeamFileByCloudIcon() throws AWTException {
//			
//		SitePageModel.waitFor(2);
//		SitePageModel.enterText(driver, By.tagName("textarea"),"This is for testing file");// write on new new comment
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver,By.xpath(".//i[contains(@class, 'fa fa-cloud-upload')]"));// ("Click on file");
//		     
//		SitePageModel.waitFor(2);
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(15);
//
//		SitePageModel.tryClick(driver,By.id("createFileButton"));// ("Click on create");
//		SitePageModel.waitFor(7);
//		
//		String strExpect = "This is for testing file";
//		SitePageModel.waitFor(3);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//
//	}
//
//	@Test
//    @TestInfo(tcName="Upload File by 'Upload' Text",feature="Team File", expectedResult="File is uploaded")
//
//	public void tc_0437_UploadPrivateTeamFileByUploadText() throws AWTException {
//		
//		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//		SitePageModel.waitFor(2);
//		
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_File_name)) {
//				
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch(StaleElementReferenceException e)
//		{
//			e.toString();
//		}
//
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.id("fileForm"));// ("Click on file");
//		SitePageModel.waitFor(7);
//
//		SitePageModel.enterText(driver, By.tagName("textarea"),"This is testing file using Upload text");// Type description
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[3]/div[1]/div/span/b"));// ("Click on file");
//		SitePageModel.waitFor(2);
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(2);
//		SitePageModel.clearTextByRobotClass();
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile2.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(3);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//		SitePageModel.tryClick(driver,By.id("createFileButton"));// ("Click on create");
//		SitePageModel.waitFor(7);
//		
//		String strExpect = "This is testing file using Upload text";
//		SitePageModel.waitFor(3);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//
//
//	}
//	@Test
//    @TestInfo(tcName="Upload File by 'ClickHere' Text",feature="Team File", expectedResult="File is uploaded")
//
//	public void tc_0438_UploadPrivateTeamFileByClickHereText() throws AWTException {
//		
//		SitePageModel.waitFor(5);
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//		
//		
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_File_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch(StaleElementReferenceException e)
//		{
//			e.toString();
//		}
//
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.id("fileForm"));// ("Click on file");
//		SitePageModel.waitFor(2);
//
//		SitePageModel.enterText(driver, By.tagName("textarea"),"This is testing file using Click Here text");// Type description
//		SitePageModel.waitFor(1);
//		SitePageModel.tryClick(driver, By.xpath("//div[contains(@class, 'small') and text()='or click here']"));
//		SitePageModel.waitFor(2);
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile2.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(2);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver,By.id("createFileButton"));// ("Click on create");
//		SitePageModel.waitFor(7);
//		
//		String strExpect = "This is testing file using Click Here text";
//		SitePageModel.waitFor(5);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//
//	}
//
//	@Test
//    @TestInfo(tcName="Edit uploaded file",feature="Team File", expectedResult="File is upadated and uploaded")
//
//	public void tc_0439_EditPrivateTeamUploadFile() throws AWTException {
//
//		List<WebElement> teamNames = driver.findElements(By
//				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
//																	// team
//																	// names
//		SitePageModel.waitFor(3);
//		
//		try{
//		for (WebElement teamName : teamNames) {
//			if (teamName.getText().equals(team_File_name)) {
//				teamName.click();// Click Previously created team
//				break;
//			}
//		}
//		}
//		catch(StaleElementReferenceException e)
//		{
//			e.toString();
//		}
//		
//		SitePageModel.waitFor(5);
//		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
//		SitePageModel.waitFor(1);           
//		
//		SitePageModel.tryClick(driver, By.linkText("Edit"));
//		SitePageModel.waitFor(1);  
//
//		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited important file");// edit description
//		SitePageModel.waitFor(1);
//
//		
//		SitePageModel.tryClick(driver,By.xpath(".//i[contains(@class, 'fa fa-cloud-upload')]"));// ("Click on file");
//
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//		SitePageModel.waitFor(1);
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile3.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//		
//		SitePageModel.waitFor(1);
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(8);
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='updateFileButton']"));//("Click on update button");
//		SitePageModel.waitFor(7);
//	
//		String strExpect = "Edited important file";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//
//	}
	
	@Test
    @TestInfo(tcName="Like uploaded file",feature="Team File", expectedResult="Like number is increased")

	public void tc_0440_DeletePrivateTeamUploadFile() {

		List<WebElement> teamNames = driver.findElements(By
				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
																	// team
																	// names
		SitePageModel.waitFor(5);
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_File_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch(StaleElementReferenceException e){
			e.toString();
		}

		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));       
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation"));
		SitePageModel.waitFor(5);

	}

	
	@Test
    @TestInfo(tcName="Like uploaded file",feature="Team File", expectedResult="Like number is increased")

    public void tc_0441_LikeUploadFile(){
		
		SitePageModel.waitFor(5);		
		List<WebElement> teamNames = driver.findElements(By
				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
																	// team
																	// names
		SitePageModel.waitFor(5);
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_File_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch(StaleElementReferenceException e){
			e.toString();
		}

		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-thumbs-up']"));//like
		
		SitePageModel.waitFor(7);
		
		String strExpect = " 1 likes this.";
		SitePageModel.waitFor(10);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	@Test
    @TestInfo(tcName="Unlike uploaded file",feature="Team File", expectedResult="Like number is decreased")

    public void tc_0442_UnLikeUploadFile(){
		
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-thumbs-up']"));//unlike
		String strExpect = "1 likes this.";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertFalse(bFound);
	    SitePageModel.waitFor(3);
	}
	
	@Test
    @TestInfo(tcName="Add comment on uploaded file",feature="Team File", expectedResult="Comment is added")

    public void tc_0443_AddCommentToFile(){
		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
		SitePageModel.waitFor(3);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(3);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(2);
		
		
		String strExpect = "First comment";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);
	    SitePageModel.waitFor(3);
	}
	
	
	
	
	@Test
    @TestInfo(tcName="Edit comment on uploaded file",feature="Team File", expectedResult="Comment is updated")

    public void tc_0444_EditCommentToFile(){
		
		SitePageModel.waitFor(3);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edit First comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(3);
		
		String strExpect = "Edit First comment";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	
	@Test
    @TestInfo(tcName="Delete comment from a uploaded file",feature="Team File", expectedResult="Comment is deleted")

    public void tc_0445_DeleteCommentToFile(){
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(3);
	
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok");
		SitePageModel.waitFor(5);
		
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(3);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		
	}
	
	
	@Test
    @TestInfo(tcName="Delete file from a uploaded file",feature="Team File", expectedResult="File is deleted")

    public void tc_0446_DeleteUploadFile(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(25);		
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);		
		SitePageModel.tryClick(driver, By.linkText("Delete"));		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
		
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);
		
	}	
	
	@Test
	@TestInfo(tcName="Create Chat without Name",feature="Chat", expectedResult="Chat is created")
    public void tc_0460_CreateNewPrivateTeamMessage(){

		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Teams '+' icon 
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.name("title"),team_Chat_name);//'Team name' value
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("createGroupButton"));//Click 'create team' button
		
		SitePageModel.waitFor(10);
		
		List<WebElement> teamNames = driver.findElements(By
				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
																	// team
																	// names
		SitePageModel.waitFor(5);
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_Chat_name)) {
				teamName.click();// Click Previously created team
				break;
			}
		}
		}
		catch(StaleElementReferenceException e){
			e.toString();
		}

		SitePageModel.waitFor(3);
		
		//SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[1]/a"));
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-details']/div/div/div/a"));// ("Click on Invite members"));

		SitePageModel.waitFor(2);

		SitePageModel.enterText(driver,By.id("externalMailInput"), "sayedul@gmail.com");//Enter email
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
		SitePageModel.waitFor(2);


		SitePageModel.enterText(driver,By.name("title"),"user5");//enter value 'invite member'
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.id("user-user5@rootnext.com"));// Click on image
																									
		SitePageModel.waitFor(7);
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-success') and 'Add Guest']"));//Click 'Add Guest'
		SitePageModel.waitFor(2);


		SitePageModel.enterText(driver,By.name("title"),"user6");//enter value 'invite member'
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.id("user-user6@rootnext.com"));// Click on image
																									
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member' 
		SitePageModel.waitFor(50);
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='sidebar-tile-members']/div[2]/div/div/a[2]/img"));//check image of right side panel
		
		SitePageModel.waitFor(5);

        SitePageModel.tryClick(driver, By.id("chatForm"));//("Click on chat icon");
        SitePageModel.waitFor(7);
//
//        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
//        SitePageModel.waitFor(2);
//
//        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "User8");
////        SitePageModel.enterAutoCompleteText(driver,By.xpath("html/body/div[5]/div/input"), "Nine");//'invite member'
//        SitePageModel.waitFor(7);
//        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
//        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created chat");
        SitePageModel.waitFor(5);

        String strExpect = "User Five, User Six";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li/a/span[1]"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);
        SitePageModel.waitFor(5);
        String chat=driver.findElement(By.xpath(".//*[@id='lsb-chats-list']/li/a/span[1]")).getText();
        System.out.println(chat);
        SitePageModel.waitFor(5);
        SitePageModel.tryClick(driver,By.xpath(".//*[@id='lsb-chats-list']/li/a/span[contains(.,'User Five, User Six')]"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(2);
    }
	@Test
	@TestInfo(tcName="Create Chat with Name",feature="Chat", expectedResult="Chat with name is created")
    public void tc_0461_CreateChatWithName(){

        SitePageModel.waitFor(5);
        SitePageModel.tryClick(driver, By.id("chatForm"));//("Click on chat icon");
        SitePageModel.waitFor(2);

        SitePageModel.enterText(driver, By.name("title"), "Testing purpose");//Enter Title Text
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.id("removeChatMember104"));//remove user6
        
        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
        SitePageModel.waitFor(2);        
        
        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "user6");
        SitePageModel.waitFor(3);
        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(3);  
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created channel");
        SitePageModel.waitFor(10);

        String strExpect = "User Five, User Six";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li/a/span[contains(.,'User Five, User Six')]"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);
    }
	
	@Test
	@TestInfo(tcName="Open a chat",feature="Chat", expectedResult="Chat window is opened")
    public void tc_0462_OpenExistingWindow(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on chat icon");
        SitePageModel.waitFor(7);
    }
	
	@Test
	@TestInfo(tcName="Close chat window",feature="Chat", expectedResult="Chat window is closed")
    public void tc_0463_CloseExistingWindow() {

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on chat icon");
        SitePageModel.waitFor(2);

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Close']"));//("Click on any existing chat");
        SitePageModel.waitFor(2);
    }

	@Test
	@TestInfo(tcName="Send text by chat",feature="Chat", expectedResult="Text is submitted")
    public void tc_0464_WriteInChat() throws AWTException{

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);

        SitePageModel.enterText(driver, By.xpath(".//*[@id='chat-textarea']")," Testing purpose.");//("write on chat");
        SitePageModel.waitFor(7);

        SitePageModel.clickButtonByRobotClass();
        SitePageModel.waitFor(7);
    }
	
	@Test
    public void tc_0465_ReadLastMessageFromChat(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(15);

        String strExpect = "Testing purpose.";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/span/p"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);

        SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-times')]"));//("Click on close option");
        SitePageModel.waitFor(7);
    }

    @Test
	@TestInfo(tcName="Finish Chat",feature="Chat", expectedResult="Chat window is closed")
    public void tc_0467_FinishChatAndApprove(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(3);  

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");        
        SitePageModel.waitFor(2);        

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(3);


    }
    
    
    @Test
	@TestInfo(tcName="Cancel the Finish Chat confirmation",feature="Chat", expectedResult="Chat window remains opened")
    public void tc_0466_FinishChatCancelAndApprove(){

    	SitePageModel.waitFor(3);
    	SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(3); 
    	
    	SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
         
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-default']"));//("Click on cancel button of POPUP");
		SitePageModel.waitFor(4);

    }


    @Test
	@TestInfo(tcName="Leave chat by invitee",feature="Chat", expectedResult="Invitee is able to leave chat")
    public void tc_0468_LeaveChatAndApprove(){
    	
        SitePageModel.tryClick(driver, By.id("chatForm"));//("Click on chat icon");
        SitePageModel.waitFor(4);

        SitePageModel.enterText(driver, By.name("title"), "Testing purpose");//Enter Title Text
        SitePageModel.waitFor(3);
//
//        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
//        SitePageModel.waitFor(2);        
//        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Nine");
//        SitePageModel.waitFor(3);
//        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
//        SitePageModel.waitFor(5);  
//        
//        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
//        SitePageModel.waitFor(2);        
//        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Two");
//        SitePageModel.waitFor(3);
//        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(5);  
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created channel");
        SitePageModel.waitFor(12);

//        String strExpect = "Tania Najnin Toma, Afsana Akter";
//        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));
////        System.out.println(aboutTextElm.get(0).getText());
//        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//        Assert.assertTrue(bFound);
//        SitePageModel.waitFor(10);

        SitePageModel.tryClick(driver, By.id("user-options"));//("Click on setting");
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
        SitePageModel.waitFor(12);

        SitePageModel.enterText(driver, By.id("username"),"user6@rootnext.com");
        SitePageModel.waitFor(1);
        SitePageModel.enterText(driver, By.id("password"),"T@skor13");
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
        SitePageModel.waitFor(40);

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on invited chat");
        SitePageModel.waitFor(10);

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);        
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-sign-out']"));//("Click on finish"));
        SitePageModel.waitFor(2);
                
		SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
		SitePageModel.waitFor(12);
        
        SitePageModel.enterText(driver, By.id("username"),"user4@rootnext.com");
		SitePageModel.waitFor(1);
        SitePageModel.enterText(driver, By.id("password"),"T@skor13");
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
        SitePageModel.waitFor(30);
    }

    @Test
	@TestInfo(tcName="Change Chat Topic",feature="Chat", expectedResult="Creator is able to change Chat topic")
    public void tc_0469_ChatChangeTopic(){

    	SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);
        
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(5);
        
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(3);       
       
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-pencil']"));//("Click on Change Topic"));
        SitePageModel.waitFor(3);
       
        SitePageModel.enterText(driver, By.xpath("//form[@class='bootbox-form']/input")," Updated Testing purpose.");
        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(5);


    }

    @Test
	@TestInfo(tcName="Maximize and minimize chat window",feature="Chat", expectedResult="Chat window is maximized and minimized")
    public void tc_0470_ChatWindowMaximizeMinimize(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-unsorted']"));//("Click on chat window"));
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-unsorted']"));//("Click on chat window"));
        SitePageModel.waitFor(3);
        
        
        //Code for removing all created chat in this session
        
               
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(1);        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(1);
        
		SitePageModel.waitFor(5);
		
		List<WebElement> teamNames = driver.findElements(By
				.xpath(".//*[@id='lsb-external-list']/ul[1]/li"));// Collect All
																	// team
		SitePageModel.waitFor(3);															// names
		try{
		for (WebElement teamName : teamNames) {
			if (teamName.getText().equals(team_Chat_name)) {
				
				teamName.click();// Click Previously created team
				break;
				}
			}
		}
		catch (StaleElementReferenceException e){
	       e.toString();
	       
		}
		
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow

		SitePageModel.waitFor(1);
SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
		
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);
		
		
		SitePageModel.tryClick(driver, By.id("user-options"));//("Click on setting");
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
        SitePageModel.waitFor(12);

        SitePageModel.enterText(driver, By.id("username"),"user3@rootnext.com");
        SitePageModel.waitFor(1);
        SitePageModel.enterText(driver, By.id("password"),"T@skor13");
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
        SitePageModel.waitFor(30);
    } 
	
}
