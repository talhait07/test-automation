package com.automation.suite;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.AppConstant;

public class TestSuit_Team_ESN_New extends TestHelper{
	
	@Test
	@TestInfo(tcName="Invite member to a team",feature="Team", expectedResult="member is included into team")
	public void tc_0402_InviteMember() {
		
		

		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		
			
			SitePageModel.waitFor(10);
			
			//SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[1]/div/div[2]/div[2]/ul/li[1]/a"));

			SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-details']/div/div/div/a"));// ("Click on Invite members"));

			SitePageModel.waitFor(2);

			SitePageModel.enterText(driver,By.name("title"), "user1.esn.coc@rootnext.com");//Enter email
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[2]/div/div/button[2]"));// Click update memeber
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver,By.xpath(".//*[@id='sidebar-tile-details']/div/div/div/a"));// ("Click on Invite members"));
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='user-user2.esn.coc@rootnext.com']/span"));
			SitePageModel.waitFor(1);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[2]/div/div/button[2]"));// Click update memeber
	}
	
	@Test
	@TestInfo(tcName="Create team using invalid info",feature="Team", expectedResult="Error message is displayed")
	public void tc_0406_CreatePublicTeamWithBlankInfo() {
		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver,By.id("lsb-external"));//Click on Public Teams 
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[3]/div/div/div[1]/div[3]/a"));
		SitePageModel.waitFor(3);

		SitePageModel.tryClick(driver, By.id("createGroupButton"));//click 'Create team' button
		SitePageModel.waitFor(25);

		String strExpectedPostError = "Required field.";
		List<WebElement> strActualPostError = driver
				.findElements(By
						.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/span"));//warning message
		SitePageModel.waitFor(10);
		boolean bFound = strActualPostError.get(0).getText()
				.contains(strExpectedPostError);
		Assert.assertTrue(bFound);
		

	}
	
	
	@Test
	@TestInfo(tcName="Create post on a team",feature="Team Post", expectedResult="Post is created on a team")
	public void tc_0407_CreatePublicTeamPost() throws AWTException {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("postForm"));// ("Click on Post");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post");//enter value in 'text' field
		SitePageModel.waitFor(2);
		
		String strUploadImage = AppConstant.IMAGE_PATH + "index.png";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(5);
	    upload.sendKeys(strUploadImage);
	    
	    SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));// Click on 'create' button
		SitePageModel.waitFor(12);
		String strExpectedPost = "Test Automation Post";
		List<WebElement> strActualPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		                                                               
		boolean bFound = strActualPost.get(0).getText().contains(strExpectedPost);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	
	@Test
	@TestInfo(tcName="Create post with required information on team",feature="Team Post", expectedResult="Post is created using required information")
	public void tc_0408_CreatePublicTeamPostWithRequiredInfo() {
		
		

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='postForm']"));// Click on Post
		SitePageModel.waitFor(4);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post with Required Field");//Text field value
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));// Click on 'create' button
		SitePageModel.waitFor(20);
		
		String strExpectedPost = "Test Automation Post with Required Field";
		List<WebElement> strActualPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));

		boolean bFound = strActualPost.get(0).getText()
				.contains(strExpectedPost);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(20);

	}
	
	@Test
 	@TestInfo(tcName="Edit a post",feature="Team Post", expectedResult="Post is updated")
	public void tc_0410_EditPublicTeamPost() {
		
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
	
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(7);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Automation Post Editing");
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='updatePostButton']"));// Click Update button
		SitePageModel.waitFor(15);

		String strExpectedEditedPost = "Test Automation Post Editing";
		List<WebElement> strActualEditedPost = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		SitePageModel.waitFor(5);
		boolean bFound = strActualEditedPost.get(0).getText()
				.contains(strExpectedEditedPost);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);
	}

	@Test
	@TestInfo(tcName="Delete Team for post",feature="Team Deletion", expectedResult="Team is deleted")
	public void tc_0411_DeletePublicTeamPost() {
		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		
		
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// Click OK button of confirmation
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
		
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);
	
	}

	@Test
	@TestInfo(tcName="Create a team announcement",feature="Team Announcement", expectedResult="Announcement is created")
	public void tc_0412_CreatePublicTeamAnnouncement() {
		
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("announcementForm"));// ("Click on Announcement");
		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.name("title"),"Test team Announcement");//enter value in 'title' field
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Announcement description");//enter value in 'Text' field
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(35);

		String strExpectedAnnouncement = "Test team Announcement";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//first announcement

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Create a team announcement with required field",feature="Team Announcement", expectedResult="Announcement is created only with required fields")
	public void tc_0413_CreatePublicTeamAnnouncementWithRequiredField() {
		
	
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.id("announcementForm"));// Open Announcement form
		SitePageModel.waitFor(3);

		SitePageModel.enterText(driver,By.name("title"),"Announcement with required field");
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver,By.tagName("textarea"),"Announcement with required field description");//enter value in 'Text' field
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(12);

		String strExpectedAnnouncement = "Announcement with required field";
		List<WebElement> strActualAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//last announcement

		boolean bFound = strActualAnnouncement.get(0).getText().contains(strExpectedAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Create announcement with invalid information",feature="Team Announcement", expectedResult="Error message is displayed")
	public void tc_0414_CreatePublicTeamAnnouncementWithBlankInfo() {

		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
				
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("announcementForm"));// ("Click on announcement");
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver,By.id("createAnnouncementButton"));// ("Click on Create");
		SitePageModel.waitFor(2);

		String strExpectedErrorAnnouncement = "Required field.";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/span"));

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedErrorAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);

	}

	@Test
	@TestInfo(tcName="Edit a team announcement",feature="Team Announcement", expectedResult="Announcement is updated")
	public void tc_0415_EditPublicTeamAnnouncement() {
		
		SitePageModel.waitFor(5);

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.linkText("Edit"));//(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/ul/li/ul/li[1]/a"));// Click on Edit
		SitePageModel.waitFor(7);

		SitePageModel.enterText(driver, By.name("title"),"Test Announcement Edit");//last announcement
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.tagName("textarea"),"Test Announcement description edit");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.id("updateAnnouncementButton"));// ("Click on Update");
		SitePageModel.waitFor(12);

		String strExpectedEditedAnnouncement = "Test Announcement Edit";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);

	}

	@Test
	@TestInfo(tcName="Delete a team announcement",feature="Team Announcement", expectedResult="Announcement is deleted")
	public void tc_0416_DeletePublicTeamAnnouncement() {
		
	
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'OK')]"));// accept confirmation
		SitePageModel.waitFor(7);
		 
		SitePageModel.tryClick(driver,By.xpath(".//span[contains(@class, 'caret')]"));//Click on 'Management' arrow
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver,By.linkText("Archive Team"));// Select Close Team
			
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class,'btn btn-success')]"));// accept confirmation
		SitePageModel.waitFor(10);

	}

	@Test
	@TestInfo(tcName="Create a team Event with required field",feature="Team Event", expectedResult="Event is created")
	public void tc_0417_CreatePublicTeamEventWithRequiredField() throws AWTException {
		

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.name("title"),"QA learning Event");//title
		SitePageModel.waitFor(2);
		
		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.waitFor(2);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));//Click 'location' field
		SitePageModel.waitFor(5);
		
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(6);
		allElement.get(0).sendKeys("Bangladesh");
		SitePageModel.waitFor(3);	
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		SitePageModel.waitFor(2);	
		SitePageModel.tryClick(driver,By.id("createEventButton"));// ("Click on create"));
		SitePageModel.waitFor(7);
		
		
		String strEvent = "QA learning Event";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strEvent);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(5);
		


	}
	
	@Test
	@TestInfo(tcName="Create a team Event with using Allday",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0418_CreatePublicTeamEvent_WithAllDay() throws AWTException {

		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team

		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.name("title"),"QA learning Event two");//title
		SitePageModel.waitFor(1);

		
		
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div/div/label"));//Click on 'all day' event;
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));//Click 'location' field
		SitePageModel.waitFor(5);
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		
		SitePageModel.waitFor(6);
		allElement.get(0).sendKeys("Bangladesh");

		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));

		SitePageModel.waitFor(3);
		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[5]/div[1]/textarea"),"QA meeting");//Description
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='createEventButton']"));// ("Click on create"));
		SitePageModel.waitFor(7);
		
		String strExpectedEditedAnnouncement = "QA learning Event two";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));

		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(5);

	}
	
	@Test
	@TestInfo(tcName="Create a team Event with inviting member",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0419_CreatePublicTeamEvent_WithInviteMember() throws AWTException {
		
SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team

		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.name("title"),"QA learning Event two");//title
		SitePageModel.waitFor(1);

		
		
		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/div/div/label"));//Click on 'all day' event;
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));//Click 'location' field
		SitePageModel.waitFor(5);
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		
		SitePageModel.waitFor(6);
		allElement.get(0).sendKeys("Bangladesh");

		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		SitePageModel.waitFor(8);
		SitePageModel.tryClick(driver, By.xpath("span[contains(@class, 'select2-chosen') and 'Find a member...'"));
		//span[contains(@class, 'select2-chosen') and normalize-space()='Find a member...']


	}
	
	@Test
	@TestInfo(tcName="Create a team Event with invalid data",feature="Team Event", expectedResult="Error message is displayed")
	public void tc_0420_CreatePublicTeamEventBlankField() {
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver,By.id("createEventButton"));// ("Click on create"));
		SitePageModel.waitFor(3);
		SitePageModel.scrollTopOfPage(driver);
		SitePageModel.waitFor(2);
		String strExpectedErrorAnnouncement = "Required field.";
		List<WebElement> strActualErrorAnnouncement = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/span"));

		boolean bFound = strActualErrorAnnouncement.get(0).getText().contains(strExpectedErrorAnnouncement);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(4);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);

	}
	
	
	@Test
	@TestInfo(tcName="Create a team event by fill-up all field",feature="Team Event", expectedResult="Event is created successfully")
	public void tc_0421_CreatePublicTeamEvent_AllInfo() throws AWTException {
	
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(4);
		SitePageModel.tryClick(driver, By.id("eventForm"));// ("Click on event form");
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver,By.name("title"),"Important event for QA meeting two");//title
		SitePageModel.waitFor(1);

		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and 'Select location']"));//Click 'location' field
		SitePageModel.waitFor(4);
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(9);
		allElement.get(0).sendKeys("Bangladesh");

		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
		SitePageModel.waitFor(2);
		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[5]/div[1]/textarea"),"QA meeting");//description
		SitePageModel.waitFor(1);

//		SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Find a member...']"));//("Click member field"));
//
//	    SitePageModel.waitFor(5);

//	    List<WebElement>allElements=driver.findElements(By.tagName("input"));
//		SitePageModel.waitFor(2);		
//		allElements.get(0).sendKeys("user8");
//
//		SitePageModel.waitFor(2);
//		SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
//		SitePageModel.waitFor(3);
		String strUploadImage = AppConstant.IMAGE_PATH + "index.png";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(5);
	    upload.sendKeys(strUploadImage);

		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-info btn-small']"));//("Click on update button"));
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("createEventButton"));
		SitePageModel.waitFor(50);
		
		String strExpectedEditedAnnouncement = "Important event for QA meeting two";
		List<WebElement> strActualEditedAnnouncement = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//last created event
		SitePageModel.waitFor(5);
		boolean bFoundEditedText = strActualEditedAnnouncement.get(0).getText()
				.contains(strExpectedEditedAnnouncement);
		Assert.assertTrue(bFoundEditedText);
		
		SitePageModel.waitFor(5);
			
	}
	
  @Test
  @TestInfo(tcName="Cancel during editing a event",feature="Team Event", expectedResult="Team event editing mode is closed")
  public void tc_0422_EditPublicTeamEventCancel() throws AWTException {
		
		driver.navigate().refresh();		
		SitePageModel.waitFor(25);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));// ("Click on Dropdown"));
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(@class, 'btn btn-default btn-small') and 'Cancel']"));//Click 'cancel' 
		
        SitePageModel.waitFor(7);
		
//        String strExpectedEditedEvent = "Important event for QA meeting two";
//		List<WebElement> strActualEditedEvent = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//last created event
//		SitePageModel.waitFor(2);
//
//		boolean bFoundEditedText = strActualEditedEvent.get(0).getText()
//				.contains(strExpectedEditedEvent);
//		Assert.assertTrue(bFoundEditedText);
//		SitePageModel.waitFor(3);
        
	}
	

  @Test
	@TestInfo(tcName="Create a team poll for multiple day",feature="Team Poll", expectedResult="Team Poll is created with multiple day")
	public void tc_0425_CreatePublicTeamMultipleChoicePollForMultipleDay() {

		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC"));
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));
		
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.name("title"),"What is our national flower");
		SitePageModel.waitFor(2);

		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(4);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");//Description
		SitePageModel.waitFor(2);

		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on Add Answers");
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Water Lily");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Rose");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"lotus");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPollButton']"));// ("Click on create button");
		SitePageModel.waitFor(10);

		String strExpect = "What is our national flower";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h5[2]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);
	}

	@Test
	@TestInfo(tcName="Create team poll for one day",feature="Team Poll", expectedResult="Team Poll is created for one day")
	public void tc_0426_CreatePublicTeamMultipleChoicePollForOneDay() {
		
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC"));

		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//multiple choice
		
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.name("title"),"What is our national bird");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		SitePageModel.waitFor(2);
		String startDate= SitePageModel.AddDateWithToday(1);
		SitePageModel.waitFor(1);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventAllDayField']/div/input"), startDate);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");//Description
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on add form");
		SitePageModel.waitFor(3);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Peacock");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Magpie Robin");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver,By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Crow");
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.id("createPollButton"));// ("Click on create button");
		SitePageModel.waitFor(10);

		List<WebElement> pollNames = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li"));

		try{
			
		for (WebElement pollName : pollNames) {
			if (pollName.getText().equals("What is our national bird")) {
				System.out.println(pollName.getText());
				break;
			}
		}}
		catch (StaleElementReferenceException e){
	       e.toString();
	}
		
		SitePageModel.waitFor(5);
	
	}
	
	
	
	@Test
	@TestInfo(tcName="Like to a Poll",feature="Public Team Poll", expectedResult="Like number is increased")
    public void tc_0457_LikePoll(){
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    SitePageModel.waitFor(2);

	}
	@Test
	@TestInfo(tcName="Unlike to a Poll",feature="Public Team Poll", expectedResult="Like number is decreased")
   public void tc_0458_UnLikePoll(){
		
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.linkText("Unlike"));//("Click on unlike");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    SitePageModel.waitFor(2);

	}
	
	
	@Test
	@TestInfo(tcName="Create team poll with invalid data",feature="Team Poll", expectedResult="Error message is displayed")
	public void tc_0427_CreatePublicTeamPollBlankField() {
		
		SitePageModel.waitFor(3);

		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC"));

		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.id("pollForm"));// ("Click on poll form");
		SitePageModel.waitFor(5);
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));
		
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPollButton']"));// ("Click on create button");
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));
		SitePageModel.waitFor(5);
	}

	@Test
	@TestInfo(tcName="Edit team poll",feature="Team Poll", expectedResult="Team poll is updated")
	public void tc_0428_EditPublicTeamPollOneDay() throws InterruptedException {
		

		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC"));

		SitePageModel.waitFor(6);

		SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));//Click down arrow icon	 
		
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.linkText("Edit"));

		SitePageModel.waitFor(3);

		//SitePageModel.tryClick(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[2]/div/div/label"));// ("Click on all day event"));
		//SitePageModel.waitFor(7); 
		
        SitePageModel.scrollDownOfPage(driver);
		SitePageModel.enterText(driver, By.tagName("textarea"), "This is edited poll of Bird");
		SitePageModel.waitFor(2);
		String startDate= SitePageModel.AddDateWithToday(0);
		SitePageModel.waitFor(1);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventAllDayField']/div/input"), startDate);
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("addPollOption"));// ("Click on add form");
		SitePageModel.waitFor(5);
		SitePageModel.scrollDownOfPage(driver);
		SitePageModel.waitFor(5);
		SitePageModel.enterText(driver,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[4]/div[2]/div[5]/input"),"Parrot");
		SitePageModel.waitFor(1);
		SitePageModel.tryClick(driver, By.id("updatePollButton"));// ("Click on update button");
		

   	SitePageModel.waitFor(30);

		String strExpect = "This is edited poll of Bird";
		String parrotXpath=".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[2]/p";
		//String print_parrotXpath = driver.findElement(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[2]/p")).getText();
		//SitePageModel.waitFor(3);                     
		
      //System.out.println(print_parrotXpath);
		SitePageModel.waitFor(3);
		
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(parrotXpath));//parrot
		                
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		SitePageModel.waitFor(5);
	}
	
	@Test
	@TestInfo(tcName="Cast vote on All day Poll",feature="Public Team Poll", expectedResult="Vote is cast in Poll")
    public void tc_0456_CastVoteInPoll(){
		
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		

		SitePageModel.waitFor(5);
		
		SitePageModel.selectRadioText(driver, By.xpath("//label[text()='Magpie Robin']"));// click radio button
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary btn-mini btn-cons m-t-5' and text() = 'Vote']"));//("Click on vote");	
		SitePageModel.waitFor(7);
		
		SitePageModel.checkMenuText(driver, "1 vote", By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[4]/a"));

		
	}
	
	@Test
	@TestInfo(tcName="Add comment on a poll",feature="Public team Poll", expectedResult="Comment is added")
    public void tc_0471_AddCommentToPoll(){
		
		
		
		SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);

	}
	
	
	@Test
	@TestInfo(tcName="Edit comment on a poll",feature="Public team Poll", expectedResult="Comment is updated")
    public void tc_0472_EditCommentToPoll(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(7);
		
	}
	
	
	@Test
	@TestInfo(tcName="Delete comment from a Poll",feature="Public team Poll", expectedResult="Comment is deleted")
     public void tc_0473_DeleteCommentToPoll(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(7);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);

	}
	
	
	@Test
	@TestInfo(tcName="Delete a poll",feature="Public Teams Poll", expectedResult="Poll is deleted")
    public void tc_0342_DeletePoll(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
		SitePageModel.waitFor(7);
		
		
		
	}
	
	@Test
	@TestInfo(tcName="Create poll with rating scale",feature="Public Teams Poll", expectedResult="Poll is created with rating")
    public void tc_0354_CreatePollInRatingScale(){
		
		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your name?");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//a[@class='semi-bold poll-more-details']"));//("Click on more details");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");
		SitePageModel.waitFor(1);
				
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[2]/input"),"A");
		SitePageModel.waitFor(10);

		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[3]/input"),"B");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[4]/input"),"C");
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[5]/input"),"D");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[6]/input"),"E");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(10);
		
		
		String strExpect = "What is your name?";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your name?']"));
	    boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound); 
		SitePageModel.waitFor(2);
		
	}
	
	
	
	@Test
	@TestInfo(tcName="Create poll with invalid data",feature="Public Teams Poll", expectedResult="Error message is displayed")
    public void tc_0355_CreatePollInRatingScaleWithBlankField(){
		
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team
		SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(10);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
		SitePageModel.waitFor(10);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(7);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check question required field
		SitePageModel.waitFor(2);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[1]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[2]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[3]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[4]"));//Check question required field
		SitePageModel.waitFor(2);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[5]"));//Check answer required field
		SitePageModel.waitFor(10);
		
		
		
	}
	@Test
	@TestInfo(tcName="Open File tab view on a Public Teams",feature="Public Teams File", expectedResult="File window is opened")
    public void tc_0343_OpenFileWindow(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.linkText("01 RN Public Team ESN_COC")); // Click on 01 RN Public Team ESN_COC team		
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
		SitePageModel.waitFor(7);
		
		String strExpect = "Description";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/label"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		
	}
	
	
}
