package com.automation.suite;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.AppConstant;
import com.automation.util.ApplicationUtility;

public class TestSuite_Channel_Everyone extends TestHelper {
	
	String post=SitePageModel.randomStringGenerator(driver);
	String channelPost= post + "Test channel post ";
	
	String Announcement=SitePageModel.randomStringGenerator(driver);
	String channelAnnouncement= Announcement +"Test channel Announcement ";
	
	String Event=SitePageModel.randomStringGenerator(driver);
	String channelEvent= Event + "Test channel Event ";
	
	String Poll=SitePageModel.randomStringGenerator(driver);
	String channelPoll= Poll + "Test channel Poll ";
	
	String File=SitePageModel.randomStringGenerator(driver);
	String channelFile= File + "Test channel File ";
	
		
 
	
	
	@Test
	@TestInfo(tcName="Create a post on everyone channel",feature="Channel Post", expectedResult="Post is created on everyone channel successfully")
		public void tc_0501_CreatePost() throws AWTException{
			
			driver.navigate().to("https://secure.connectik.com");		
			SitePageModel.waitFor(30);			
		
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);			
			SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"channel_post_name");//("type post text");
			SitePageModel.waitFor(1);
			
			String strUploadImage = AppConstant.IMAGE_PATH + "TestPost.jpg";
		    WebElement upload = driver.findElement(By.id("attachImageButton"));
		    SitePageModel.waitFor(10);
		    upload.sendKeys(strUploadImage);
		    
		    SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
			SitePageModel.waitFor(5);
			

		}
		
		
		
	@Test
	@TestInfo(tcName="Create post using invalid data",feature="Channel Post", expectedResult="Error message is displayed")
     public void tc_0502_CreateBlankPost(){
			
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
			SitePageModel.waitFor(5);
			
			String strExpect = "Required field.";
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='postCreateForm']/span"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			
			SitePageModel.waitFor(3);
		}
		
		
		@Test
		@TestInfo(tcName="Edit a post on everyone channel",feature="Channel Post", expectedResult="Post is updated successfully")
	     public void tc_0503_EditPost(){
			
			SitePageModel.waitFor(3);			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(4);
			
			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
			SitePageModel.waitFor(4);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited channel post name");//("edit text"));
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='updatePostButton']"));//("Click on update button"));
			SitePageModel.waitFor(5);
		
			String strExpect = "Edited channel post name";
			SitePageModel.waitFor(3);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			
			SitePageModel.waitFor(2);
		}
		
		@Test
		@TestInfo(tcName="Like an Post",feature="Channel Post", expectedResult="Like number is increased")
	    public void tc_0543_LikePost(){
			
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "1";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
			}
		
		
		@Test
		@TestInfo(tcName="UnLike an Post",feature="Channel Post", expectedResult="Like number is decreased")
	    public void tc_0544_UnLikePost(){
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "Like";
			SitePageModel.waitFor(2);									  
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}

		
		@Test
		@TestInfo(tcName="Add comment on a post of a Channel",feature="Channel Post", expectedResult="Comment is added under post")
	    public void tc_0545_AddCommentToPost(){
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(5);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(5);
			
			
			String strExpect = "First comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		    
		    String strExpect1 = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
			boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		    Assert.assertTrue(bFound1);
		}
		
		
		
		@Test
		@TestInfo(tcName="Edit comment on a post of a Channel",feature="Channel Post", expectedResult="Comment is Edit under post of Channel")

	    public void tc_0546_EditCommentToPost(){
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);
			
		
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.linkText("Edit"));
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
			SitePageModel.waitFor(17);
			
			
			String strExpect = "Edited first comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		
		@Test
		@TestInfo(tcName="Delete comment from post of a Channel",feature="Channel Post", expectedResult="Comment is deleted under post of Channel")
	    public void tc_0547_DeleteCommentFromPost(){
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);
		
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(5);
			
			String strExpect = "Second comment";
			SitePageModel.waitFor(10);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		
		
		@Test
		@TestInfo(tcName="Delete a post",feature="Channel Post", expectedResult="Post is Deleted")
	     public void tc_0504_DeletePost(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on post dropdown");
			SitePageModel.waitFor(3);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(3);
				
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(2);	
			
		}
		
		@Test
		@TestInfo(tcName="Create a post on everyone channel",feature="Channel Post", expectedResult="Post is created on everyone channel successfully")
			public void tc_0548_CreatePostUsingOnlyRequiredField() {
				
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);	
			
				SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
				SitePageModel.waitFor(10);			
				SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
				SitePageModel.waitFor(3);
				
				SitePageModel.enterText(driver, By.tagName("textarea"),"Using only required field channel post name");//("type post text");
				SitePageModel.waitFor(1);
				
				SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
				SitePageModel.waitFor(5);
				
				SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on post dropdown");
				SitePageModel.waitFor(3);
				
				SitePageModel.tryClick(driver, By.linkText("Delete"));
				SitePageModel.waitFor(3);
					
				SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
				SitePageModel.waitFor(2);

			}
		
		@Test
		@TestInfo(tcName="Create a Announcement on everyone channel",feature="Channel Announcement", expectedResult="Announcement is created successfully")
	     public void tc_0505_CreateAnnouncement(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("announcementForm"));//("Click on Announcement");
			SitePageModel.waitFor(3);
		
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Test channel announcement name");//"announcement title";
			SitePageModel.waitFor(1);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"This is automation testing purpose.");//"Announcement description";
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createAnnouncementButton']"));//("Click on create button"));
			SitePageModel.waitFor(3);
		
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));//Check announcement title
			SitePageModel.waitFor(1);

			String strExpect = "Test channel announcement name";
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			
			SitePageModel.waitFor(1);			
		}
		
		
		@Test
		@TestInfo(tcName="Create Announcement with invalid data",feature="Channel Announcement", expectedResult="Error message is dispalyed")
	     public void tc_0506_CreateAnnouncementWithBlankInfo(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("announcementForm"));//("Click on Announcement");
			SitePageModel.waitFor(3);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createAnnouncementButton']"));//("Click on create button"));
			SitePageModel.waitFor(2);

			String strExpect = "Required field.";
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/span"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			
			SitePageModel.waitFor(1);
			
			String strExpect2 = "Required field.";
			List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/span"));
			boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
			Assert.assertTrue(bFound2);
			
			SitePageModel.waitFor(1);
			
		}
		
		@Test
		@TestInfo(tcName="Edit a Announcement",feature="Channel Announcement", expectedResult="Announcement is updated")
	     public void tc_0507_EditAnnouncement(){

			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(3);
			
			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
			SitePageModel.waitFor(2);
		
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Edited channel announcement name");
			SitePageModel.waitFor(1);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited this is automation testing purpose.");
			SitePageModel.waitFor(1);
		
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='updateAnnouncementButton']"));//("Click on update button");
			SitePageModel.waitFor(4);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//Check announcement title
			SitePageModel.waitFor(1);
			
			String strExpect = "Edited channel announcement name";
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound);
			
			SitePageModel.waitFor(1);
		}
		
		@Test
		@TestInfo(tcName="Like an Announcement",feature="Channel Announcement", expectedResult="Like number is increased")
	    public void tc_0549_LikeAnnouncement(){
			
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "1";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
			}
		
		
		@Test
		@TestInfo(tcName="UnLike an Announcement",feature="Channel Announcement", expectedResult="Like number is decreased")
	    public void tc_0550_UnLikeAnnouncement(){
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "Like";
			SitePageModel.waitFor(2);									  
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		@Test
		@TestInfo(tcName="Add Comment under Announcement portion",feature="Channel Announcement", expectedResult="Comment is added")

	    public void tc_0551_AddCommentToAnnouncement(){
			
			
			SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			
			String strExpect = "First comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		    
		    String strExpect1 = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
			boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		    Assert.assertTrue(bFound1);
		}
		
		
		
		@Test
		@TestInfo(tcName="Edit Comment under Announcement portion",feature="Channel Announcement", expectedResult="Comment is edited")
	    public void tc_0552_EditCommentToAnnouncement(){
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.linkText("Edit"));
			SitePageModel.waitFor(5);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
			SitePageModel.waitFor(17);

			String strExpect = "Edited first comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		@Test
		@TestInfo(tcName="Delete Comment from Announcement portion",feature="Channel Announcement", expectedResult="Comment is Deleted")
	    public void tc_0553_DeleteCommentFromAnnouncement(){
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(7);
			
			String strExpect = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		@Test
		@TestInfo(tcName="Delete a Announcement",feature="Channel Announcement", expectedResult="Announcement is deleted")
	       public void tc_0508_DeleteAnnouncement(){
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(2);

			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button");
			SitePageModel.waitFor(3);
		
			
		}
		
		
		
		@Test
		@TestInfo(tcName="Create a Allday Event on everyone channel",feature="Channel Event", expectedResult="Allday Event is created")
	     public void tc_0509_CreateEventForAllDay() throws AWTException{
					    
		    String AllDaychannelEvent= "Alldayevent";
		    String location = "Bangladesh";
		    
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
					
			SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),AllDaychannelEvent);
			SitePageModel.waitFor(1);
			
			
			if(driver.findElement(By.className("select2-chosen")).getText().equals("Select location")){

	            SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
	            SitePageModel.waitFor(3);
	            List<WebElement> listInput=driver.findElements(By.tagName("input"));

	            listInput.get(0).sendKeys(location);
	            SitePageModel.waitFor(5);
	            SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
	            SitePageModel.waitFor(3);
			}
			
			
			SitePageModel.tryClick(driver, By.tagName("textarea"));//("Click on description"));
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"QA meeting");// type description
			SitePageModel.waitFor(1);
		
			String strUploadImage = AppConstant.IMAGE_PATH + "TestEvent.jpg";
		    WebElement upload = driver.findElement(By.id("attachImageButton"));
		    upload.sendKeys(strUploadImage);
		    
		    SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("createEventButton"));//("Click on create"));
			SitePageModel.waitFor(8);
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
					    
			 String strExpect = "Alldayevent";
			 SitePageModel.waitFor(2);
			 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
			 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			 Assert.assertTrue(bFound);
		}
		
		@Test
		@TestInfo(tcName="Create a Multiple day Event",feature="Channel Event", expectedResult="Multiple day Event is created")
	      public void tc_0510_CreateEventForMultipleDay() throws AWTException{
			
		
		    SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on Channel"));
			SitePageModel.waitFor(2);
					    
		    String MultipleDaychannelEvent= "Multipledayevent";
		    String location = "Bangladesh";
		    
		    SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-feeds-list']/ul[1]/li[1]/a/span[1]"));//("Click on Everyone Channel"));
			SitePageModel.waitFor(4);
			
			SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),MultipleDaychannelEvent);//("type channel name");
			SitePageModel.waitFor(1);
			
			String startDate= SitePageModel.AddDateWithToday(3);
			SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
			String endDate= SitePageModel.AddDateWithToday(5);
			SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
			SitePageModel.waitFor(3);

			
			if(driver.findElement(By.className("select2-chosen")).getText().equals("Select location")){

	            SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
	            SitePageModel.waitFor(3);
	            List<WebElement> listInput=driver.findElements(By.tagName("input"));
	            listInput.get(0).sendKeys(location);
	            SitePageModel.waitFor(5);
	            SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
	            SitePageModel.waitFor(3);
			}
			
			
			SitePageModel.enterText(driver,By.tagName("textarea"),"QA meeting");// type description
			SitePageModel.waitFor(2);
			
			String strUploadImage = AppConstant.IMAGE_PATH + "TestEvent2.jpg";
		    WebElement upload = driver.findElement(By.id("attachImageButton"));
		    upload.sendKeys(strUploadImage);
		    
		    SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createEventButton']"));//("Click on create"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
		
			    
			    String strExpect = "Multipledayevent";
				 SitePageModel.waitFor(2);
				 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
				 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
				 Assert.assertTrue(bFound);
			
		}	
		
		
		@Test
		@TestInfo(tcName="Create Event with invalid data",feature="Channel Event", expectedResult="Error message is displayed")
	    public void tc_0511_CreateEventWithBlankInfo(){
			
		    SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on Channel"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);

			SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
			SitePageModel.waitFor(7);

			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createEventButton']"));//("Click on create"));
			SitePageModel.waitFor(7);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check required field
			SitePageModel.waitFor(1);
			
		}
		
		
		@Test
		@TestInfo(tcName="Edit an Event",feature="Channel Event", expectedResult="Event is updated")
	    public void tc_0512_EditEventInChannel() throws AWTException{
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Logout link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
			SitePageModel.waitFor(2);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Edited multiple day");//("edit title")
			SitePageModel.waitFor(1);
			
			SitePageModel.enterText(driver,By.tagName("textarea"),"Edited QA meeting");// type description
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.id("updateEventButton"));
			SitePageModel.waitFor(15);
			
			String strExpect = "Edited multiple day";
			 SitePageModel.waitFor(2);
			 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
			 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			 Assert.assertTrue(bFound);
			
			
		}
		
		
		@Test
		@TestInfo(tcName="Like an Event",feature="Channel Event", expectedResult="Like number is increased")
	     public void tc_0514_LikeEvent(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-feeds-list']/ul[1]/li[1]/a/span[1]"));//("Click on Everyone Channel"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "1";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a[2]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
			
			
		}
		
		
		@Test
		@TestInfo(tcName="UnLike an Event",feature="Channel Event", expectedResult="Like number is decreased")
	     public void tc_0515_UnLikeEvent(){
			
			SitePageModel.waitFor(5);
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-feeds-list']/ul[1]/li[1]/a/span[1]"));//("Click on Everyone Channel"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "Like";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}

		
		@Test
		@TestInfo(tcName="Add comment on Event",feature="Channel Event", expectedResult="Comment is added")
	    public void tc_0516_AddCommentToEvent(){
			
			SitePageModel.waitFor(10);
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(13);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(15);
			
			String strExpect = "First comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		    
		    String strExpect1 = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[3]/div/div/div[2]/p/p"));
			boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		    Assert.assertTrue(bFound1);
		}
		
		
		
		@Test
		@TestInfo(tcName="Edit comment to an Event",feature="Channel Event", expectedResult="Comment is updated")
	      public void tc_0517_EditCommentToEvent(){
			
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
			SitePageModel.waitFor(2);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
			SitePageModel.waitFor(9);
			
			
			String strExpect = "Edited first comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		
		@Test
		@TestInfo(tcName="Delete comment to an Event",feature="Channel Event", expectedResult="Comment is deleted")
	    public void tc_0518_DeleteCommentFromEvent(){
			
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(10);
			
			String strExpect = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}		
		
		
		@Test
		@TestInfo(tcName="Delete an Event",feature="Channel Event", expectedResult="Event is deleted")
	    public void tc_0519_DeleteEvent(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver,By.linkText("Delete"));// Select delete
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
			SitePageModel.waitFor(7);			
		}
		@Test
		@TestInfo(tcName="Create a event on everyone channel",feature="Channel Event", expectedResult="Event is created on everyone channel successfully")
			public void tc_0554_CreateEventUsingOnlyRequiredField() {
				
			driver.navigate().refresh(); 
			SitePageModel.waitFor(20);	
			
				SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
				SitePageModel.waitFor(10);	
				
				SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
				SitePageModel.waitFor(3);
				
				SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"EventUsingOnlyRequiredField");//("type channel name");
				SitePageModel.waitFor(1);
				
				SitePageModel.tryClick(driver, By.xpath(".//*[@id='createEventButton']"));//("Click on create"));
				SitePageModel.waitFor(5);
				
				SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on post dropdown");
				SitePageModel.waitFor(3);
				
				SitePageModel.tryClick(driver, By.linkText("Delete"));
				SitePageModel.waitFor(3);
					
				SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
				SitePageModel.waitFor(2);

			}
		
	
		@Test
		@TestInfo(tcName="Create Poll on everyone channel",feature="Channel Poll", expectedResult="Poll is created")
	    public void tc_0520_CreatePollWithBlankField(){
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
			SitePageModel.waitFor(8);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
			SitePageModel.waitFor(10);

			SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
			SitePageModel.waitFor(7);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check question required field
			SitePageModel.waitFor(2);
		
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/span"));//Check answer required field
			SitePageModel.waitFor(2);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/span[1]"));//Check answer required field
			SitePageModel.waitFor(2);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/span[2]"));//Check answer required field
			SitePageModel.waitFor(2);
			
	
		}
		
		@Test
		@TestInfo(tcName="Create All day Poll",feature="Channel Poll", expectedResult="All day Poll is created")
	     public void tc_0521_CreatePollForAllDay(){
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
			SitePageModel.waitFor(10);
		
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is the name of our national bird?");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("//label[text()='One day poll']"));//("Click on all day event"));
			SitePageModel.waitFor(5);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.id("addPollOption"));//("Click on add form");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Doel");
			SitePageModel.waitFor(4);

			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Peacock");
			SitePageModel.waitFor(7);
		
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Crow");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
			SitePageModel.waitFor(15);
			
			
			 String strExpect = "What is the name of our national bird?";
			 SitePageModel.waitFor(2);
			 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is the name of our national bird?']"));
			 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			 Assert.assertTrue(bFound);

		}

		@Test
		@TestInfo(tcName="Create Multiple day Poll",feature="Channel Poll", expectedResult="Multiple day Poll is created")
	   public void tc_0522_CreatePollForMultipleDay(){
			SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
			SitePageModel.waitFor(10);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your favourite color?");
			SitePageModel.waitFor(1);
			
			String startDate= SitePageModel.AddDateWithToday(3);
			SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
			String endDate= SitePageModel.AddDateWithToday(5);
			SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
			SitePageModel.waitFor(5);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"This is multiple day poll");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.id("addPollOption"));//("Click on add form");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Red");
			SitePageModel.waitFor(1);

			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Green");
			SitePageModel.waitFor(1);
		
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Blue");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
			SitePageModel.waitFor(17);
				
			String strExpect = "What is your favourite color?";
			 SitePageModel.waitFor(2);
			 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your favourite color?']"));
			 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			 Assert.assertTrue(bFound);

		}

		@Test
		@TestInfo(tcName="Edit a Poll",feature="Channel Poll", expectedResult="Poll info is updated")
	      public void tc_0523_EditPoll(){
			
			driver.navigate().refresh();
			SitePageModel.waitFor(20);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
			SitePageModel.waitFor(5);

			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your unfavourite color?");
			SitePageModel.waitFor(2);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited This is general poll");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.id("updatePollButton"));//("Click on update button");
			SitePageModel.waitFor(30);
		
			String strExpect = "What is your unfavourite color?";
			SitePageModel.waitFor(2);
			 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your unfavourite color?']"));
			 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			 Assert.assertTrue(bFound); 
			 SitePageModel.waitFor(2);

			  
			  
		}


		@Test
		@TestInfo(tcName="Cast vote on All day Poll",feature="Channel Poll", expectedResult="Vote is cast in Poll")
	    public void tc_0524_CastVoteInPoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.selectRadioText(driver, By.xpath("//label[text()='Peacock']"));// click radio button
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary btn-mini btn-cons m-t-5' and text() = 'Vote']"));//("Click on vote");	
			SitePageModel.waitFor(7);
			

			
		}
		
		
		@Test
		@TestInfo(tcName="Like to a Poll",feature="Channel Poll", expectedResult="Like number is increased")
	    public void tc_0525_LikePoll(){
			
			
			driver.navigate().refresh();
		    SitePageModel.waitFor(20);
		    
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "1";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);

		}
		
		
		@Test
		@TestInfo(tcName="Unlike to a Poll",feature="Channel Poll", expectedResult="Like number is decreased")
	     public void tc_0526_UnLikePoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
			SitePageModel.waitFor(7);
			
			String strExpect = "Like";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);

		}
				

		@Test
		@TestInfo(tcName="Add comment on a poll",feature="Channel Poll", expectedResult="Comment is added")
	    public void tc_0527_AddCommentToPoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			String strExpect = "First comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		    
		    String strExpect1 = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[3]/div/div/div[2]/p/p"));
			boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		    Assert.assertTrue(bFound1);

		}
		
		
		@Test
		@TestInfo(tcName="Edit comment on a poll",feature="Channel Poll", expectedResult="Comment is updated")
	    public void tc_0528_EditCommentToPoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.linkText("Edit"));
			SitePageModel.waitFor(5);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
			SitePageModel.waitFor(7);
			

		}
		
		
		@Test
		@TestInfo(tcName="Delete comment from a Poll",feature="Channel Poll", expectedResult="Comment is deleted")
	     public void tc_0529_DeleteCommentToPoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(7);
			
			String strExpect = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);

		}
		

		@Test
		@TestInfo(tcName="Delete a poll",feature="Channel Poll", expectedResult="Poll is deleted")
	      public void tc_0530_DeletePoll(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);			
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
			SitePageModel.waitFor(2);			
		}		
		

		@Test
		@TestInfo(tcName="Create poll with rating scale",feature="Everyone Channel Poll", expectedResult="Poll is created with rating")
	    public void tc_0541_CreatePollInRatingScale(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);	
			
			SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
			SitePageModel.waitFor(7);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your name?");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath("//a[@class='semi-bold poll-more-details']"));//("Click on more details");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");
			SitePageModel.waitFor(1);
					
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[2]/input"),"A");
			SitePageModel.waitFor(10);

			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[3]/input"),"B");
			SitePageModel.waitFor(1);
		
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[4]/input"),"C");
			SitePageModel.waitFor(1);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[5]/input"),"D");
			SitePageModel.waitFor(1);
		
			SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[6]/input"),"E");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
			SitePageModel.waitFor(10);
			
			
			String strExpect = "What is your name?";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your name?']"));
		    boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
			Assert.assertTrue(bFound); 
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);			
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
			SitePageModel.waitFor(2);	
		}
		
		
		
		@Test
		@TestInfo(tcName="Create poll with invalid data",feature="Everyone Channel Poll", expectedResult="Error message is displayed")
	    public void tc_0542_CreatePollInRatingScaleWithBlankField(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);			
			
			SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
			SitePageModel.waitFor(7);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check question required field
			SitePageModel.waitFor(2);
		
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[1]"));//Check answer required field
			SitePageModel.waitFor(2);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[2]"));//Check answer required field
			SitePageModel.waitFor(2);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[3]"));//Check answer required field
			SitePageModel.waitFor(2);
			
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[4]"));//Check question required field
			SitePageModel.waitFor(2);
		
			SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[5]"));//Check answer required field
			SitePageModel.waitFor(2);
			
		}

		
		@Test
		@TestInfo(tcName="Open File tab view on everyone channel",feature="Channel File", expectedResult="File window is opened")
	    public void tc_0531_OpenFileWindow(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
			SitePageModel.waitFor(3);
			
			String strExpect = "Description";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/label"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);			
		}
		
		
		@Test
		@TestInfo(tcName="Close File tab view on everyone channel",feature="Channel File", expectedResult="File window is closed")
	    public void tc_0532_CloseFileWindow(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
			SitePageModel.waitFor(2);
		}
		
//		@Test
//		@TestInfo(tcName="Upload File by click on link ",feature="Channel File", expectedResult="File is uploaded")
//	    public void tc_0533_UploadFileByfileSelection() throws AWTException{
//			
//			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
//			SitePageModel.waitFor(10);
//			
//			
//			SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
//			SitePageModel.waitFor(5);
//		
//			SitePageModel.enterText(driver, By.tagName("textarea"),"This is important file using UploadFileByfileSelection");// Type description
//			SitePageModel.waitFor(1);
//			
//			SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[3]/div[1]/div/i"));//("Click on file");
//			SitePageModel.waitFor(3);
//			
//			String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//			SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//			SitePageModel.waitFor(1);
//			SitePageModel.clearTextByRobotClass();
//			SitePageModel.waitFor(1);
//
//			String strUploadImage = AppConstant.IMAGE_PATH + "TestFile2.xlsx";
//			String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//			
//			SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//			SitePageModel.waitFor(1);
//			SitePageModel.clickButtonByRobotClass();
//			SitePageModel.waitFor(10);
//		
//			SitePageModel.tryClick(driver, By.id("createFileButton"));//("Click on create button");
//			SitePageModel.waitFor(15);
//			
//			String strExpect = "This is important file using UploadFileByfileSelection";
//			SitePageModel.waitFor(2);
//			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//		    Assert.assertTrue(bFound);
//		}
		
		
		

//		@Test
//		@TestInfo(tcName="Edit uploaded file",feature="Channel File", expectedResult="File is upadated and uploaded")
//	     public void tc_0534_EditUploadFile() throws AWTException{
//			
//			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
//			SitePageModel.waitFor(10);
//			
//			
//			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
//			SitePageModel.waitFor(2);
//			
//			SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
//			SitePageModel.waitFor(2);
//
//			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited important file");// edit description 
//			SitePageModel.waitFor(1);
//			
//			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[3]/div[1]/div/span"));//("Click on file");
//			SitePageModel.waitFor(7);
//			
//			String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//			SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//			SitePageModel.waitFor(1);
//			SitePageModel.clearTextByRobotClass();
//			SitePageModel.waitFor(1);
//
//			String strUploadImage = AppConstant.IMAGE_PATH + "TestFile3.xlsx";
//			String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//			
//			SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//			SitePageModel.waitFor(1);
//			SitePageModel.clickButtonByRobotClass();
//			SitePageModel.waitFor(10);
//		
//			SitePageModel.tryClick(driver, By.id("updateFileButton"));//("Click on update button");
//			SitePageModel.waitFor(15);
//		
//			String strExpect = "Edited important file";
//			SitePageModel.waitFor(2);
//			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//		    Assert.assertTrue(bFound);
//		}
		
		
		
		
		@Test
		@TestInfo(tcName="Like uploaded file",feature="Channel File", expectedResult="Like number is increased")
	    public void tc_0535_LikeUploadFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a/i"));//("Click on like");
			SitePageModel.waitFor(5);
			
			String strExpect = " 1 likes this.";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		@Test
		@TestInfo(tcName="Unlike uploaded file",feature="Channel File", expectedResult="Like number is decreased")
	      public void tc_0536_UnLikeUploadFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a[1]/i"));//("Click on like");
			SitePageModel.waitFor(5);
			
			String strExpect = " 1 likes this.";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertFalse(bFound);
		}
		
		
		@Test
		@TestInfo(tcName="Add comment on uploaded file",feature="Channel File", expectedResult="Comment is added")
	     public void tc_0537_AddCommentToFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			
			SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
			SitePageModel.waitFor(7);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
			SitePageModel.waitFor(6);
				
			String strExpect = "First comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		    
		    String strExpect1 = "Second comment";
			SitePageModel.waitFor(5);
			List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
			boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
		    Assert.assertTrue(bFound1);
		}
		
		
		
		
		@Test
		@TestInfo(tcName="Edit comment on uploaded file",feature="Channel File", expectedResult="Comment is updated")
	     public void tc_0538_EditCommentToFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(3);
			
			SitePageModel.tryClick(driver, By.linkText("Edit"));
			SitePageModel.waitFor(2);
			
			SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
			SitePageModel.waitFor(7);
				
			String strExpect = "Edited first comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);
		}
		
		
		
		
		
		@Test
		@TestInfo(tcName="Delete comment from a uploaded file",feature="Channel File", expectedResult="Comment is deleted")
	    public void tc_0539_DeleteCommentToFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
			SitePageModel.waitFor(3);
		
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok");
			SitePageModel.waitFor(2);
			
			String strExpect = "Second comment";
			SitePageModel.waitFor(2);
			List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
			boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		    Assert.assertTrue(bFound);			
		}
		
		
		@Test
		@TestInfo(tcName="Delete file from a uploaded file",feature="Channel File", expectedResult="File is deleted")
	    public void tc_0540_DeleteUploadFile(){
			
			SitePageModel.tryClick(driver, By.partialLinkText("Everyone"));//click on 'Everyone Channel link
			SitePageModel.waitFor(10);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(3);
			
			SitePageModel.tryClick(driver, By.linkText("Delete"));
			SitePageModel.waitFor(2);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
			SitePageModel.waitFor(2);			
		}	
		
		
		
}