package com.automation.suite;

//import java.awt.AWTException;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;











import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
//import com.automation.util.AppConstant;
//import com.google.common.collect.Multiset.Entry;
public class TestSuite_MyProfile extends TestHelper{

	@Test
	@TestInfo(tcName="Open My profile",feature="My profile", expectedResult="My Profile page is displayed")

	public void tc_1001_OpenMYProfile(){
		
		SitePageModel.tryClick(driver, By.id("user-options"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText("My Profile"));
		
		SitePageModel.waitFor(5);
		SitePageModel.elementIsVisible(driver, By.id("btn-main-edit"));
		
		
		
			}
	
	@Test
	@TestInfo(tcName="Open Main profile page",feature="My profile", expectedResult="Main profile page is displayed in Edit mode")

	public void tc_1002_OpenMainProfileOnEditMode(){
		
		//SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("btn-main-edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.elementIsVisible(driver, By.id("btn-main-cancel"));
		
		
		
		
	}
	
	@Test
	@TestInfo(tcName="Cancel Main profile page from Edit mode",feature="My profile", expectedResult="My profile page is displayed")

	public void tc_1003_CancelMainProfileFromEditMode(){
		
		//SitePageModel.waitFor(10);
		SitePageModel.tryClick(driver, By.id("btn-main-cancel"));
		SitePageModel.waitFor(5);
		//Asserting that Edit Button is visible after Canceling edit mode
		
		SitePageModel.elementIsVisible(driver, By.id("btn-main-edit"));
		
		
		
	}
	
	@Test
	@TestInfo(tcName="Edit Main profile information",feature="My profile", expectedResult="Main profile information is updated")

	public void tc_1004_EditAllMainProfileInfo(){
		
		//SitePageModel.waitFor(10);
		//SitePageModel.tryClick(driver, By.id("btn-main-edit"));
		SitePageModel.waitFor(3);
		
		String firstName="User";
		String lastName="Three";
		String jobTitle="SQA Engineer";
		//String department="QA";
		String birthDay="July 16, 1993";
		String gender="Male";
		String address="Bangladesh";
		String skypeAccount="Taskor13";
		String phone="1234567890";
		String mobile="01711111111";

		SitePageModel.tryClick(driver, By.id("btn-main-edit"));
		
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.id("first-name"), firstName);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("last-name"), lastName);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("job-title"), jobTitle);
		SitePageModel.waitFor(1);
		//SitePageModel.enterText(driver, By.id("department"), department);
		//SitePageModel.waitFor(4);
		
		SitePageModel.enterAutoCompleteDate(driver, By.id("birthday"), birthDay);
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.id("dropdownMenu4"));
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.linkText(gender));
		SitePageModel.waitFor(2);
		
		if(driver.findElement(By.className("select2-chosen")).getText().equals("Select location")){
			
			SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
			SitePageModel.waitFor(6);
			List<WebElement> listInput=driver.findElements(By.tagName("input"));
			SitePageModel.waitFor(7);
		 
			listInput.get(0).sendKeys(address);
			SitePageModel.waitFor(3);
			listInput.get(0).sendKeys(Keys.TAB);
			SitePageModel.waitFor(2);
					 
			//SitePageModel.enterAutoCompleteText(driver, By.className("select2-input"), address);
			//SitePageModel.enterAutoCompleteText(driver, By.id("address"), "Dhaka");
			//SitePageModel.enterAutoCompleteText(driver, By.xpath("//div[contains(@class, 'select2-search')]/label/following-sibling::input"), address);
			//SitePageModel.waitFor(10);
		}
		else{
			SitePageModel.waitFor(2);
			SitePageModel.tryClick(driver, By.className("select2-search-choice-close"));
			SitePageModel.waitFor(6);			
			SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
			SitePageModel.waitFor(6);
			List<WebElement> listInput=driver.findElements(By.tagName("input"));
			SitePageModel.waitFor(7);
		 
			listInput.get(0).sendKeys(address);
			SitePageModel.waitFor(3);
			SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
			listInput.get(0).sendKeys(Keys.TAB);
			SitePageModel.waitFor(2);
			
		}
		 SitePageModel.tryClick(driver, By.id("skype-account"));
			SitePageModel.waitFor(1);

		SitePageModel.enterText(driver, By.id("skype-account"), skypeAccount);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("work-phone"), phone);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("mobile-phone"), mobile);
		SitePageModel.waitFor(1);
							
			
		SitePageModel.tryClick(driver, By.id("btn-main-save"));
		SitePageModel.waitFor(8);
		
		//
		
		String strExpectedName=firstName+" "+lastName;
		List<WebElement> strActualName = driver.findElements(By.xpath("//div[contains(@class, 'p-l-20 col-xs-6')]/h3"));
		SitePageModel.waitFor(2);
		//System.out.println(strActualName.get(0).getText());		
		boolean bFoundName = strActualName.get(0).getText().equals(strExpectedName);
		Assert.assertTrue(bFoundName);
		
		String strExpectedJobTitle=jobTitle;
		List<WebElement> strActualJobTitle = driver.findElements(By.xpath("//div[contains(@class, 'm-t-5') and text()='"+jobTitle+"']"));
		SitePageModel.waitFor(2);
		boolean bFoundJobTitle = strActualJobTitle.get(0).getText().equals(strExpectedJobTitle);
		Assert.assertTrue(bFoundJobTitle);
		
		
		String strExpectedBirthDate=birthDay;
		List<WebElement> strActualBirthDate = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and text()='"+birthDay+"']"));
		SitePageModel.waitFor(2);
		boolean bFoundBirthDate = strActualBirthDate.get(0).getText().contains(strExpectedBirthDate);
		Assert.assertTrue(bFoundBirthDate);
		SitePageModel.waitFor(2);
		
		String strExpectedGender=gender;
		List<WebElement> strActualGender = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and text()='"+gender+"']"));
		SitePageModel.waitFor(2);
		boolean bFoundGender = strActualGender.get(0).getText().equals(strExpectedGender);
		Assert.assertTrue(bFoundGender);
		SitePageModel.waitFor(2);
		
		String strExpectedPhone=phone;
		List<WebElement> strActualPhone = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and text()='"+phone+"']"));
		SitePageModel.waitFor(2);
		boolean bFoundPhone = strActualPhone.get(0).getText().equals(strExpectedPhone);
		Assert.assertTrue(bFoundPhone);
		SitePageModel.waitFor(2);
		
		String strExpectedMobile=mobile;
		List<WebElement> strActualMobile = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and text()='"+mobile+"']"));
		SitePageModel.waitFor(2);
		boolean bFoundMobile = strActualMobile.get(0).getText().equals(strExpectedMobile);
		Assert.assertTrue(bFoundMobile);
		SitePageModel.waitFor(2);
		
		
		String strExpectedAddress=address;
		//
		//List<WebElement> strActualMobile = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/div[3]/div[5]"));
		
		//List<WebElement> strActualAddress = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and text()='"+address+"']"));
		List<WebElement> strActualAddress = driver.findElements(By.xpath("//div[contains(@class, 'm-b-5') and contains(.,'"+address+"')]"));
		
		
		SitePageModel.waitFor(2);
		boolean bFoundAddress = strActualAddress.get(0).getText().contains(strExpectedAddress);
		Assert.assertTrue(bFoundAddress);
		SitePageModel.waitFor(2);
		 
		
	
	
	}
	
	
	@Test
	@TestInfo(tcName="Invalid submission of Main profile",feature="My profile", expectedResult="Error message is displayed")

	public void tc_1005_EditMainProfileEssentialFiledWithBlankData(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(15);
		SitePageModel.tryClick(driver, By.id("btn-main-edit"));
		SitePageModel.waitFor(5);
		
		String firstName="";
		String lastName="";
					
				
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.id("first-name"), firstName);
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("last-name"), lastName);
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("btn-main-save"));
		SitePageModel.waitFor(3);
		
		String strExpectedFirstNameErrorText="Required field.";		
		
		List<WebElement> strActualFirstNameErrorElement = driver.findElements(By.xpath(".//*[@id='first-name']/following-sibling::span"));
		SitePageModel.waitFor(2);			
		boolean bFoundFirstNameError = strActualFirstNameErrorElement.get(0).getText().equals(strExpectedFirstNameErrorText);
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundFirstNameError);
		
		String strExpectedLastNameErrorText="Required field.";
		
		List<WebElement> strActualLastNameErrorElements = driver.findElements(By.xpath(".//*[@id='last-name']/following-sibling::span"));
		SitePageModel.waitFor(2);
		
		SitePageModel.waitFor(2);
		boolean bFoundLastNameError = strActualLastNameErrorElements.get(0).getText().equals(strExpectedLastNameErrorText);
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundLastNameError);
		SitePageModel.waitFor(2);
		
	
	}
	
	
	@Test
	@TestInfo(tcName="Edit Additional information",feature="My profile", expectedResult="Additional profile information is updated")

	public void tc_1006_EditAdditionalProfileInfo(){
		
		driver.navigate().refresh();
		//SitePageModel.tryClick(driver, By.id("user-options"));
		//SitePageModel.waitFor(5);
		//SitePageModel.tryClick(driver, By.linkText("My Profile"));
		
		SitePageModel.waitFor(13);
		
		SitePageModel.tryClick(driver, By.id("btn-additional-edit"));			
		SitePageModel.waitFor(5);
		
		String education="Academy";
		String university="NAcademy";
		String pSkills="SQA";
		String hobbies="Fishing";		
		String spokenLanguage="Bengali";
		String interest="philosophy";
		String fraternities="Alpha Delta";
		String graduateSchool="Test Gradualte School";
		String otherAssociations="ABC Association";
		String bio="I am a social worker";
		
		
		
	
// Close entry if it is found 


	
		int closeBtnNumber=driver.findElements(By.className("select2-search-choice-close")).size();

		//System.out.println(closeBtnNumber);
		
		if(closeBtnNumber>1){
			SitePageModel.waitFor(5);

			for (int i=1;i<closeBtnNumber; i++){
	
				for (int j=0; j<4;j++){
		
					try{
			//System.out.println(eventLists.get(i).getText());
				driver.findElement(By.className("select2-search-choice-close")).click();
				SitePageModel.waitFor(5);
				break;
					}
					catch (StaleElementReferenceException e){
			       e.toString();
			     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
			      
					}
	//size() != 0;
				}
				SitePageModel.waitFor(2);
			}
			SitePageModel.scrollTopOfPage(driver);
			
			SitePageModel.waitFor(4);
		}	
		
		
		SitePageModel.waitFor(7);	
		// Select Options from Education
				
		
		SitePageModel.tryClick(driver, By.className("select2-choices"));
		//SitePageModel.tryClick(driver, By.xpath("//div[@id='s2id_education']/ul"));
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//div[contains(@class, 'select2-result-label') and text()='"+education+"']"));
		
		
		SitePageModel.waitFor(5);	
		
			
		// Find rest of All input box location
			
		SitePageModel.waitFor(1);	
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(5);
	
		
		//Set Name of University or Institution
		allElement.get(4).click();		
		SitePageModel.waitFor(3);
		allElement.get(4).sendKeys(university);
		SitePageModel.waitFor(2);
		allElement.get(4).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		//Set Professional Skills
		allElement.get(6).click();		
		SitePageModel.waitFor(5);
		allElement.get(6).sendKeys(pSkills);
		SitePageModel.waitFor(5);
		allElement.get(6).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(5);
		
		
		//set Hobbies
		allElement.get(8).click();
		SitePageModel.waitFor(2);
		allElement.get(8).sendKeys(hobbies);
		SitePageModel.waitFor(5);
		allElement.get(8).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		//Set Spoken Languages
		allElement.get(10).click();		
		SitePageModel.waitFor(5);
		allElement.get(10).sendKeys(spokenLanguage);
		SitePageModel.waitFor(2);
		allElement.get(10).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		//Interest
		allElement.get(12).click();		
		SitePageModel.waitFor(5);
		allElement.get(12).sendKeys(interest);
		SitePageModel.waitFor(2);
		allElement.get(12).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		
		//SitePageModel.scrollBottomOfPage(driver);
		//SitePageModel.waitFor(8);
		
		//set fraternities
		
		allElement.get(14).click();		
		SitePageModel.waitFor(5);
		allElement.get(14).sendKeys(fraternities);
		SitePageModel.waitFor(2);
		allElement.get(14).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		//set graduateSchool	
		SitePageModel.enterText(driver, By.id("graduate-school"), graduateSchool);			
		SitePageModel.waitFor(1);
		
		//set otherAssociations			
				
		allElement.get(17).click();		
		SitePageModel.waitFor(5);
		allElement.get(17).sendKeys(otherAssociations);
		SitePageModel.waitFor(2);
		allElement.get(17).sendKeys(Keys.TAB);		
		SitePageModel.waitFor(2);
		
		//set bio
		SitePageModel.enterText(driver, By.id("personal-bio"), bio);				
		SitePageModel.waitFor(1);
		
		

		//Submit form 
		
		SitePageModel.tryClick(driver, By.id("btn-additional-save"));
		SitePageModel.waitFor(30);
		
	
		//Asserting info
		
		String strExpectedEducation=education;		
		List<WebElement> strActualEducationElement = driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[1]"));
		SitePageModel.waitFor(2);			
		boolean bFoundEducation = strActualEducationElement.get(0).getText().equals(strExpectedEducation);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundEducation);
		
		String strExpecteduniversity=university;		
		List<WebElement> listActualUniversityElements = driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[2]"));
		SitePageModel.waitFor(2);			
		boolean bFoundUniversity = listActualUniversityElements.get(0).getText().equals(strExpecteduniversity);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundUniversity);
		
		String strExpectedSkills=pSkills;		
		List<WebElement> listActualSkillsElements= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[3]"));
		SitePageModel.waitFor(2);			
		boolean bFoundSkills = listActualSkillsElements.get(0).getText().equals(strExpectedSkills);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundSkills);
		
		String strExpectedHobbies=hobbies;
		List<WebElement> listActualHobbiesElements= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[4]"));
		SitePageModel.waitFor(2);			
		boolean bFoundHobbies = listActualHobbiesElements.get(0).getText().equals(strExpectedHobbies);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundHobbies);
		
		String strExpectedLanguage=spokenLanguage;
		List<WebElement> listActualLanguageElements= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[5]"));
		SitePageModel.waitFor(2);			
		boolean bFoundLanguage= listActualLanguageElements.get(0).getText().equals(strExpectedLanguage);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundLanguage);
		
		String strExpectedInterest=interest;
		List<WebElement> listActualInterest= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[6]"));
		SitePageModel.waitFor(2);			
		boolean bFoundInterests= listActualInterest.get(0).getText().equals(strExpectedInterest);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundInterests);
		
		String strExpectedgraduateSchool=graduateSchool;
		List<WebElement> listActualgraduateSchool= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[7]"));
		SitePageModel.waitFor(2);			
		boolean bFoundSchool= listActualgraduateSchool.get(0).getText().equals(strExpectedgraduateSchool);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundSchool);
		
		String strExpectedFraternities=fraternities;
		List<WebElement> listActualfraternities= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[8]"));
		SitePageModel.waitFor(2);			
		boolean bFoundFraternities= listActualfraternities.get(0).getText().equals(strExpectedFraternities);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundFraternities);
		
		String strExpectedotherAssociations=otherAssociations;
		List<WebElement> listActualAssociationsElements= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[9]"));
		SitePageModel.waitFor(2);			
		boolean bFoundAssociation= listActualAssociationsElements.get(0).getText().equals(strExpectedotherAssociations);
		SitePageModel.waitFor(1);
		Assert.assertTrue(bFoundAssociation);
		
		//String interest="philosophy";
		//String fraternities="Alpha Delta";
		//String graduateSchool="Test Gradualte School";
		//String otherAssociations="ABC Association";
		//String bio="I am a social worker";
		
	}
	
	@Test
	@TestInfo(tcName="Clear all additional information",feature="My profile", expectedResult="All additional information is cleared")

	public void tc_1007_EditAdditionalProfileClearAllSingleEntry(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(17);
		
		for (int j=0; j<4;j++){
			
			try{
				//System.out.println(eventLists.get(i).getText());
				SitePageModel.tryClick(driver, By.id("btn-additional-edit"));	
				//SitePageModel.waitFor(5);
				break;
				}
				catch (StaleElementReferenceException e){
				       e.toString();
				     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
				      
				 }
		//size() != 0;
		}
				
		SitePageModel.waitFor(5);
			
			
		//Find all close button index
		int closeBtnNumber=driver.findElements(By.className("select2-search-choice-close")).size();
		
		//Delete all info
		
		SitePageModel.waitFor(5);
		
		if(closeBtnNumber>1){
		for (int i=1;i<closeBtnNumber; i++){
			
			for (int j=0; j<4;j++){
				
				try{
					//System.out.println(eventLists.get(i).getText());
					driver.findElement(By.className("select2-search-choice-close")).click();
					SitePageModel.waitFor(5);
					break;
					}
					catch (StaleElementReferenceException e){
					       e.toString();
					     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
					      
					 }
			//size() != 0;
			}
			SitePageModel.waitFor(5);
		}
		
		}	
		
		SitePageModel.enterText(driver, By.id("graduate-school"), "");			
		SitePageModel.waitFor(1);
		
		//set bio
		SitePageModel.enterText(driver, By.id("personal-bio"), "");				
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("btn-additional-save"));
		SitePageModel.waitFor(6);
		
		// Asserting that any element found then test cases will be failed
		
		boolean bElem=SitePageModel.doesElementExist(driver, By.xpath(".//*[@id='aditional']/div/div[2]/div/div[1]"));
		SitePageModel.waitFor(2);                                
		Assert.assertFalse(bElem);
		//SitePageModel.waitFor(10);
		
	}
	
	
	
	
	
	
	
	
	@Test
	@TestInfo(tcName="Edit Additional profile information with multiple data",feature="My profile", expectedResult="Additional profile information is updated with multiple data")

	public void tc_1008_EditAdditionalProfileAddMultipleData(){
		
	
		driver.navigate().refresh();
		SitePageModel.waitFor(20);
		
		SitePageModel.tryClick(driver, By.id("btn-additional-edit"));			
		SitePageModel.waitFor(5);
		
		String education1="Academy";
		String education2="University";
		
		String university1="VAcademy";
		String university2="DUniversity";
		String university=university1+";"+university2+";";
		
		String pSkills1="Automation";
		String pSkills2="SQA";
		String pSkills=pSkills1+";"+pSkills2+";";
		
		String hobbies1="Gardening";
		String hobbies2="Fishing";
		String hobbies=hobbies1+";"+hobbies2+";";
		
		String spokenLanguage1="English";
		String spokenLanguage2="Bengali";
		String spokenLanguage=spokenLanguage1+";"+spokenLanguage2+";";
		
		String interest1="Philosophy";
		String interest2="Games";
		String interest=interest1+";"+interest2+";";
		
		String fraternities1="Delta";
		String fraternities2="Alpha";
		String fraternities=fraternities1+";"+fraternities2+";";
		
		String graduateSchool="Test Graduate School One";
		//String graduateSchool2="Test Graduate School Two";
		
		String otherAssociations1="ABC";
		String otherAssociations2="XYZ";
		String otherAssociations=otherAssociations1+";"+otherAssociations2+";";
		
		String bio="I am a social worker";
		
		
		// Close entry if it is found 


		
		int closeBtnNumber=driver.findElements(By.className("select2-search-choice-close")).size();

		//Delete all info
		if(closeBtnNumber>1){
		SitePageModel.waitFor(5);

			for (int i=1;i<closeBtnNumber; i++){
			
				for (int j=0; j<4;j++){
				
					try{
					//System.out.println(eventLists.get(i).getText());
						driver.findElement(By.className("select2-search-choice-close")).click();
						SitePageModel.waitFor(5);
						break;
					}
					catch (StaleElementReferenceException e){
					       e.toString();
					     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
					      
					 }
			//size() != 0;
				}
			SitePageModel.waitFor(5);
			}
			SitePageModel.scrollTopOfPage(driver);
			
			SitePageModel.waitFor(3);
		}	
		
		SitePageModel.waitFor(5);	
		// Select Options from Education
		SitePageModel.tryClick(driver, By.className("select2-choices"));
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath("//div[contains(@class, 'select2-result-label') and text()='"+education1+"']"));
		
		SitePageModel.waitFor(3);
		SitePageModel.tryClick(driver, By.className("select2-choices"));
		SitePageModel.tryClick(driver, By.xpath("//div[contains(@class, 'select2-result-label') and text()='"+education2+"']"));
		SitePageModel.waitFor(3);
		
		
		
						
			
		// Find rest of All input box location
			
		SitePageModel.waitFor(1);	
		List<WebElement>allElement=driver.findElements(By.tagName("input"));
		SitePageModel.waitFor(3);
								
		//Set Name of University or Institution
		allElement.get(4).click();		
		SitePageModel.waitFor(5);
		allElement.get(4).sendKeys(university);
		SitePageModel.waitFor(2);
		
		//SitePageModel.tryClick(driver, By.id("btn-additional-save"));
		//SitePageModel.waitFor(7);
		
		//Set Professional Skills
		allElement.get(6).click();		
		SitePageModel.waitFor(5);
		allElement.get(6).sendKeys(pSkills);
		SitePageModel.waitFor(2);
				
		
		//set Hobbies
		allElement.get(8).click();
		SitePageModel.waitFor(5);
		allElement.get(8).sendKeys(hobbies);
		SitePageModel.waitFor(2);
		
		
		//Set Spoken Languages
		allElement.get(10).click();		
		SitePageModel.waitFor(5);
		allElement.get(10).sendKeys(spokenLanguage);
		SitePageModel.waitFor(2);
		
	//Interest
		
		allElement.get(12).click();		
		SitePageModel.waitFor(5);
		allElement.get(12).sendKeys(interest);
		SitePageModel.waitFor(2);
		
		
		
		//SitePageModel.scrollBottomOfPage(driver);
		//SitePageModel.waitFor(8);
		
		//set fraternities
		
		allElement.get(14).click();		
		SitePageModel.waitFor(5);
		allElement.get(14).sendKeys(fraternities);
		SitePageModel.waitFor(3);
		
		//set graduateSchool	
		SitePageModel.enterText(driver, By.id("graduate-school"), graduateSchool);			
		SitePageModel.waitFor(5);
		
		//set otherAssociations			
				
		allElement.get(17).click();		
		SitePageModel.waitFor(3);
		allElement.get(17).sendKeys(otherAssociations);
		SitePageModel.waitFor(3);
		
		
		//set bio
		SitePageModel.enterText(driver, By.id("personal-bio"), bio);				
		SitePageModel.waitFor(1);
			
		
		
		
	
		
		// Save additional profile info
		SitePageModel.tryClick(driver, By.id("btn-additional-save"));
		SitePageModel.waitFor(7);
		SitePageModel.scrollTopOfPage(driver);
		SitePageModel.waitFor(3);
		// Asserting that Saving data is displayed on profile
		String strExpectedEducation1=education1;		
		List<WebElement> strActualEducationElement1 = driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[1]"));
		SitePageModel.waitFor(2);		
		boolean bFoundEducation1 = strActualEducationElement1.get(0).getText().contains(strExpectedEducation1);
		//System.out.println(bFoundEducation1);
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundEducation1);
		
		
		String strExpectedEducation2=education2;		
		List<WebElement> strActualEducationElement2 = driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[2]"));
		SitePageModel.waitFor(2);		
		boolean bFoundEducation2 = strActualEducationElement2.get(0).getText().contains(strExpectedEducation2);
		//System.out.println(strActualEducationElement2.get(0).getText());
		SitePageModel.waitFor(2);
		Assert.assertTrue(bFoundEducation2);
		
		// 		
		String strExpecteduniversity=university1+","+university2;		
		List<WebElement> listActualUniversityElements = driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[3]"));
		SitePageModel.waitFor(2);		
		boolean bFoundUniversity = listActualUniversityElements.get(0).getText().equals(strExpecteduniversity);
		//System.out.println("Uni"+bFoundUniversity);
		SitePageModel.waitFor(3);
		Assert.assertTrue(bFoundUniversity);
				
		
		String strExpectedSkills1=pSkills1;		
		List<WebElement> listActualSkillsElements1= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[4]"));
		SitePageModel.waitFor(2);
		
		String strExpectedSkills2=pSkills2;
		List<WebElement> listActualSkillsElements2= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[5]"));
		SitePageModel.waitFor(2);
		
		boolean bFound1Skills1 = listActualSkillsElements1.get(0).getText().contains(strExpectedSkills1);
		boolean bFound2Skills1 = listActualSkillsElements1.get(0).getText().contains(strExpectedSkills2);
		//System.out.println("Skill1:"+bFoundSkills1);
		SitePageModel.waitFor(4);
		Assert.assertTrue(bFound1Skills1||bFound2Skills1);
		
				
		boolean bFound1Skills2 = listActualSkillsElements2.get(0).getText().contains(strExpectedSkills2);
		boolean bFound2Skills2 = listActualSkillsElements2.get(0).getText().contains(strExpectedSkills1);
		SitePageModel.waitFor(2);
		//System.out.println("Skills2:"+bFoundSkills2);
		Assert.assertTrue(bFound1Skills2||bFound2Skills2);
		
		String strExpectedHobbies1=hobbies1;
		List<WebElement> listActualHobbiesElements1= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[6]"));
		SitePageModel.waitFor(2);	
		String strExpectedHobbies2=hobbies2;
		List<WebElement> listActualHobbiesElements2= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[7]"));
		SitePageModel.waitFor(2);
		
		boolean bFound1Hobbies1 = listActualHobbiesElements1.get(0).getText().contains(strExpectedHobbies1);
		SitePageModel.waitFor(1);
		boolean bFound2Hobbies1 = listActualHobbiesElements1.get(0).getText().contains(strExpectedHobbies2);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFound1Hobbies1||bFound2Hobbies1);
		
					
		boolean bFound1Hobbies2 = listActualHobbiesElements2.get(0).getText().contains(strExpectedHobbies2);
		SitePageModel.waitFor(1);
		boolean bFound2Hobbies2 = listActualHobbiesElements2.get(0).getText().contains(strExpectedHobbies1);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies2"+bFoundHobbies2);
		Assert.assertTrue(bFound1Hobbies2||bFound2Hobbies2);
			
		
		String strExpectedLanguage1=spokenLanguage1;
		List<WebElement> listActualLanguageElements1= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[8]"));
		SitePageModel.waitFor(2);			
		String strExpectedLanguage2=spokenLanguage2;
		List<WebElement> listActualLanguageElements2= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[9]"));
		SitePageModel.waitFor(2);
		
		boolean bFound1Language1= listActualLanguageElements1.get(0).getText().contains(strExpectedLanguage1);
		SitePageModel.waitFor(1);
		boolean bFound12Language1= listActualLanguageElements1.get(0).getText().contains(strExpectedLanguage2);
		SitePageModel.waitFor(1);
		//System.out.println("Language1"+bFoundLanguage1);
		Assert.assertTrue(bFound1Language1||bFound12Language1);
		
					
		boolean bFound1Language2= listActualLanguageElements2.get(0).getText().contains(strExpectedLanguage2);
		SitePageModel.waitFor(1);
		boolean bFound2Language2= listActualLanguageElements2.get(0).getText().contains(strExpectedLanguage1);
		SitePageModel.waitFor(1);
		//System.out.println("Language1"+bFoundLanguage2);
		Assert.assertTrue(bFound1Language2||bFound2Language2);
		
		String strExpectedInterest1=interest1;
		List<WebElement> listActualInterest1= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[10]"));
		SitePageModel.waitFor(5);			
		String strExpectedInterest2=interest2;
		List<WebElement> listActualInterest2= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[11]"));
		SitePageModel.waitFor(5);	
		
		boolean bFound1Interest1 = listActualInterest1.get(0).getText().contains(strExpectedInterest1);
		SitePageModel.waitFor(1);
		boolean bFound2Interest1 = listActualInterest1.get(0).getText().contains(strExpectedInterest2);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFound1Interest1||bFound2Interest1);
		
						
		boolean bFound1Interest2 = listActualInterest2.get(0).getText().contains(strExpectedInterest2);
		SitePageModel.waitFor(1);
		boolean bFound2Interest2 = listActualInterest2.get(0).getText().contains(strExpectedInterest1);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFound1Interest2||bFound2Interest2);
	
		String strExpectedGraduateSchool=graduateSchool;
		List<WebElement> listActualGraduateSchool= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[12]"));
		SitePageModel.waitFor(5);			
		boolean bFoundGraduateSchool = listActualGraduateSchool.get(0).getText().contains(strExpectedGraduateSchool);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFoundGraduateSchool);
		
	
		
		String strExpectedFraternities=fraternities1+","+fraternities2;
		List<WebElement> listActualFraternities= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[13]"));
		SitePageModel.waitFor(5);			
		boolean bFoundFraternities = listActualFraternities.get(0).getText().contains(strExpectedFraternities);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFoundFraternities);
						
		String strExpectedOtherAssociations=otherAssociations1+","+otherAssociations2;
		List<WebElement> listActualOtherAssociations= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[14]"));
		SitePageModel.waitFor(5);			
		boolean bFoundOtherAssociations = listActualOtherAssociations.get(0).getText().contains(strExpectedOtherAssociations);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFoundOtherAssociations);
		
		/*
		
		String strExpectedBio=bio;
		List<WebElement> listActualBio= driver.findElements(By.xpath(".//*[@id='aditional']/div/div[2]/div/div[15]"));
		SitePageModel.waitFor(5);			
		boolean bFoundBio = listActualBio.get(0).getText().contains(strExpectedBio);
		SitePageModel.waitFor(1);
		//System.out.println("Hobbies1"+bFoundHobbies1);
		Assert.assertTrue(bFoundBio);
		
		*/
		
		
	}
	
	
	
	
	@Test
	@TestInfo(tcName="Clear multiple data of additional information",feature="My profile", expectedResult="All additional information is cleared")

	public void tc_1009_EditAdditionalProfileClearAllMultipleEntry(){
		
		
		driver.navigate().refresh();
		SitePageModel.waitFor(17);
		
		for (int j=0; j<4;j++){
			
			try{
				//System.out.println(eventLists.get(i).getText());
				SitePageModel.tryClick(driver, By.id("btn-additional-edit"));	
				//SitePageModel.waitFor(5);
				break;
				}
				catch (StaleElementReferenceException e){
				       e.toString();
				     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
				      
				 }
		//size() != 0;
		}
				
		SitePageModel.waitFor(5);
			
			
		//Find all close button index
		int closeBtnNumber=driver.findElements(By.className("select2-search-choice-close")).size();
		
		//Delete all info
		
		SitePageModel.waitFor(5);
		
		if(closeBtnNumber>1){
		for (int i=1;i<closeBtnNumber; i++){
			
			for (int j=0; j<4;j++){
				
				try{
					//System.out.println(eventLists.get(i).getText());
					driver.findElement(By.className("select2-search-choice-close")).click();
					SitePageModel.waitFor(5);
					break;
					}
					catch (StaleElementReferenceException e){
					       e.toString();
					     //  System.out.println("Trying to recover from a stale element :" + e.getMessage());
					      
					 }
			//size() != 0;
			}
			SitePageModel.waitFor(5);
		}
		
		}	
		
		SitePageModel.enterText(driver, By.id("graduate-school"), "");			
		SitePageModel.waitFor(3);
		
		//set bio
		SitePageModel.enterText(driver, By.id("personal-bio"), "");				
		SitePageModel.waitFor(3);
		
		SitePageModel.tryClick(driver, By.id("btn-additional-save"));
		SitePageModel.waitFor(7);
		
		// Asserting that any element found then test cases will be failed
		
		boolean bElem=SitePageModel.doesElementExist(driver, By.xpath(".//*[@id='aditional']/div/div[2]/div/div[1]"));
		SitePageModel.waitFor(3);
		Assert.assertFalse(bElem);
		//SitePageModel.waitFor(10);
		
		
	}
}
