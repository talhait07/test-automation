package com.automation.suite;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;

public class TestSuite_Chat extends TestHelper {
	
	@Test
	@TestInfo(tcName="Create Chat without Name",feature="Chat", expectedResult="Chat is created")
    public void tc_1401_CreateNewPublicChat(){

		driver.navigate().to("https://secure.connectik.com");		
        SitePageModel.waitFor(20);
        
        SitePageModel.tryClick(driver, By.id("lsb-chat-create"));//("Click on chat icon");
        SitePageModel.waitFor(7);

        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
        SitePageModel.waitFor(2);

        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Test1");
//        SitePageModel.enterAutoCompleteText(driver,By.xpath("html/body/div[5]/div/input"), "Nine");//'invite member'
        SitePageModel.waitFor(7);
        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created channel");
        SitePageModel.waitFor(5);

        String strExpect = "Test1 COC";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li/a/span[1]"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);
        
        SitePageModel.tryClick(driver,By.xpath(".//*[@id='lsb-chats-list']/li/a/span[contains(.,'Test1')]"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(2);
    }

    @Test
	@TestInfo(tcName="Create Chat with Name",feature="Chat", expectedResult="Chat with name is created")
    public void tc_1402_CreateChatWithName(){

        SitePageModel.waitFor(5);
        SitePageModel.tryClick(driver, By.id("lsb-chat-create"));//("Click on chat icon");
        SitePageModel.waitFor(2);

        SitePageModel.enterText(driver, By.name("title"), "Testing purpose");//Enter Title Text
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
        SitePageModel.waitFor(2);        
        
        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Test1");
        SitePageModel.waitFor(3);
        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(3);  
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created channel");
        SitePageModel.waitFor(10);

        String strExpect = "Test1 COC";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li/a/span[contains(.,'Test1')]"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);
    }


    @Test
	@TestInfo(tcName="Open a chat",feature="Chat", expectedResult="Chat window is opened")
    public void tc_1403_OpenExistingWindow(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on chat icon");
        SitePageModel.waitFor(7);
    }


    @Test
	@TestInfo(tcName="Close chat window",feature="Chat", expectedResult="Chat window is closed")
    public void tc_1404_CloseExistingWindow() {

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on chat icon");
        SitePageModel.waitFor(2);

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Close']"));//("Click on any existing chat");
        SitePageModel.waitFor(2);
    }

    @Test
	@TestInfo(tcName="Send text by chat",feature="Chat", expectedResult="Text is submitted")
    public void tc_1405_WriteInChat() throws AWTException{

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);

        SitePageModel.enterText(driver, By.xpath(".//*[@id='chat-textarea']")," Testing purpose.");//("write on chat");
        SitePageModel.waitFor(3);

        SitePageModel.clickButtonByRobotClass();
    }

    @Test
    public void tc_1406_ReadLastMessageFromChat(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(15);

        String strExpect = "Testing purpose.";
        List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[1]/div[1]/div[2]/div/div[3]/div[1]/div/span"));
        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
        Assert.assertTrue(bFound);

        SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[1]/div[1]/div[1]/a[2]/i"));//("Click on close option");
        SitePageModel.waitFor(7);
    }

    @Test
	@TestInfo(tcName="Finish Chat",feature="Chat", expectedResult="Chat window is closed")
    public void tc_1407_FinishChatAndApprove(){

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(3);  

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");        
        SitePageModel.waitFor(2);        

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(3);


    }
    
    @Test
	@TestInfo(tcName="Cancel the Finish Chat confirmation",feature="Chat", expectedResult="Chat window remains opened")
    public void tc_1408_FinishChatCancelAndApprove(){

		SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
         
        SitePageModel.waitFor(2);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-default']"));//("Click on cancel button of POPUP");
		SitePageModel.waitFor(4);

    }


    @Test
	@TestInfo(tcName="Leave chat by invitee",feature="Chat", expectedResult="Invitee is able to leave chat")
    public void tc_1409_LeaveChatAndApprove(){
    	
        SitePageModel.tryClick(driver, By.id("lsb-chat-create"));//("Click on chat icon");
        SitePageModel.waitFor(4);

        SitePageModel.enterText(driver, By.name("title"), "Testing purpose");//Enter Title Text
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
        SitePageModel.waitFor(2);        
        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Test1");
        SitePageModel.waitFor(3);
        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(5);  
        
        SitePageModel.tryClick(driver,By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Invite Member...']"));//'invite member'
        SitePageModel.waitFor(2);        
        SitePageModel.enterText(driver, By.xpath("//div[@id='select2-drop']/div/input"), "Test5");
        SitePageModel.waitFor(3);
        SitePageModel.tryClick(driver,By.xpath("//div[@role='option']"));
        SitePageModel.waitFor(5);  
        
        SitePageModel.tryClick(driver, By.id("createChatButton"));//("Click on created channel");
        SitePageModel.waitFor(12);

//        String strExpect = "Tania Najnin Toma, Afsana Akter";
//        List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));
////        System.out.println(aboutTextElm.get(0).getText());
//        boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//        Assert.assertTrue(bFound);
//        SitePageModel.waitFor(10);

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='user-options']/i"));//("Click on setting");
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
        SitePageModel.waitFor(12);

        SitePageModel.enterText(driver, By.id("username"),"test5.coc@rootnext.com");
        SitePageModel.waitFor(1);
        SitePageModel.enterText(driver, By.id("password"),"T@skor13");
        SitePageModel.waitFor(1);
        
  

    	SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
    	SitePageModel.waitFor(5);
    	
    	SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
    	SitePageModel.waitFor(5); 
    	SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
    	SitePageModel.waitFor(35); 
        
        
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on invited chat");
        SitePageModel.waitFor(10);

        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(2);        
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-sign-out']"));//("Click on LEAVE"));
        SitePageModel.waitFor(2);
                
		SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
		SitePageModel.waitFor(2);
		SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
		SitePageModel.waitFor(12);
        
        SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com");
		SitePageModel.waitFor(1);
        SitePageModel.enterText(driver, By.id("password"),"T@skor13");
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
        SitePageModel.waitFor(30);
    }

    @Test
	@TestInfo(tcName="Change Chat Topic",feature="Chat", expectedResult="Creator is able to change Chat topic")
    public void tc_1410_ChatChangeTopic(){

    	SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);
        
        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(5);
        
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(3);       
       
        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-pencil']"));//("Click on Change Topic"));
        SitePageModel.waitFor(3);
       
        SitePageModel.enterText(driver, By.xpath("//form[@class='bootbox-form']/input")," Updated Testing purpose.");
        SitePageModel.waitFor(2);
        
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(5);
		
		
        //Code for removing all created chat in this session
        
        SitePageModel.tryClick(driver, By.xpath("//a[@title='Settings']"));//("Click on setting link");
        SitePageModel.waitFor(1);        
        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-lock']"));//("Click on finish"));
        SitePageModel.waitFor(1);
        SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
		SitePageModel.waitFor(1);


    }

    @Test
	@TestInfo(tcName="Maximize and minimize chat window",feature="Chat", expectedResult="Chat window is maximized and minimized")
    public void tc_1411_ChatWindowMaximizeMinimize(){
    	
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-chats-list']/li[1]/a/span[1]"));//("Click on existing chat");
        SitePageModel.waitFor(7);

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-unsorted']"));//("Click on chat window"));
        SitePageModel.waitFor(3);

        SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-unsorted']"));//("Click on chat window"));
        SitePageModel.waitFor(3);
                

        
    } 
}