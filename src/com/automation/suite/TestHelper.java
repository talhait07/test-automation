package com.automation.suite;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.FluentWait;
//import org.openqa.selenium.support.ui.Wait;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.automation.util.PropertySettings;
import com.automation.util.SessionBean;

//import com.thoughtworks.selenium.Wait;

/*
 * this class will provide common functionality and properties for test suite
 */
public class TestHelper {

	public static WebDriver driver = null;
	protected PropertySettings settings = null;

	protected long rand = 0;

	protected String baseUrl = "";
	protected int waitTime = 2;
	protected JavascriptExecutor jse = null;

	@BeforeTest
	public void testBeforeTest(ITestContext pSettings) throws Exception {

		if (settings == null) {
			settings = (PropertySettings) pSettings.getAttribute("setting");
			this.baseUrl = settings.getBaseUrl();

			//System.out.println(this.baseUrl);
		}

		if (driver == null) {
			driver = settings.getDriver();
			jse = (JavascriptExecutor) driver;
			driver.get(this.baseUrl);
			//System.out.println(this.baseUrl);
			makeWait(this.waitTime);
			settings.setSession(new SessionBean());
		} else {
			makeWait(this.waitTime - 2);
		}
		
		//System.out.println("Line:68");

		if (jse == null && driver != null) {
			jse = (JavascriptExecutor) driver;
		}
		//System.out.println("Line:73");
		rand = new Date().getTime();

		setTargetServer();

		
		//System.out.println("Line:79");
		// System.out.println("BeforeTest");
	}

	@BeforeMethod
	public void setUp(ITestContext pSettings) {
		// System.out.println("BeforeMethod");
		// refreshBrowser();
	}

	@AfterMethod
	public void aferMethod(ITestContext pSettings) {
		// System.out.println("AfterMethod");
	}

	@AfterTest
	public void aferTest(ITestContext pSettings) {
		// System.out.println("AfterTest");

	}

	protected void quitDriver() {
		driver.manage().deleteAllCookies();
		driver.quit();
		driver = null;
		settings.setCurrentDriver(null);
	}
	
	protected void setTargetServer(){
		setToSession("targetServer", "one");
	}

	

	/*
	 * The method will help to scroll browser
	 */
	protected void browserScroll(int x, int y) {
		String jsCode = "window.scrollBy(" + x + "," + y + ")";
		System.out.println(jsCode + " ========");
		jse.executeScript(jsCode, "");
	}

	/*
	 * make some wait the execution
	 */
	public void makeWait(int waitForSecond) {
		try {
			Thread.sleep(1000 * waitForSecond);
		} catch (InterruptedException ie) {
			System.out.println(ie.getMessage());
		}
	}
	protected static void implicitlyWait(WebDriver driver, int waitForSecond) {
		driver.manage().timeouts().implicitlyWait(waitForSecond, TimeUnit.SECONDS);
	}

	/**
	 * This method will delete newly created file or folder
	 */
	protected void doChecked(String elementName) {
		try {
			WebElement element = driver.findElement(By.partialLinkText(elementName));
			String href = element.getAttribute("href");

			String[] temp = null;
			temp = href.split("/");
			String hashValue = temp[temp.length - 1];
			String xpathStr = ".//*[@id='" + hashValue + "']/td[1]/button";
			driver.findElement(By.xpath(xpathStr)).click();
			makeWait(1);
		} catch (Exception e) {
		}
	}

	protected String getFromSession(String key) {
		Map<String, String> sessionList = settings.getSession()
				.getTemporaryValue();
		return sessionList.get(key);
	}

	protected void setToSession(String key, String value) {
		Map<String, String> sessionList = settings.getSession()
				.getTemporaryValue();
		sessionList.put(key, value);
		SessionBean sb = new SessionBean();
		sb.setTemporaryValue(sessionList);
		settings.setSession(sb);
		// settings.setSession(new
		// SessionBean().setTemporaryValue(sessionList));
	}

	protected void goBack() {
		// driver.navigate().back();
		jse.executeScript("history.go(-1)", "");
		// makeWait(this.waitTime+2);
	}

}
