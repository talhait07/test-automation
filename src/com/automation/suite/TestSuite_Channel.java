package com.automation.suite;

import java.awt.AWTException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.AppConstant;
import com.automation.util.ApplicationUtility;

public class TestSuite_Channel extends TestHelper {

	String name=SitePageModel.randomStringGenerator(driver);
	String channel_name = name +"Test channel";
	
	String name2=SitePageModel.randomStringGenerator(driver);
	String channel_name2 = name2 +"Test channel inviteMember";
	
	String name3=SitePageModel.randomStringGenerator(driver);
	String channel_name3 = name3 +"Test channel search";
	
	String name4=SitePageModel.randomStringGenerator(driver);
	String channel_name4 = name4 +"Test channel Favourite";
	
	String nameEdit=SitePageModel.randomStringGenerator(driver);
	String channelNameEdit = nameEdit +"Test channel";
	
	String post=SitePageModel.randomStringGenerator(driver);
	String channelPost= post + "Test channel post";
	
	String post2=SitePageModel.randomStringGenerator(driver);
	String channelPost2= post2 + "Test channel ppost";
	
	String Announcement=SitePageModel.randomStringGenerator(driver);
	String channelAnnouncement= Announcement +"Test channel Announcement";
	
	String Event=SitePageModel.randomStringGenerator(driver);
	String channelEvent= Event + "Test channel Event";
	
	String Event2=SitePageModel.randomStringGenerator(driver);
	String channelEvent2= Event2 + "Test channel Event2";
	
	String Poll=SitePageModel.randomStringGenerator(driver);
	String channelPoll= Poll + "Test channel Poll";
	
	String Poll2=SitePageModel.randomStringGenerator(driver);
	String channelPoll2= Poll2 + "Test channel Poll2";
	
	String File=SitePageModel.randomStringGenerator(driver);
	String channelFile= File + "Test channel File";

	@Test
	public void LaunchConnectikHome() {
	
		SitePageModel.maximizeWindow(driver);
		
		SitePageModel.waitFor(30);
		SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com");
		SitePageModel.waitFor(1);
		SitePageModel.enterText(driver, By.id("password"),"T@skor13");
		SitePageModel.waitFor(1);  
		SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
		SitePageModel.waitFor(40);	
		SitePageModel.elementIsVisible(driver, By.xpath("/html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[1]/div[2]/div/h2"));//Click 'Sign Out'
		SitePageModel.waitFor(3);
		
//		List <WebElement> menuIconList = driver.findElements(By.xpath("//div[@class='iconset top-menu-toggle-dark']")); //Find All (+) icon and click on 1st
//		SitePageModel.waitFor(5);
//		menuIconList.get(1).click();		
//		SitePageModel.waitFor(5);
	}
	
	
	
	@Test
	@TestInfo(tcName="Update memeber of a PublicTeams",feature="PublicTeams", expectedResult="Member is  updated successfully")
	public void tc_0302_PublicTeamsUpdateMember(){
		
		driver.navigate().refresh();	
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Info & Members"));//("Click on info & member"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath(".//input[contains(@class, 'form-control')]"),"test1");//("type invite member");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("user-test1.coc@rootnext.com"));//("Click on members");
		SitePageModel.waitFor(5);
	
		SitePageModel.tryClick(driver,By.xpath("//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                             
		SitePageModel.waitFor(30);
		
		WebElement memImage = driver
				.findElement(By
						.xpath(".//*[@id='sidebar-tile-members']/div[2]/div/div/a[1]/img"));
		SitePageModel.waitFor(15);

		Assert.assertTrue(memImage.isDisplayed());
		
		
		
	}
	
	@Test
	@TestInfo(tcName="Edit Public Teams info",feature="PublicTeams", expectedResult="Public Teams information is  updated")
	public void tc_0303_CreatedPublicTeamsUpdate(){
		
		driver.navigate().refresh();	
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.xpath(".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Info & Members"));//("Click on info & member"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),channelNameEdit);
		SitePageModel.waitFor(1);
		
	    SitePageModel.enterText(driver, By.tagName("textarea"),"This channel only use for testing purpose");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                             
		SitePageModel.waitFor(30);
		SitePageModel.tryClick(driver, By.xpath(".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.linkText("Info & Members"));//("Click on info & member"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"01 RN Public Team");
		SitePageModel.waitFor(1);
		
	    SitePageModel.enterText(driver, By.tagName("textarea"),"This channel only use for testing purpose");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                             
		SitePageModel.waitFor(30);
		
	}
	

	
	@Test
	@TestInfo(tcName="Delete Public Teams",feature="Public Teams", expectedResult="Public Teams is  deleted successfully")
	public void tc_0304_PublicTeamsDelete() throws AWTException{
	
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(
				".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Archive Team"));//("Click on close channel"));
		SitePageModel.waitFor(7);	
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-success']"));//("Click on ok button"));
		SitePageModel.waitFor(5);	
	
	
		
		SitePageModel.tryClick(driver, By.linkText("Reactivate Team"));//("Click on close channel"));
		SitePageModel.waitFor(7);	
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-success']"));//("Click on ok button"));
		SitePageModel.waitFor(5);	

	}
	
	@Test
	@TestInfo(tcName="Create Public Teams with required field",feature="Public Teams", expectedResult="Public Teams is created successfully")
	public void tc_0305_CreatePublicTeamsUsingOnlyRequiredField(){
		
		String Ch_Name=SitePageModel.randomStringGenerator(driver);
		String channelNameRequired="channelReq "+Ch_Name;
		
		
		SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on public teams");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//a[@class='pull-right btn teamsCustom btn-primary']"));//("Click on add teams");
		SitePageModel.waitFor(15);
	
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),channelNameRequired);//("type channel name");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createGroupButton']"));//("Click on create button"));
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.partialLinkText("channelReq"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);	
		
		SitePageModel.tryClick(driver, By.xpath(
				".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Archive Team"));//("Click on close channel"));
		SitePageModel.waitFor(7);	
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-success']"));//("Click on ok button"));
		SitePageModel.waitFor(5);
	}
	
	
	
	@Test
	@TestInfo(tcName="Create Public Teams with invalid data",feature="Public Teams", expectedResult="Error message is displayed")
	public void tc_0306_CreatePublicTeamsUsingRequiredFieldBlank(){
		
		SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on public teams");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//a[@class='pull-right btn teamsCustom btn-primary']"));//("Click on add teams");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createGroupButton']"));//("Click on create channel"));
		SitePageModel.waitFor(10);
		
//		String strExpect = "Required field.";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='postCreateForm']/span"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//		Assert.assertTrue(bFound);
//		
//		SitePageModel.waitFor(2);
		
	}
	
	
	
	@Test
	@TestInfo(tcName="Invite memeber to Public Teams",feature="Public Teams", expectedResult="Member is invited")
    public void tc_0307_InviteMemberInPublicTeams(){
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'created Channel link
		SitePageModel.waitFor(7);	
		
		SitePageModel.tryClick(driver,By.xpath(".//a[contains(@class, 'btn btn-mini btn-default') and text() = 'Invite Members']"));//Click on '' arrow
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//input[contains(@class, 'form-control')]"),"Five");//("type invite member");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("user-test5.coc@rootnext.com"));//("Click on members");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath("//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                             
		SitePageModel.waitFor(30);
		
		WebElement memImage = driver
				.findElement(By
						.xpath(".//*[@id='sidebar-tile-members']/div[2]/div/div/a[1]/img"));
		SitePageModel.waitFor(5);

		Assert.assertTrue(memImage.isDisplayed());
		
		SitePageModel.tryClick(driver,By.xpath(".//a[contains(@class, 'btn btn-mini btn-default') and text() = 'Invite Members']"));//Click on '' arrow
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//input[contains(@class, 'form-control')]"),"Five");//("type invite member");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.id("user-test5.coc@rootnext.com"));//("Click on members");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.xpath("//button[contains(@class, 'btn btn-info btn-small') and 'Update Members']")); //click 'update member'                                                                             
		SitePageModel.waitFor(30);
		
		
	}

	
	@Test
    @TestInfo(tcName="Search created Public Teams",feature="Public Teams", expectedResult="Public Teams is searching successfully")
	
    public void tc_0356_SearchPublicTeams() throws AWTException{
		
		driver.navigate().to("https://secure.connectik.com");
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on public teams");
		SitePageModel.waitFor(15);
		
		SitePageModel.enterText(driver, By.xpath("//input[@class='form-control teamsCustom']"),"01 RN Public Team");
		SitePageModel.waitFor(5);
		
		SitePageModel.clickButtonByRobotClass();
		SitePageModel.waitFor(10);
		
		SitePageModel.checkMenuText(driver,"01 RN Public Team" ,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[3]/div/div/div[2]/div/div/div/div/h5/a"));
		SitePageModel.waitFor(5);
		
	}
	
	@Test
    @TestInfo(tcName="Remove From Favourite Public Teams",feature="Public Teams", expectedResult="Public Teams is Remove From Favourite successfully")
	
    public void tc_0363_RemoveFromFavouritePublicTeams() {
		
		driver.navigate().to("https://secure.connectik.com");
		SitePageModel.waitFor(30);
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.partialLinkText("Remove from Favorites"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.elementIsVisible(driver, By.partialLinkText("Add to Favorites"));//click on 'Everyone Channel link
		SitePageModel.waitFor(5);
	
	}
	
	@Test
    @TestInfo(tcName="Add To Favourite Public Teams",feature="Public Teams", expectedResult="Public Teams is Add To Favourite successfully")
	
    public void tc_0364_AddToFavouritePublicTeams() {
		
//		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
//		SitePageModel.waitFor(20);
		
		SitePageModel.tryClick(driver, By.partialLinkText("Add to Favorites"));//click on 'Everyone Channel link
		SitePageModel.waitFor(5);
		
		SitePageModel.elementIsVisible(driver, By.partialLinkText("Remove from Favorites"));//click on 'Everyone Channel link
		SitePageModel.waitFor(10);
		
			
	}
	
	

	
	
	@Test
	@TestInfo(tcName="Create a post on a Public Teams",feature="Public Teams Post", expectedResult="Post is created on a Public Teams successfully")
	public void tc_0308_CreatePost() throws AWTException{
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"channel_post_name");//("type post text");
		SitePageModel.waitFor(1);
		
		String strUploadImage = AppConstant.IMAGE_PATH + "TestPost.jpg";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(5);
	    upload.sendKeys(strUploadImage);
	    
	    SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
		SitePageModel.waitFor(30);
		
	
		String strExpect = "channel_post_name";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(2);

	}
	
	
	
	
	@Test
	@TestInfo(tcName="Create post using invalid data",feature="Public Teams Post", expectedResult="Error message is displayed")
    public void tc_0309_CreateBlankPost(){

		SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
		SitePageModel.waitFor(30);
		
		String strExpect = "Required field.";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='postCreateForm']/span"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(2);
	}
	

	@Test
	@TestInfo(tcName="Edit a post on a Public Teams",feature="Public Teams Post", expectedResult="Post is updated successfully")
    public void tc_0310_EditPost(){

		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited channel post name");//("edit text"));
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='updatePostButton']"));//("Click on update button"));
		SitePageModel.waitFor(30);
	
		String strExpect = "Edited channel post name";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(2);
	}
	@Test
	@TestInfo(tcName="Like an Post",feature="Public Teams Post", expectedResult="Like number is increased")
    public void tc_0357_LikePost(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		}
	
	
	@Test
	@TestInfo(tcName="UnLike an Post",feature="Public Teams Post", expectedResult="Like number is decreased")
    public void tc_0358_UnLikePost(){
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);									  
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}

	
	@Test
	@TestInfo(tcName="Add comment on a post of a Public Teams",feature="Public Teams Post", expectedResult="Comment is added under post")
    public void tc_0311_AddCommentToPost(){
		
		
		SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(2);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);
	}
	
	
	
	@Test
	@TestInfo(tcName="Edit comment on a post of a Public Teams",feature="Public Teams Post", expectedResult="Comment is Edit under post of Public Teams")

    public void tc_0312_EditCommentToPost(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
	
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(7);
		
		
		String strExpect = "Edited first comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	
	@Test
	@TestInfo(tcName="Delete comment from post of a Public Teams",feature="Public Teams Post", expectedResult="Comment is deleted under post of Public Teams")
    public void tc_0313_DeleteCommentFromPost(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);
	
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(5);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	
	@Test
	@TestInfo(tcName="Delete a post",feature="Public Teams Post", expectedResult="Post is Deleted")

    public void tc_0314_DeletePost(){
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(4);
			
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(5);
		
			
		
		
		
	}
	
	
	@Test
	@TestInfo(tcName="Create a post on everyone Public Teams",feature="Public Teams Post", expectedResult="Post is created on everyone Public Teams successfully")
		public void tc_0361_CreatePostUsingOnlyRequiredField() {
				
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);	
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("postForm"));//("Click on post");
		SitePageModel.waitFor(3);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Using only required field channel post name");//("type post text");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createPostButton']"));//("Click on create button"));
		SitePageModel.waitFor(5);
	
		SitePageModel.checkMenuText(driver,"Using only required field channel post name" ,By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
		SitePageModel.waitFor(5);
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(4);
			
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(5);
		

		}
	
	@Test
	@TestInfo(tcName="Create a Announcement on a Public Teams",feature="Public Teams Announcement", expectedResult="Announcement is created successfully")

    public void tc_0315_CreateAnnouncement(){
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

		
		SitePageModel.tryClick(driver, By.id("announcementForm"));//("Click on Announcement");
		SitePageModel.waitFor(7);
	
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Test channel announcement name");//"announcement title";
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is automation testing purpose.");//"Announcement description";
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));//("Click on create button"));
		SitePageModel.waitFor(15);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));//Check announcement title
		SitePageModel.waitFor(2);

		String strExpect = "Test channel announcement name";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(5);
		
	
		
	}
	
	
	@Test
	@TestInfo(tcName="Create Announcement with invalid data",feature="Public Teams Announcement", expectedResult="Error message is dispalyed")

    public void tc_0316_CreateAnnouncementWithBlankInfo(){
		
		
		SitePageModel.tryClick(driver, By.id("announcementForm"));//("Click on Announcement");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("createAnnouncementButton"));//("Click on create button"));
		SitePageModel.waitFor(10);

		
		String strExpect = "Required field.";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/span"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(2);
		
		String strExpect2 = "Required field.";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm2 = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[2]/span"));
		boolean bFound2 = aboutTextElm2.get(0).getText().contains(strExpect2);
		Assert.assertTrue(bFound2);
		
		SitePageModel.waitFor(1);
		
	}
	
	@Test
	@TestInfo(tcName="Edit a Announcement",feature="Public Teams Announcement", expectedResult="Announcement is updated")

    public void tc_0317_EditAnnouncement(){
		
		driver.navigate().refresh(); 
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(5);
	
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Edited channel announcement name");
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited this is automation testing purpose.");
		SitePageModel.waitFor(1);
	
		SitePageModel.tryClick(driver, By.id("updateAnnouncementButton"));//("Click on update button");
		SitePageModel.waitFor(15);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));//Check announcement title
		SitePageModel.waitFor(2);
		
		String strExpect = "Edited channel announcement name";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/h5[2]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound);
		
		SitePageModel.waitFor(1);
	}
	
	@Test
	@TestInfo(tcName="Like an Announcement",feature="Public Teams Announcement", expectedResult="Like number is increased")
    public void tc_0359_LikeAnnouncement(){
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		}
	
	
	@Test
	@TestInfo(tcName="UnLike an Announcement",feature="Public Teams Announcement", expectedResult="Like number is decreased")
    public void tc_0360_UnLikeAnnouncement(){
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);									  
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	@Test
	@TestInfo(tcName="Add Comment under Announcement portion",feature="Public Teams Announcement", expectedResult="Comment is added")

    public void tc_0318_AddCommentToAnnouncement(){
		
		
		SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);
	}
	
	
	
	@Test
	@TestInfo(tcName="Edit Comment under Announcement portion",feature="Public Teams Announcement", expectedResult="Comment is edited")
    public void tc_0319_EditCommentToAnnouncement(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(17);

		String strExpect = "Edited first comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	@Test
	@TestInfo(tcName="Delete Comment from Announcement portion",feature="Public Teams Announcement", expectedResult="Comment is Deleted")
    public void tc_0320_DeleteCommentFromAnnouncement(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(7);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Delete a Announcement",feature="Public Teams Announcement", expectedResult="Announcement is deleted")
    public void tc_0321_DeleteAnnouncement(){
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);

		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button");
		SitePageModel.waitFor(7);
	
		
	}
	
	@Test
	@TestInfo(tcName="Create a Allday Event on a Channel",feature="Public Teams Event", expectedResult="Allday Event is created")
    public void tc_0322_CreateEventForAllDay() throws AWTException{
		
		driver.navigate().refresh();
	    SitePageModel.waitFor(30);
	    String AllDaychannelEvent= "Alldayevent";
	    String location = "Bangladesh";
	    
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),AllDaychannelEvent);
		SitePageModel.waitFor(1);
		
		
		if(driver.findElement(By.className("select2-chosen")).getText().equals("Select location")){

            SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
            SitePageModel.waitFor(3);
            List<WebElement> listInput=driver.findElements(By.tagName("input"));

            listInput.get(0).sendKeys(location);
            SitePageModel.waitFor(5);
            SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
            SitePageModel.waitFor(3);
		}
		
		
		SitePageModel.tryClick(driver, By.tagName("textarea"));//("Click on description"));
		SitePageModel.waitFor(3);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"QA meeting");// type description
		SitePageModel.waitFor(1);
	
		String strUploadImage = AppConstant.IMAGE_PATH + "TestEvent.jpg";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    upload.sendKeys(strUploadImage);
	    
	    SitePageModel.waitFor(10);
		
		SitePageModel.tryClick(driver, By.id("createEventButton"));//("Click on create"));
		SitePageModel.waitFor(8);
		

		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
				    
		 String strExpect = "Alldayevent";
		 SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound);
	    
	    

	}
	
	@Test
	@TestInfo(tcName="Create Event with invalid data",feature="Public Teams Event", expectedResult="Error message is displayed")
    public void tc_0323_CreateEventWithBlankInfo(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);	

		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(10);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.xpath(".//*[@id='createEventButton']"));//("Click on create"));
		SitePageModel.waitFor(7);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check required field
		SitePageModel.waitFor(2);
		
		
		
	
	}
	
	
	@Test
	@TestInfo(tcName="Create a Multiple day Event",feature="Public Teams Event", expectedResult="Multiple day Event is created")
    public void tc_0324_CreateEventForMultipleDay() throws AWTException{
		
		driver.navigate().refresh();
	    SitePageModel.waitFor(30);
	    String MultipleDaychannelEvent= "Multipledayevent";
	    String location = "Bangladesh";
	    
		SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),MultipleDaychannelEvent);//("type channel name");
		SitePageModel.waitFor(1);
		
		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(5);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(5);

		
		if(driver.findElement(By.className("select2-chosen")).getText().equals("Select location")){

            SitePageModel.tryClick(driver, By.xpath("//span[contains(@class, 'select2-chosen') and normalize-space()='Select location']"));
            SitePageModel.waitFor(6);
            List<WebElement> listInput=driver.findElements(By.tagName("input"));
            SitePageModel.waitFor(7);

            listInput.get(0).sendKeys(location);
            SitePageModel.waitFor(5);
            SitePageModel.tryClick(driver, By.xpath("//div[@role='option']"));
            SitePageModel.waitFor(10);
		}
		
		
		SitePageModel.enterText(driver,By.tagName("textarea"),"QA meeting");// type description
		SitePageModel.waitFor(1);
		
		String strUploadImage = AppConstant.IMAGE_PATH + "TestEvent2.jpg";
	    WebElement upload = driver.findElement(By.id("attachImageButton"));
	    SitePageModel.waitFor(10);
	    upload.sendKeys(strUploadImage);
	    
	    SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.id("createEventButton"));//("Click on create"));
		SitePageModel.waitFor(15);
		
		driver.navigate().refresh();
	    SitePageModel.waitFor(25);
	    
	    String strExpect = "Multipledayevent";
		 SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound);
	    
		
	}

	
	@Test
	@TestInfo(tcName="Edit an Event",feature="Public Teams Event", expectedResult="Event is updated")
    public void tc_0325_EditEventInChannel() throws AWTException{
		driver.navigate().refresh(); 
		SitePageModel.waitFor(25);
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"Edited multiple day");//("edit title")
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver,By.tagName("textarea"),"Edited QA meeting");// type description
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("updateEventButton"));
		SitePageModel.waitFor(20);
		
		String strExpect = "Edited multiple day";
		 SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/h4"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound);
		
	}
	
	
	@Test
	@TestInfo(tcName="Like an Event",feature="Public Teams Event", expectedResult="Like number is increased")
    public void tc_0326_LikeEvent(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a[2]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		}
	
	
	@Test
	@TestInfo(tcName="UnLike an Event",feature="Public Teams Event", expectedResult="Like number is decreased")
    public void tc_0327_UnLikeEvent(){
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}

	
	@Test
	@TestInfo(tcName="Add comment on Event",feature="Public Teams Event", expectedResult="Comment is added")
    public void tc_0328_AddCommentToEvent(){
		
		
		SitePageModel.tryClick(driver,By.linkText("0 comments"));// Click comments
		SitePageModel.waitFor(10);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
//	    String strExpect1 = "Second comment";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
//		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
//	    Assert.assertTrue(bFound1);
	}
	
	
	
	@Test
	@TestInfo(tcName="Edit comment to an Event",feature="Public Teams Event", expectedResult="Comment is updated")
    public void tc_0329_EditCommentToEvent(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(10);
		
		
		String strExpect = "Edited first comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	
	@Test
	@TestInfo(tcName="Delete comment to an Event",feature="Public Teams Event", expectedResult="Comment is deleted")
    public void tc_0330_DeleteCommentFromEvent(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(10);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//p[contains(@class, 'c-text text-black small-text no-margin')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Delete an Event",feature="Public Teams Event", expectedResult="Event is deleted")
    public void tc_0331_DeleteEvent(){
		driver.navigate().refresh(); 
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.linkText("Delete"));// Select delete
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
		SitePageModel.waitFor(7);
		
		
		
	}
	
	
	@Test
	@TestInfo(tcName="Delete an Event",feature="Public Teams Event", expectedResult="Event is deleted")
    public void tc_0365_CheckSystemTimeEvent(){
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
		SitePageModel.waitFor(3);

		
		String systemTime= SitePageModel.getCurrnetSystemTime(driver);
		
		System.out.println(systemTime);
		SitePageModel.waitFor(3);
		
       String roundTime= SitePageModel.getRoundSystemTime(driver);
		
		System.out.println(roundTime);
		
		
	}
	
	
	@Test
	@TestInfo(tcName="Create a Event on everyone channel",feature="Public Teams Event", expectedResult="Event is created on everyone channel successfully")
		public void tc_0362_CreateEventUsingOnlyRequiredField() {
			
		driver.navigate().refresh(); 
		SitePageModel.waitFor(30);	
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
			SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
			SitePageModel.waitFor(3);
			
			SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"EventUsingOnlyRequiredField");//("type channel name");
			SitePageModel.waitFor(1);
			
			SitePageModel.tryClick(driver, By.xpath(".//*[@id='createEventButton']"));//("Click on create"));
			SitePageModel.waitFor(15);
			
			SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver,By.linkText("Delete"));// Select delete
			SitePageModel.waitFor(5);
			
			SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
			SitePageModel.waitFor(7);
		}
	
	
	@Test
	@TestInfo(tcName="Create All day Poll",feature="Public Teams Poll", expectedResult="All day Poll is created")
    public void tc_0332_CreatePollForAllDay(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
		SitePageModel.waitFor(10);
	
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is the name of our national bird?");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//label[text()='One day poll']"));//("Click on all day event"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("addPollOption"));//("Click on add form");
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Doel");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Peacock");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Crow");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(15);
		
		
		 String strExpect = "What is the name of our national bird?";
		 SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is the name of our national bird?']"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound);
	}

	
	
	@Test
	@TestInfo(tcName="Create Poll on a Public Teams",feature="Public Teams Poll", expectedResult="Poll is created")
    public void tc_0333_CreatePollWithBlankField(){
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
		SitePageModel.waitFor(7);

		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(7);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check question required field
		SitePageModel.waitFor(2);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/span"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/span[1]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/span[2]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		
	}



	@Test
	@TestInfo(tcName="Create Multiple day Poll",feature="Public Teams Poll", expectedResult="Multiple day Poll is created")
    public void tc_0334_CreatePollForMultipleDay(){
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[1]/a"));//("Click on poll form");
		SitePageModel.waitFor(10);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your favourite color?");
		SitePageModel.waitFor(1);
		
		String startDate= SitePageModel.AddDateWithToday(3);
		SitePageModel.enterAutoCompleteDate(driver,By.xpath(".//*[@id='eventStartDateField']/div/input"), startDate);
		String endDate= SitePageModel.AddDateWithToday(5);
		SitePageModel.enterAutoCompleteDate(driver,  By.xpath(".//*[@id='eventEndDateField']/div/input"), endDate);
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is multiple day poll");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("addPollOption"));//("Click on add form");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[2]/input"),"Red");
		SitePageModel.waitFor(1);

		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[3]/input"),"Green");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[4]/div[2]/div[4]/input"),"Blue");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(7);
			
		String strExpect = "What is your favourite color?";
		 SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your favourite color?']"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound);
	}

	@Test
	@TestInfo(tcName="Edit a Poll",feature="Public Teams Poll", expectedResult="Poll info is updated")
    public void tc_0335_EditPoll(){
		driver.navigate().refresh();
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
		SitePageModel.waitFor(5);

		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your unfavourite color?");
		SitePageModel.waitFor(2);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited This is general poll");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='updatePollButton']"));//("Click on update button");
		SitePageModel.waitFor(7);
	
		String strExpect = "What is your unfavourite color?";
		SitePageModel.waitFor(2);
		 List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your unfavourite color?']"));
		 boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		 Assert.assertTrue(bFound); 
		 SitePageModel.waitFor(2);
		 
		  }


	@Test
	@TestInfo(tcName="Cast vote on All day Poll",feature="Public Teams Poll", expectedResult="Vote is cast in Poll")
    public void tc_0336_CastVoteInPoll(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		SitePageModel.selectRadioText(driver, By.xpath("//label[text()='Peacock']"));// click radio button
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary btn-mini btn-cons m-t-5' and text() = 'Vote']"));//("Click on vote");	
		SitePageModel.waitFor(7);
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
		SitePageModel.waitFor(7);
	}
	
	@Test
	@TestInfo(tcName="Like to a Poll",feature="Public Teams Poll", expectedResult="Like number is increased")
    public void tc_0337_LikePoll(){	
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(5);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Unlike to a Poll",feature="Public Teams Poll", expectedResult="Like number is decreased")
    public void tc_0338_UnLikePoll(){
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "Like";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[6]/ul/li[1]/a"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Add comment on a poll",feature="Public Teams Poll", expectedResult="Comment is added")
    public void tc_0339_AddCommentToPoll(){
		SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);
	}
	
	
	@Test
	@TestInfo(tcName="Edit comment on a poll",feature="Public Teams Poll", expectedResult="Comment is updated")
    public void tc_0340_EditCommentToPoll(){
	
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(7);
		
	}
	
	
	
	@Test
	@TestInfo(tcName="Delete comment from a Poll",feature="Public Teams Poll", expectedResult="Comment is deleted")
    public void tc_0341_DeleteCommentToPoll(){
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(7);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div/div[7]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Delete a poll",feature="Public Teams Poll", expectedResult="Poll is deleted")
    public void tc_0342_DeletePoll(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of poll");
		SitePageModel.waitFor(7);
		
		
		
	}
	
	@Test
	@TestInfo(tcName="Create poll with rating scale",feature="Public Teams Poll", expectedResult="Poll is created with rating")
    public void tc_0354_CreatePollInRatingScale(){
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),"What is your name?");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//a[@class='semi-bold poll-more-details']"));//("Click on more details");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"This is general poll");
		SitePageModel.waitFor(1);
				
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[2]/input"),"A");
		SitePageModel.waitFor(10);

		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[3]/input"),"B");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[4]/input"),"C");
		SitePageModel.waitFor(1);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[5]/input"),"D");
		SitePageModel.waitFor(1);
	
		SitePageModel.enterText(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/div[6]/input"),"E");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(10);
		
		
		String strExpect = "What is your name?";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("//h5[@class='semi-bold text' and text()= 'What is your name?']"));
	    boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
		Assert.assertTrue(bFound); 
		SitePageModel.waitFor(2);
		
	}
	
	
	
	@Test
	@TestInfo(tcName="Create poll with invalid data",feature="Public Teams Poll", expectedResult="Error message is displayed")
    public void tc_0355_CreatePollInRatingScaleWithBlankField(){
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.id("pollForm"));//("Click on poll form");
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div/div[2]/a"));//("Click on poll form");
		SitePageModel.waitFor(10);
		
		SitePageModel.tryClick(driver, By.id("createPollButton"));//("Click on create button");
		SitePageModel.waitFor(7);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[1]/span"));//Check question required field
		SitePageModel.waitFor(2);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[1]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[2]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[3]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[4]"));//Check question required field
		SitePageModel.waitFor(2);
	
		SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div/div[3]/span[5]"));//Check answer required field
		SitePageModel.waitFor(2);
		
		
		
	}
	
	

	@Test
	@TestInfo(tcName="Open File tab view on a Public Teams",feature="Public Teams File", expectedResult="File window is opened")
    public void tc_0343_OpenFileWindow(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(30);
		
		
		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
		SitePageModel.waitFor(7);
		
		String strExpect = "Description";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[1]/label"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
		
	}
	
	
	@Test
	@TestInfo(tcName="Close File tab view on a Public Teams",feature="Public Teams File", expectedResult="File window is closed")
    public void tc_0344_CloseFileWindow(){
			
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
		SitePageModel.waitFor(5);
	}
	
	
//	@Test
//	@TestInfo(tcName="Upload File by Drag and drop icon",feature="Public Teams File", expectedResult="File is uploaded")
//    public void tc_0345_UploadFileByUsingDragAndDropIcon() throws AWTException{
//		
//		driver.navigate().refresh();
//		SitePageModel.waitFor(30);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
//		SitePageModel.waitFor(7);
//
//		SitePageModel.enterText(driver, By.tagName("textarea"),"This is important file using UploadFileByDragAndDrop");// Type description
//		SitePageModel.waitFor(1);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[3]/div[1]/div/span"));//("Click on file");
//		SitePageModel.waitFor(7);
//		
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//		SitePageModel.waitFor(1);
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//		
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//	
//		SitePageModel.tryClick(driver, By.id("createFileButton"));//("Click on create button");
//		SitePageModel.waitFor(15);
//		
//		String strExpect = "This is important file using UploadFileByDragAndDrop";
//		SitePageModel.waitFor(5);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//	    
//	    SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver, By.linkText("Delete"));
//		SitePageModel.waitFor(7);
//		
//		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
//		SitePageModel.waitFor(5);
//		
//	
//	}
	
	
//	@Test
//	@TestInfo(tcName="Upload File by click on link ",feature="Public Teams File", expectedResult="File is uploaded")
//    public void tc_0346_UploadFileByfileSelection() throws AWTException{
//		SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//click on 'Everyone Channel link
//		SitePageModel.waitFor(7);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='fileForm']"));//("Click on file");
//		SitePageModel.waitFor(7);
//	
//		SitePageModel.enterText(driver, By.tagName("textarea"),"This is important file using UploadFileByfileSelection");// Type description
//		SitePageModel.waitFor(1);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='content-creator']/div[2]/div[2]/div[3]/div[1]/div/i"));//("Click on file");
//		SitePageModel.waitFor(7);
//		
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//		SitePageModel.waitFor(1);
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile2.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//		
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//	
//		SitePageModel.tryClick(driver, By.id("createFileButton"));//("Click on create button");
//		SitePageModel.waitFor(20);
//		
//		String strExpect = "This is important file using UploadFileByfileSelection";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//	
//	}
	
	
//	@Test
//	@TestInfo(tcName="Edit uploaded file",feature="Public Teams File", expectedResult="File is upadated and uploaded")
//    public void tc_0347_EditUploadFile() throws AWTException{
//		
//		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
//		SitePageModel.waitFor(5);
//		
//		SitePageModel.tryClick(driver,By.linkText("Edit"));// Select Edit
//		SitePageModel.waitFor(7);
//
//		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited important file");// edit description 
//		SitePageModel.waitFor(7);
//		
//		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[3]/div[1]/div/span"));//("Click on file");
//		SitePageModel.waitFor(7);
//		
//		String strDocumentImgPath = AppConstant.IMAGE_PATH + "Desktop.png";
//		SitePageModel.clickLocationBySikuli(strDocumentImgPath);
//		SitePageModel.waitFor(1);
//		SitePageModel.clearTextByRobotClass();
//		SitePageModel.waitFor(1);
//
//		String strUploadImage = AppConstant.IMAGE_PATH + "TestFile3.xlsx";
//		String strTextBoxImgPath = AppConstant.IMAGE_PATH + "TextBox.png";
//		
//		SitePageModel.typeImagePathBySikuli(strTextBoxImgPath, strUploadImage);
//
//		SitePageModel.waitFor(1);
//		SitePageModel.clickButtonByRobotClass();
//		SitePageModel.waitFor(10);
//	
//		SitePageModel.tryClick(driver, By.id("updateFileButton"));//("Click on update button");
//		SitePageModel.waitFor(20);
//	
//		String strExpect = "Edited important file";
//		SitePageModel.waitFor(2);
//		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[1]/p"));
//		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
//	    Assert.assertTrue(bFound);
//	}
	
	
	@Test
	@TestInfo(tcName="Like uploaded file",feature="Public Teams File", expectedResult="Like number is increased")
    public void tc_0348_LikeUploadFile(){
		
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = "1";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//a[contains(@class, 'action-link')]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	@Test
	@TestInfo(tcName="Unlike uploaded file",feature="Public Teams File", expectedResult="Like number is decreased")
    public void tc_0349_UnLikeUploadFile(){
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-thumbs-up')]"));//("Click on like");
		SitePageModel.waitFor(7);
		
		String strExpect = " 1 likes this.";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[4]/ul/li[1]"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertFalse(bFound);
	}

	@Test
	@TestInfo(tcName="Add comment on uploaded file",feature="Public Teams File", expectedResult="Comment is added")
    public void tc_0350_AddCommentToFile(){
		
		SitePageModel.tryClick(driver, By.xpath("//li[@class='toggleComments']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"First comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
		
		SitePageModel.enterText(driver, By.xpath(".//*[@id='commentText']"),"Second comment");
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath(".//button[text()='Comment']"));//("Click on comment");
		SitePageModel.waitFor(7);
			
		String strExpect = "First comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	    
	    String strExpect1 = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm1 = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[3]/div/div/div[2]/p/p"));
		boolean bFound1 = aboutTextElm1.get(0).getText().contains(strExpect1);
	    Assert.assertTrue(bFound1);
	}
	
	
	@Test
	@TestInfo(tcName="Edit comment on uploaded file",feature="Public Teams File", expectedResult="Comment is updated")
    public void tc_0351_EditCommentToFile(){
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Edit"));
		SitePageModel.waitFor(5);
		
		SitePageModel.enterText(driver, By.tagName("textarea"),"Edited first comment");// write on new new comment
		SitePageModel.waitFor(1);
		
		SitePageModel.tryClick(driver, By.xpath("//button[contains(.,'Save')]"));//("Click on save");
		SitePageModel.waitFor(17);
			
		String strExpect = "Edited first comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
	}
	
	
	
	@Test
	@TestInfo(tcName="Delete comment from a uploaded file",feature="Public Teams File", expectedResult="Comment is deleted")
    public void tc_0352_DeleteCommentToFile(){
		
		SitePageModel.tryClick(driver, By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/ul/li/a/i"));//("Click on comment option");
		SitePageModel.waitFor(5);
	
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok");
		SitePageModel.waitFor(7);
		
		String strExpect = "Second comment";
		SitePageModel.waitFor(2);
		List<WebElement> aboutTextElm = driver.findElements(By.xpath("html/body/div[2]/div[2]/div/div[2]/div[2]/div/ul/li[3]/div[2]/div/div[5]/div[2]/div[2]/div/div/div[2]/p/p"));
		boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
	    Assert.assertTrue(bFound);
			}
	
	
	@Test
	@TestInfo(tcName="Delete file from a uploaded file",feature="Public Teams File", expectedResult="File is deleted")
    public void tc_0353_DeleteUploadFile(){
		
		driver.navigate().refresh();
		SitePageModel.waitFor(25);
		
		SitePageModel.tryClick(driver, By.xpath(".//i[contains(@class, 'fa fa-angle-down')]"));//("Click on comment");
		SitePageModel.waitFor(5);
		
		SitePageModel.tryClick(driver, By.linkText("Delete"));
		SitePageModel.waitFor(7);
		
		SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button"));
		SitePageModel.waitFor(5);
		
		
		
	}
}













