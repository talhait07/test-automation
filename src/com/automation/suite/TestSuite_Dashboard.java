package com.automation.suite;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.automation.pagemodel.SitePageModel;
import com.automation.util.ApplicationUtility;

public class TestSuite_Dashboard extends TestHelper{
	
	String name=SitePageModel.randomStringGenerator(driver);
//	String channel_name="Test channel "+name;
	String channel_name= name+"Test channel";
	
	@Test
	@TestInfo(tcName="Dashboard Channel Section",feature="Dashboard", expectedResult="Channel is created and count is increased on Dashboard")
    public void tc_0801_TestDashboardChannelLink(){
		
		List <WebElement> menuIconList = driver.findElements(By.xpath("//div[@class='iconset top-menu-toggle-dark']")); //Find All (+) icon and click on 1st
		SitePageModel.waitFor(1);
		menuIconList.get(1).click();		
		SitePageModel.waitFor(1);

		SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
		SitePageModel.waitFor(5);

//    SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//    SitePageModel.waitFor(5);


    String strCountChannel_Previous = driver.findElement(By.xpath(".//*[@id='widget-stats']/table/tbody/tr/td[3]/div/div/div[2]")).getText(); 
//    SitePageModel.waitFor(10);
    System.out.println(strCountChannel_Previous);
//    SitePageModel.waitFor(10);


    int intCountChannel_Previous = ApplicationUtility.getDecimalFromStringText(strCountChannel_Previous);
//    SitePageModel.waitFor(10);
    System.out.println("Previous channel number: "+intCountChannel_Previous);


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//    SitePageModel.tryClick(driver, By.id("lsb-feeds-create"));//("Click on +");
//    SitePageModel.waitFor(75);
//
//    SitePageModel.enterText(driver, By.name("title"),channel_name);//("type channel name");
//    SitePageModel.waitFor(1);
//
//    SitePageModel.tryClick(driver, By.xpath(".//*[@id='createGroupButton']"));//("Click on create button"));
//    SitePageModel.waitFor(30);
    
    SitePageModel.tryClick(driver, By.id("lsb-feeds"));//("Click on public teams");
	SitePageModel.waitFor(5);
	
	SitePageModel.tryClick(driver, By.xpath("//a[@class='pull-right btn teamsCustom btn-primary']"));//("Click on add teams");
	SitePageModel.waitFor(5);
	
	SitePageModel.enterText(driver, By.xpath("//input[@name='title']"),channel_name);//("type channel name");
	SitePageModel.waitFor(1);
	
	SitePageModel.tryClick(driver, By.xpath(".//*[@id='createGroupButton']"));//("Click on create button"));
	SitePageModel.waitFor(15);

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    driver.navigate().to("https://secure.connectik.com");
    SitePageModel.waitFor(20);

    SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-dashboard']/p"));
    SitePageModel.waitFor(5);

//    SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//    SitePageModel.waitFor(5);

    String strCountChannel_after = driver.findElement(By.xpath(".//*[@id='widget-stats']/table/tbody/tr/td[3]/div/div/div[2]")).getText(); 
    System.out.println(strCountChannel_after);
    SitePageModel.waitFor(1);

    int intCountChannel_BeforeSubtract = ApplicationUtility.getDecimalFromStringText(strCountChannel_after);
//    SitePageModel.waitFor(10);
    System.out.println("After Creation: "+intCountChannel_BeforeSubtract);

    int intCountChannel_afterSubtract = intCountChannel_BeforeSubtract -1;
    System.out.println("After Subtraction: "+intCountChannel_afterSubtract);


    Assert.assertEquals(intCountChannel_Previous, intCountChannel_afterSubtract);

    //Code for DELETING Created Channel

    SitePageModel.tryClick(driver, By.xpath("//span[text()='"+channel_name+"']"));//("Click on Created channel name");
    SitePageModel.waitFor(10);

	SitePageModel.tryClick(driver, By.xpath(".//a[@role = 'button' and text() = 'Management']"));//("Click on management");
    SitePageModel.waitFor(1);

    SitePageModel.tryClick(driver, By.linkText("Archive Team"));//("Click on close channel"));
    SitePageModel.waitFor(2);

    SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-success']"));//("Click on ok button"));
    SitePageModel.waitFor(5);
}

@Test
@TestInfo(tcName="Dashboard Receommendation Section",feature="Dashboard", expectedResult="Recommendation link is functioning from Dashboard")
public void tc_0802_TestDashboardRecommendationsLink(){

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
    SitePageModel.waitFor(4);

//    SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//    SitePageModel.waitFor(5);


    SitePageModel.tryClick(driver, By.xpath(".//*[@id='widget-stats']/table/tbody/tr/td[2]/div/div/a"));
    SitePageModel.waitFor(5);

    String strExpect = "Recommendations";
    List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/h3/span")); 
    boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
    Assert.assertTrue(bFound);
}

@Test
@TestInfo(tcName="Dashboard File Section",feature="Dashboard", expectedResult="File link is functioning from Dashboard")
public void tc_0803_TestDashboardFilesLink(){

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
    SitePageModel.waitFor(4);

//SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//    SitePageModel.waitFor(5);

    SitePageModel.tryClick(driver, By.xpath(".//*[@id='widget-stats']/table/tbody/tr/td[4]/div/div/a"));
    SitePageModel.waitFor(5);

    String strExpect = "Files";
    List<WebElement> aboutTextElm = driver.findElements(By.xpath(".//*[@id='main']/div[2]/div[2]/div/div[2]/div[1]/h3/span")); 
    boolean bFound = aboutTextElm.get(0).getText().contains(strExpect);
    Assert.assertTrue(bFound);
}

@Test
@TestInfo(tcName="Dashboard Member Section",feature="Dashboard", expectedResult="Member is invited and count is increased on Dashboard")
public void tc_0804_TestDashboardMemberLink(){

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(5);	
	
//	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//	SitePageModel.waitFor(5);		

	String strCountMember_Previous = driver.findElement(By.partialLinkText("Your connections:")).getText();
	SitePageModel.waitFor(1);
//	System.out.println(strCountMember_Previous);
	
	int intCountMemeber_Previous = ApplicationUtility.getDecimalFromStringText(strCountMember_Previous);
	SitePageModel.waitFor(1);  	
//	System.out.println("Member number before creation: "+intCountMemeber_Previous);
	

	SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
	SitePageModel.waitFor(2);	
	SitePageModel.tryClick(driver, By.partialLinkText("My Connections"));//click on 'My Connections link
	SitePageModel.waitFor(2);	
	
	SitePageModel.tryClick(driver, By.id("tab-members"));
	SitePageModel.waitFor(3); 		
		
	SitePageModel.enterText(driver, By.xpath(".//*[@id='members']/form/div/input"),"test5"); // Search by member
	SitePageModel.waitFor(1);	
	SitePageModel.tryClick(driver, By.xpath(".//*[@id='members']/form/div/span/button")); //Click on search button
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.xpath("//button[text()='Add Connection' and @class='btn btn-block btn-success btn-mini m-b-10']"));//("Click on Add button");
	SitePageModel.waitFor(3);
	
	SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
	SitePageModel.waitFor(20);
	
	SitePageModel.enterText(driver, By.id("username"),"test5.coc@rootnext.com");				
	SitePageModel.waitFor(1);
	SitePageModel.enterText(driver, By.id("password"),"T@skor13");
	SitePageModel.waitFor(1);

	SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'
	SitePageModel.waitFor(5); 
	
	SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
	SitePageModel.waitFor(5); 
	SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
	SitePageModel.waitFor(35);  
			
	SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
	SitePageModel.waitFor(2);	
	SitePageModel.tryClick(driver, By.partialLinkText("My Connections"));// Click on Connection
	SitePageModel.waitFor(3);	
	SitePageModel.tryClick(driver, By.xpath("//button[text()='Accept' and @class='btn btn-success btn-small']")); //Click on Accept button
	SitePageModel.waitFor(7);			
	
	SitePageModel.tryClick(driver, By.id("user-options"));//click on 'option icon'
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Logout"));//click on 'Logout link
	SitePageModel.waitFor(20);
	
	SitePageModel.enterText(driver, By.id("username"),"test3.coc@rootnext.com");				
	SitePageModel.waitFor(1);  
	
	SitePageModel.enterText(driver, By.id("password"),"T@skor13");
	SitePageModel.waitFor(1);
	
	SitePageModel.tryClick(driver, By.xpath("//button[@type='submit']"));//click on 'log in button'		
	SitePageModel.waitFor(5);
	
	SitePageModel.tryClick(driver, By.xpath("//label[text()='rootnext.com']"));// Select rootnext.com radio button
	SitePageModel.waitFor(5); 
	SitePageModel.tryClick(driver,By.xpath(".//button[contains(@class, 'btn btn-primary btn-cons') and 'Continue']"));//Click 'Continue'
	SitePageModel.waitFor(35);  

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
//	SitePageModel.tryClick(driver, By.xpath(".//*[@id='lsb-dashboard']/p"));
	SitePageModel.waitFor(5);		
	
//	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
//	SitePageModel.waitFor(5);		

	String strCountMember_After = driver.findElement(By.partialLinkText("Your connections:")).getText();
	
	SitePageModel.waitFor(1);
//	System.out.println(strCountMember_After);
	
	int intCountMember_BeforeSubtract = ApplicationUtility.getDecimalFromStringText(strCountMember_After);
	SitePageModel.waitFor(1);  	
//	System.out.println("Member number after creation: "+intCountMember_BeforeSubtract);
	
	int intCountMember_afterSubtract = intCountMember_BeforeSubtract -1; 
//	System.out.println("After Subtraction: "+intCountMember_afterSubtract);		
	
	Assert.assertEquals(intCountMemeber_Previous, intCountMember_afterSubtract);
			
	SitePageModel.tryClick(driver, By.partialLinkText("Your connections:")); // Click on Connection
	SitePageModel.waitFor(5);	
    SitePageModel.tryClick(driver, By.xpath("//button[text()='Remove Connection' and @class='btn btn-danger btn-small m-t-5']")); //Click on Remove button 
	SitePageModel.waitFor(5);
	}

@Test
@TestInfo(tcName="Dashboard Welcome message",feature="Dashboard", expectedResult="Welcome message shows correctly and can remove properly.")
public void tc_0805_TestDashboardWelcomeNote(){
	
	SitePageModel.waitFor(5);	
	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);	
	
	SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='welcome-widget']/div/div/div[1]/h2"));//Check 'Welcome to Connectik, Teste' text

	SitePageModel.tryClick(driver, By.partialLinkText("Remove this welcome area"));
	SitePageModel.waitFor(1);	
	
    SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
	SitePageModel.waitFor(3);		
	
	SitePageModel.tryClick(driver, By.id("user-options")); //Click on setting icon
	SitePageModel.tryClick(driver, By.partialLinkText("Preferences")); // Click on Preferences
	SitePageModel.waitFor(7);
	
	SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-desktop']"));  // Scroll to bottom on Preferences POPup
	SitePageModel.waitFor(2);		
	SitePageModel.tryClick(driver, By.xpath("//label[text()='Show Welcome screen']"));// Click on check-box 'Show Welcome screen'
	SitePageModel.waitFor(1);
	SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-sm btn-primary']"));//("Click on ok button of POPUP");
	SitePageModel.waitFor(20);
	
	SitePageModel.elementIsVisible(driver, By.xpath(".//*[@id='welcome-widget']/div/div/div[1]/h2"));//Check 'Settings icon'
	SitePageModel.waitFor(2);		
}

@Test
@TestInfo(tcName="Dashboard Welcome message link check",feature="Dashboard", expectedResult="Links in welcome message working properly.")
public void tc_0806_TestDashboardWelcomeNotelinkVerify(){
			
	SitePageModel.waitFor(2);	
	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);			
	SitePageModel.tryClick(driver, By.partialLinkText("Update Your Profile"));
	SitePageModel.waitFor(4);		
	SitePageModel.checkUrl(driver, "https://esn.connectik.org/profile");
	SitePageModel.waitFor(2);	

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Join a Channel"));
	SitePageModel.waitFor(3);	
	SitePageModel.checkUrl(driver, "https://esn.connectik.org/group/search");
	SitePageModel.waitFor(2);		
	
	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Create a Team"));
	SitePageModel.waitFor(3);	
	SitePageModel.checkUrl(driver, "https://esn.connectik.org/group/create?type=EXTERNAL");
	SitePageModel.waitFor(2);
}

@Test
@TestInfo(tcName="My Task of Dashboard ",feature="Dashboard", expectedResult="Created task is showing on My Task section correctly.")
public void tc_0807_TestDashboardMyTask(){
	
	SitePageModel.waitFor(2);	
	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);
	
	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
	SitePageModel.waitFor(1);	

	String strExpected = "You have no outstanding tasks at the moment.";
	String strActual1= driver.findElement(By.xpath(".//div[contains(@class,'padding-20 text-center small-text')]")).getText();
	Assert.assertEquals(strExpected, strActual1);
	
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Tasks"));
	SitePageModel.waitFor(3);		
	SitePageModel.enterText(driver, By.name("task"), "Test task for Dashboard");	//task title	
	SitePageModel.waitFor(1);		
	SitePageModel.tryClick(driver, By.id("add-task"));
	SitePageModel.waitFor(3);		

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);
	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
	SitePageModel.waitFor(1);
	
	SitePageModel.tryClick(driver, By.partialLinkText("Test task for Dashboard"));
	SitePageModel.waitFor(3);
	
	SitePageModel.tryClick(driver, By.xpath("//i[@class='fa fa-angle-down']"));
	SitePageModel.waitFor(2);
	SitePageModel.tryClick(driver, By.partialLinkText("Delete"));
	SitePageModel.waitFor(2);
    SitePageModel.tryClick(driver, By.xpath("//button[@class='btn btn-primary']"));//("Click on ok button of POPUP");
	SitePageModel.waitFor(3);

	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);			
	
	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='dashboard-announcement']/h4"));
	SitePageModel.waitFor(1);
	
	String strActual2= driver.findElement(By.xpath(".//div[contains(@class,'padding-20 text-center small-text') and text() = 'You have no outstanding tasks at the moment.']")).getText();

//	String strActual2= driver.findElement(By.xpath(".//div[contains(@class,'padding-20 text-center small-text')]")).getText();
//	System.out.println(strActual2);
	Assert.assertEquals(strExpected, strActual2);
}

@Test
@TestInfo(tcName="My Task of Dashboard ",feature="Dashboard", expectedResult="Created task is showing on My Task section correctly.")
public void tc_0808_TestDashboardMyConnection(){

	
	SitePageModel.waitFor(3);
	SitePageModel.tryClick(driver, By.partialLinkText("Dashboard"));
	SitePageModel.waitFor(3);	
	
	SitePageModel.scrollElement(driver,By.xpath(".//*[@id='widget-my-connections']/div/div/div[2]"));
	SitePageModel.waitFor(3);
	
	String strActual2= driver.findElement(By.xpath(".//div[contains(@class,'padding-20 text-center small-text') and text() = 'You have no connections yet.']")).getText();
	System.out.println(strActual2);
}


@Test
@TestInfo(tcName="Delete an Event",feature="Public Teams Event", expectedResult="Event is deleted")
public void tc_0365_CheckSystemTimeEvent(){
	
	
	SitePageModel.tryClick(driver, By.partialLinkText("01 RN Public Team"));//("Click on comment");
	SitePageModel.waitFor(7);
	
	SitePageModel.tryClick(driver, By.id("eventForm"));//("Click on event form");
	SitePageModel.waitFor(3);
 
	String elementval = driver.findElement(By.xpath(".//*[@id='eventStartDateField']/div/input")).getAttribute("value");

//	SitePageModel.getTimeDifference(driver, elementval);
	
	SitePageModel.waitFor(2);
	String ntime = "29";
	
	System.out.println("Current Difference time= "+elementval);

	String systemTime= SitePageModel.getCurrnetSystemTime(driver);
	System.out.println("Current System time= "+systemTime);

	
}


}