package com.automation.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;

public class ApplicationUtility {
	
	public static boolean checkValidURL(String strURL) {
		String strPattern = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		try {
	        Pattern patt = Pattern.compile(strPattern);
	        Matcher matcher = patt.matcher(strURL);
	        
	        return matcher.matches();
	    } catch (RuntimeException e) {
	        return false;
	    }      
	}
	
	public static void downloadImageFromURL(String strURL) throws IOException{

		URL url = new URL(strURL);

        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response1 = out.toByteArray();

        String strDestinationPath = AppConstant.IMAGE_PATH + "test-image1.jpg";
        FileOutputStream fos = new FileOutputStream(strDestinationPath);
        fos.write(response1);
        fos.close();
	}
	
	public static int getDecimalFromStringText(String strFullString)
	{
		int nDecimal = 0;
		Matcher matcher = Pattern.compile("\\d+").matcher(strFullString);
		if(matcher.find()) {
		    //System.out.println(matcher.group());
			nDecimal = Integer.parseInt(matcher.group());   		
		}
		return nDecimal;
	}
}


